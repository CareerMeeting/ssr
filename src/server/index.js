/* eslint-disable no-console */
import express from 'express';
import { renderToNodeStream } from 'react-dom/server';
import { renderStylesToNodeStream } from 'emotion-server';
import { getLoadableState } from 'loadable-components/server';
import { Helmet } from 'react-helmet';
import createMemoryHistory from 'history/createMemoryHistory';

import configureStore from '../shared/redux/store';
import { getAppWithRouter, renderHeader, renderFooter } from './render';
import sagas from '../shared/redux/sagas';

const app = express();

app.use('/assets', express.static('./dist'));

const renderApp = async (req, res) => {
    const store = configureStore({}, {}, createMemoryHistory());
    const context = {};
    const { rootApp, sheetsRegistry } = getAppWithRouter(store, req.url, context);

    if (context.url) {
        res.redirect(context.url);
        return;
    }

    let loadableState;

    store.runSagas(sagas).done.then(() => {
        const helmet = Helmet.renderStatic();

        res.status(200)
            .write(renderHeader(helmet));

        const htmlSteam = renderToNodeStream(rootApp)
            .pipe(renderStylesToNodeStream());
        const preloadedState = store.getState();
        const cssSheet = sheetsRegistry.toString();

        htmlSteam.pipe(res, { end: false });
        htmlSteam.on('end', () => {
            res.write(renderFooter(cssSheet, loadableState, preloadedState));
            return res.send();
        });
    }).catch(err => {
        res.status(500);
        // TODO: SEND PROBLEM VIEW
    });

    loadableState = await getLoadableState(rootApp);

    // Dispatch a close event so sagas stop listening after they're resolved
    store.close();
};

app.get('*', renderApp);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`☄️ SSR listen on port ${port}`));
