import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { ThemeProvider } from 'emotion-theming';

import App from '../shared/app';
import withStyleAndStore from '../shared/app/withStyleAndStore';

export const renderHeader = helmet => `
    <!DOCTYPE html>
    <html lang="fr" style="height: 100%;">
        <head>
            <meta charset="UTF-8">
            ${helmet.title.toString()}
            ${helmet.meta.toString()}
            ${helmet.link.toString()}
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="icon" type="image/png" href="/assets/favicon.ico" />
        </head>
        <body ${helmet.bodyAttributes.toString()} style="height: 100%;margin: 0;">
            <div id="root" style="height: 100%;">
`;

export const renderFooter = (css, loadableState, preloadedState) => `
            </div>
            <script>
                // WARNING: See the following for security issues around embedding JSON in HTML:
                // http://redux.js.org/docs/recipes/ServerRendering.html#security-considerations
                window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
            </script>
            <style id="jss-server-side">${css}</style>
            <script src="/assets/vendor.js"></script>
            <script src="/assets/client.js"></script>
            ${loadableState.getScriptTag()}
        </body>
    </html>
`;

export const getAppWithRouter = (store, url, context) => {
    // We wrap App into ThemeProvider from emotion-theming even
    // if we don't use their themes because there is an error from their module
    // cf. https://github.com/emotion-js/emotion/issues/467
    const AppWithRouter = (
        <StaticRouter location={url} context={context}>
            <ThemeProvider theme={{}}>
                <App />
            </ThemeProvider>
        </StaticRouter>
    );
    const { AppWithStyleAndStore, sheetsRegistry } = withStyleAndStore(AppWithRouter, store);

    return ({
        rootApp: AppWithStyleAndStore,
        sheetsRegistry,
    });
};
