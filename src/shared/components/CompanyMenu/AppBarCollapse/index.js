import { branch, compose, withPropsOnChange, withState, renderNothing } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { withStyles } from '@material-ui/core';

import Content from './content';
import type { EntitiesStructure } from '../../../redux/types';
import { companyType } from '../../../helpers/session/constants';
import { styles } from './styles';

const mapStateToProps = ({ entities }: { entities: EntitiesStructure }) => ({
    userType: path(['session', 'userType'], entities),
});

const connectToStore = connect(mapStateToProps, null);

const initAuthorisation = ({ setIsAuthenticatedAsCompany, userType = '' }) => {
    setIsAuthenticatedAsCompany(userType.toString() === companyType);
};

const container = compose(
    connectToStore,
    withState('isAuthenticatedAsCompany', 'setIsAuthenticatedAsCompany', false),
    withPropsOnChange(['userType'], initAuthorisation),
    withStyles(styles),
    branch(({ isAuthenticatedAsCompany }) => !isAuthenticatedAsCompany, renderNothing),
)(Content);

export default container;
