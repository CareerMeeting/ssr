const styles = theme => ({
    buttonCollapse: {
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
        margin: '10px',
        boxShadow: 'none',
    },
});

export default styles;
