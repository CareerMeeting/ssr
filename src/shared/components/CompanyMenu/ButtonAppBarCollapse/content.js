import IconButton from '@material-ui/icons/Menu';
import React from 'react';
import { func, object } from 'prop-types';
import { withStyles } from '@material-ui/core/';

import styles from './styles';

const ButtonAppBarCollapse = ({ classes, toggleCollapse }) => (
    <IconButton className={classes.buttonCollapse} onClick={toggleCollapse}>
        <b className="mdi mdi-home" />
    </IconButton>
);

ButtonAppBarCollapse.propTypes = {
    classes: object,
    toggleCollapse: func.isRequired,
};

ButtonAppBarCollapse.defaultProps = {
    classes: {},
};

export default withStyles(styles)(ButtonAppBarCollapse);
