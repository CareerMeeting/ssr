import { compose, withState, withHandlers, withPropsOnChange } from 'recompose';
import { connect } from 'react-redux';
import { find, pathOr, path, equals, reject } from 'ramda';
import { withStyles } from '@material-ui/core';

import Content from './content';
import { updateCompanySlotByID } from '../../../redux/user/actions';
import { styles } from './styles';
import { getQuery } from '../../../helpers/navigation/index';

const handlers = {
    handleCanEditCapacitySlot: ({ linkedStudents = [] }) => school => !!find(linkedStudent => school === linkedStudent.school, linkedStudents),
    handleActivationSlotChange: ({ setIsEnabledSlot, setHasUpdatedSlot }) => (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        setIsEnabledSlot(!!value);
        setHasUpdatedSlot(true);
    },
    handleSchoolsTargetedChange: ({ setSchoolsTargeted, setHasUpdatedSlot, isFeatureCompanyCanChangeCapacitySchoolSlotEnabled }) => (event, school) => {
        const capacityFromInput = pathOr(0, ['target', 'value'], event);
        const capacity = (!isFeatureCompanyCanChangeCapacitySchoolSlotEnabled && capacityFromInput > 1) ? 1 : capacityFromInput;

        setHasUpdatedSlot(true);
        setSchoolsTargeted(prev => [
            ...reject(s => equals(school)(s.school), prev),
            {
                school,
                capacity,
            },
        ]);
    },
    handleEditMode: ({ setIsInEditMode }) => () => {
        setIsInEditMode(prev => !prev);
    },
    handleValidateSlotEdited: ({ _id, setIsInEditMode, updateCompanySlotByIdAction, isEnabledSlot, schoolsTargetedSlot }) => () => {
        updateCompanySlotByIdAction(_id, { isEnabled: isEnabledSlot, schoolsTargeted: schoolsTargetedSlot });
        setIsInEditMode(prev => !prev);
    },
    handleCancelSlotEdited: ({ setIsInEditMode, setSchoolsTargeted, setIsEnabledSlot, setHasUpdatedSlot, isEnabled, schoolsTargeted }) => () => {
        setSchoolsTargeted(schoolsTargeted);
        setIsEnabledSlot(isEnabled);
        setHasUpdatedSlot(false);
        setIsInEditMode(prev => !prev);
    },
};

const mapStateToProps = ({ entities }) => ({
    isLoading: path(['companies', 'isLoading'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    {
        updateCompanySlotByIdAction: updateCompanySlotByID,
    },
);

const initSlot = ({ setIsEnabledSlot, setSchoolsTargeted, isEnabled, schoolsTargeted }) => {
    setIsEnabledSlot(isEnabled);
    setSchoolsTargeted(schoolsTargeted);
};

const container = compose(
    connectToStore,
    withState('hasUpdatedSlot', 'setHasUpdatedSlot', false),
    withState('isEnabledSlot', 'setIsEnabledSlot', false),
    withState('isInEditMode', 'setIsInEditMode', false),
    withState('schoolsTargetedSlot', 'setSchoolsTargeted', []),
    withPropsOnChange(['isEnabled', 'schoolsTargeted'], initSlot),
    withState('isDialogOpen', 'setDialogState', false),
    withHandlers(handlers),
    withStyles(styles),
);

export default container(Content);
