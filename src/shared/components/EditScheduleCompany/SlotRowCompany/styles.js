import styled from 'react-emotion';
import { TableCell } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import InfoIcon from '@material-ui/icons/Info';

import { green, info, white, red } from '../../../app/colors';

export const styles = theme => ({
    editIcon: {
        color: theme.palette.secondary.main,
    },
    tableCellCenter: {
        textAlign: 'center',
    },
});

export const TableCellCentered = styled(TableCell)({
    textAlign: 'center',
});

export const TableCellSchool = styled(TableCell)(({ capacity }) => ({
    textAlign: 'center',
    backgroundColor: (capacity > 0) && info,
    color: (capacity > 0) && white,
}));

export const IconCheck = styled(CheckIcon)({
    color: green,
});

export const IconCancel = styled(CancelIcon)({
    color: red,
});

export const IconInfo = styled(InfoIcon)({
    color: red,
});
