import React from 'react';
import { find, equals } from 'ramda';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Switch from '@material-ui/core/Switch/Switch';
import TextField from '@material-ui/core/TextField/TextField';
import TableRow from '@material-ui/core/TableRow/TableRow';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';

import { IconCancel, IconCheck, IconInfo, TableCellCentered, TableCellSchool } from './styles';
import { ActiveCircleIcon, DeactivateCircleIcon } from '../../StudentsListTable/styles';
import { TableCellCheckbox } from '../styles';

const SchedulerInEditMode = (
    {
        _id,
        startHour,
        isInEditMode,
        isEnabledSlot,
        isSelected,
        schoolsTargetedSlot = [],
        handleSelectSlot,
        schools = [],
        handleEditMode,
        handleActivationSlotChange,
        handleSchoolsTargetedChange,
        handleCancelSlotEdited,
        handleValidateSlotEdited,
        isFeatureCompanyCanDeactivateSlotEnabled,
        handleCanEditCapacitySlot,
        canSchedule,
        classes,
    } : {
        _id: String,
        startHour: String,
        isInEditMode: boolean,
        canSchedule: Boolean,
        isEnabledSlot: boolean,
        handleCanEditCapacitySlot: Function,
        isFeatureCompanyCanDeactivateSlotEnabled: Boolean,
        isSelected: boolean,
        schoolsTargetedSlot: Array,
        handleSelectSlot: Function,
        schools: Array,
        handleEditMode: Function,
        handleActivationSlotChange: Function,
        handleSchoolsTargetedChange: Function,
        handleCancelSlotEdited: Function,
        handleValidateSlotEdited: Function,
        classes: any,
    }) =>
    (<TableRow
        hover
        aria-checked={isSelected}
        tabIndex={-1}
        key={_id}
        selected={isSelected}
    >
        {canSchedule && (
            <TableCellCheckbox
                padding="checkbox"
                onClick={event => handleSelectSlot(event, _id)}
            >
                <Checkbox checked={isSelected} />
            </TableCellCheckbox>
        )}
        <TableCellCentered
            className={classes.tableCellCenter}
            component="th"
            scope="row"
            padding="none"
        >
            {startHour}
        </TableCellCentered>
        {isFeatureCompanyCanDeactivateSlotEnabled && (
            <TableCellCentered
                className={classes.tableCellCenter}
                component="th"
                scope="row"
                padding="none"
            >
                {!isInEditMode && (isEnabledSlot ? <ActiveCircleIcon /> : <DeactivateCircleIcon />)}
                {isInEditMode && <Switch onChange={handleActivationSlotChange} checked={isEnabledSlot} value={isEnabledSlot} />}
            </TableCellCentered>
        )}
        {schools.map((school) => {
            const schoolTargeted = find(s => equals(school)(s.school), schoolsTargetedSlot);
            const capacity = schoolTargeted ? schoolTargeted.capacity : 0;
            const canEditCapacitySlot = handleCanEditCapacitySlot(school);
            if (isInEditMode) {
                return (
                    <TableCellSchool className={classes.tableCellCenter} key={`${school}_${_id}_edit`}>
                        {canEditCapacitySlot && (
                            <Tooltip title={`Impossible de changer la capacité d'accueil pour l'école ${school.toUpperCase()} car un étudiant
                             s'est déjà inscris à ce créneau. Si vous souhaitez changer la capacité de ce créneau pour cette école vous devez d'abord
                             contacter l'administrateur de cette école pour qu'il désinscrive cet étudiant.`}>
                                <IconInfo />
                            </Tooltip>
                        )}
                        <TextField
                            id={school}
                            label="Capacité"
                            disabled={handleCanEditCapacitySlot(school)}
                            value={capacity}
                            onChange={event => handleSchoolsTargetedChange(event, school)}
                            type="number"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                            variant="filled"
                        />
                    </TableCellSchool>
                );
            }

            return (
                <TableCellSchool className={classes.tableCellCenter} key={`${school}_${_id}`} capacity={capacity}>
                    {capacity}
                </TableCellSchool>
            );
        })}
        {canSchedule && (
            <TableCellCentered className={classes.tableCellCenter}>
                {!isInEditMode && (
                    <IconButton onClick={handleEditMode} >
                        <EditIcon fontSize="small" className={classes.editIcon} />
                    </IconButton>
                )}
                {isInEditMode && (
                    <div>
                        <IconButton onClick={handleValidateSlotEdited} alt="Valider ce créneau">
                            <IconCheck />
                        </IconButton>
                        <IconButton onClick={handleCancelSlotEdited} >
                            <IconCancel />
                        </IconButton>
                    </div>
                )}
            </TableCellCentered>
        )}
    </TableRow>);

export default SchedulerInEditMode;
