import { compose, withState, withHandlers, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { map, path, contains, equals, reject, pathOr } from 'ramda';
import { withStyles } from '@material-ui/core';
import moment from 'moment';

import Content from './content';
import { styles } from './styles';
import { getQuery } from '../../helpers/navigation';
import { enableCompanySlots, fetchCompanySchools } from '../../redux/user/actions';

const handlersHelpers = {
    isSlotSelected: ({ slotsSelected }) => id => contains(id, slotsSelected),
};

const handlers = {
    handleSelectSlot: ({ setSlotsSelected, isSlotSelected }) => (event, id) => {
        if (isSlotSelected(id)) {
            setSlotsSelected(prev => reject(equals(id), prev));
            return;
        }

        setSlotsSelected(prev => [
            ...prev,
            id,
        ]);
    },
    handleSelectAllSlots: ({ setSlotsSelected, slots = [] }) => (event) => {
        if (event.target.checked) {
            setSlotsSelected(map(({ _id }) => _id, slots));
            return;
        }
        setSlotsSelected([]);
    },
    handleToggleDialogMakeAvailableSlots: ({ setDialogMakeAvailableOpen }) => () => {
        setDialogMakeAvailableOpen(prev => !prev);
    },
    handleMakeAvailableSelectedSlots: ({ enableCompanySlotsAction, setSlotsSelected, slotsSelected, setDialogMakeAvailableOpen }) => () => {
        enableCompanySlotsAction(slotsSelected);
        setDialogMakeAvailableOpen(prev => !prev);
        setSlotsSelected([]);
    },
};

const mapStateToProps = ({ entities }, ownerProps) => {
    const { id } = getQuery(path(['location'], ownerProps));
    const companyDataPath = id ? ['companies', id] : ['user'];
    const isFeatureCompanyCanChangeCapacitySchoolSlotEnabled =
        path(['webConfig', 'features', 'isFeatureCompanyCanChangeCapacitySchoolSlotEnabled'], entities);
    const eventDate = pathOr(new Date(), ['webConfig', 'eventDate'], entities);
    const maxAbilityToSubscribeBeforeEventDate = 4;
    const maxDateToSubscribe = moment(eventDate).subtract(maxAbilityToSubscribeBeforeEventDate, 'days').utc();
    const currentDate = moment();
    const canSchedule = currentDate.isSameOrBefore(maxDateToSubscribe, 'day');


    return ({
        canSchedule,
        isLoading: path(['companies', 'isLoading'], entities),
        schools: path([...companyDataPath, 'schools'], entities),
        slots: path([...companyDataPath, 'slots'], entities),
        isFeatureCompanyCanDeactivateSlotEnabled: path(['webConfig', 'features', 'isFeatureCompanyCanDeactivateSlotEnabled'], entities),
        isFeatureCompanyCanChangeCapacitySchoolSlotEnabled,
    });
};

const lifecyclePayload = {
    componentDidMount() {
        this.props.fetchCompanySchools();
    },
};

const connectToStore = connect(
    mapStateToProps,
    {
        enableCompanySlotsAction: enableCompanySlots,
        fetchCompanySchools,
    },
);

const container = compose(
    connectToStore,
    lifecycle(lifecyclePayload),
    withState('slotsSelected', 'setSlotsSelected', []),
    withState('isDialogMakeAvailableOpen', 'setDialogMakeAvailableOpen', false),
    withHandlers(handlersHelpers),
    withHandlers(handlers),
    withStyles(styles),
);

export default container(Content);
