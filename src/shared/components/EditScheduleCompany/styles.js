import styled from 'react-emotion';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import { TableCell } from '@material-ui/core';
import { info, white } from '../../app/colors';

export const styles = theme => ({
    table: {
        maxWidth: 2000,
    },
    thCheckbox: {
        width: 48,
        borderRight: 'grey solid 0.1px',
    },
});

export const stylesTableHead = theme => ({
    thead: {
        backgroundColor: theme.palette.primary.main,
    },
    checkbox: {
        color: white,
    },
    tableCellCentered: {
        color: white,
        textAlign: 'center',
    },
});

export const TableCellCheckbox = styled(TableCell)({
    width: 48,
    borderRight: 'grey solid 0.1px',
});

export const TableCellCentered = styled(TableCell)({
    textAlign: 'center',
});

export const TableCellSchool = styled(TableCell)(({ isFilled }) => ({
    textAlign: 'center',
    backgroundColor: isFilled && info,
    color: isFilled && white,
}));

export const TableWrapper = styled('div')({
    overflowX: 'auto',
});

export const IconPlus = styled(AddIcon)({
    marginLeft: 5,
});

export const Field = styled(TextField)({
    minWidth: 300,
    margin: '6% 0',
    display: 'grid',
});

export const CenterBlockButtons = styled('div')({
    display: 'flex',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 20,
});
