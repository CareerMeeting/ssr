import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import { length } from 'ramda';

import { TableCellCheckbox, TableWrapper, CenterBlockButtons, stylesTableHead } from './styles';
import SlotRowCompany from './SlotRowCompany';
import { PaperStyled } from '../CompaniesListTable/styles';
import { Body } from '../../views/AdminList/styles';
import { ButtonValidate, IconCheck } from '../../views/SuperAdminSettings/styles';
import Dialog from '../Dialog';

const getDialogMakeAvailableSlots = () => (
    <div>
        <p>En rendant disponible tous les créneaux sélectionnés, vous accueillerez un étudiant pour chacun des créneaux sélectionnés.</p>
    </div>
);

const TableHeadSchedulerEditMode = (
    {
        classes,
        canSchedule,
        handleSelectAllSlots,
        numberOfSlotsSelected,
        numberOfSlots,
        schools = [],
        isFeatureCompanyCanDeactivateSlotEnabled,
    }) =>
    (<TableHead className={classes.thead}>
        <TableRow>
            {canSchedule && (
                <TableCellCheckbox padding="checkbox" >
                    <Checkbox
                        className={classes.checkbox}
                        indeterminate={numberOfSlotsSelected > 0 && numberOfSlotsSelected < numberOfSlots}
                        checked={numberOfSlotsSelected === numberOfSlots}
                        onChange={handleSelectAllSlots}
                    />
                </TableCellCheckbox>
            )}
            <TableCell className={classes.tableCellCentered}>Horaires</TableCell>
            {isFeatureCompanyCanDeactivateSlotEnabled && (<TableCell className={classes.tableCellCentered}>Disponible</TableCell>)}
            {schools.map(school => (
                <TableCell className={classes.tableCellCentered} key={school}>{`Capacité d'étudiants de ${school.toUpperCase()} à accueillir`}</TableCell>
            ))}
            {canSchedule && (
                <TableCell className={classes.tableCellCentered}>Actions</TableCell>
            )}
        </TableRow>
    </TableHead>);

const TableHeadSchedulerEditModeStyled = withStyles(stylesTableHead)(TableHeadSchedulerEditMode);

const SchedulerInEditMode = (
    {
        classes,
        canSchedule,
        handleMakeAvailableSelectedSlots,
        handleSelectAllSlots,
        handleSelectSlot,
        handleToggleDialogMakeAvailableSlots,
        isDialogMakeAvailableOpen,
        isSlotSelected,
        schools = [],
        slots = [],
        slotsSelected = [],
        isFeatureCompanyCanDeactivateSlotEnabled,
        isFeatureCompanyCanChangeCapacitySchoolSlotEnabled,
    } : {
        isFeatureCompanyCanDeactivateSlotEnabled: Boolean,
        isFeatureCompanyCanChangeCapacitySchoolSlotEnabled: Boolean,
        classes: any,
        canSchedule: Boolean,
        handleMakeAvailableSelectedSlots: Function,
        handleSelectAllSlots: Function,
        handleSelectSlot: Function,
        handleToggleDialogMakeAvailableSlots: Function,
        isDialogMakeAvailableOpen: boolean,
        isSlotSelected: boolean,
        schools: Array,
        slots: Array,
        slotsSelected: Array,
    }) =>
    (<Body>
        <PaperStyled>
            <TableWrapper>
                <Table className={classes.table} aria-labelledby="tableEditScheduleCompany" >
                    <TableHeadSchedulerEditModeStyled
                        handleSelectAllSlots={handleSelectAllSlots}
                        numberOfSlotsSelected={length(slotsSelected)}
                        numberOfSlots={length(slots)}
                        schools={schools}
                        canSchedule={canSchedule}
                        isFeatureCompanyCanDeactivateSlotEnabled={isFeatureCompanyCanDeactivateSlotEnabled}
                    />
                    <TableBody>
                        {slots.map(({ _id, startHour, isEnabled, schoolsTargeted = [], linkedStudents = [] }) => (
                            <SlotRowCompany
                                key={_id}
                                _id={_id}
                                canSchedule={canSchedule}
                                startHour={startHour}
                                linkedStudents={linkedStudents}
                                schools={schools}
                                isSelected={isSlotSelected(_id)}
                                schoolsTargeted={schoolsTargeted}
                                isEnabled={isEnabled}
                                handleSelectSlot={handleSelectSlot}
                                isFeatureCompanyCanDeactivateSlotEnabled={isFeatureCompanyCanDeactivateSlotEnabled}
                                isFeatureCompanyCanChangeCapacitySchoolSlotEnabled={isFeatureCompanyCanChangeCapacitySchoolSlotEnabled}
                            />
                        ))}
                    </TableBody>
                </Table>
            </TableWrapper>
        </PaperStyled>
        {canSchedule && (
            <CenterBlockButtons>
                <ButtonValidate
                    variant="outlined"
                    color="secondary"
                    onClick={handleToggleDialogMakeAvailableSlots}
                    disabled={length(slotsSelected) === 0}
                >
                    Rendre disponible les créneaux séléctionnés <IconCheck />
                </ButtonValidate>
                <Dialog
                    open={isDialogMakeAvailableOpen}
                    title={'Êtes vous sur d\'être disponible pour tous les créneaux sélectionnés ?'}
                    content={getDialogMakeAvailableSlots()}
                    handleOk={handleMakeAvailableSelectedSlots}
                    handleCancel={handleToggleDialogMakeAvailableSlots}
                />
            </CenterBlockButtons>
        )}
    </Body>);

export default SchedulerInEditMode;
