import styled from 'react-emotion';
import Grid from '@material-ui/core/Grid/Grid';
import DownloadIcon from '@material-ui/icons/CloudDownload';
import CalendarIcon from '@material-ui/icons/CalendarToday';
import { NavLink } from 'react-router-dom';
import Typography from '@material-ui/core/Typography/Typography';

export const Body = styled(Grid)({
    marginBottom: 10,
});

export const LinkedinButton = styled('a')({
    marginBottom: 10,
});

export const Title = styled(Typography)({
    textAlign: 'center',
    marginTop: '1.35em',
});

export const CenterBlock = styled('div')({
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    marginBottom: 10,
});

export const SeePlanningLink = styled(NavLink)({
    color: 'inherit',
    textDecoration: 'none',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
});

export const SettingItem = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 10,
    paddingTop: 10,
});

export const Separator = styled('hr')({
    width: 160,
    marginTop: 5,
});

export const styles = theme => ({
    profilePicture: {
        backgroundColor: theme.palette.secondary.main,
        width: 80,
        height: 80,
    },
    icon: {
        position: 'relative',
        top: 6,
        color: theme.palette.secondary.main,
    },
    hr: {
        width: '50%',
        margin: '4% 0%',
    },
});

export const IconDownload = styled(DownloadIcon)({
    marginLeft: 5,
});

export const IconPlanning = styled(CalendarIcon)({
    marginLeft: 5,
});
