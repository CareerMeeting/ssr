// @flow
import React from 'react';
import Grid from '@material-ui/core/Grid/Grid';
import Avatar from '@material-ui/core/Avatar/Avatar';
import classNames from 'classnames';
import IconButton from '@material-ui/core/IconButton/IconButton';
import PhoneIcon from '@material-ui/icons/PhoneOutlined';
import SchoolIcon from '@material-ui/icons/School';
import EmailIcon from '@material-ui/icons/Email';

import type { FunctionnalComponent } from '../../app/types';
import { Body, IconDownload, IconPlanning, SeePlanningLink, Title, CenterBlock } from './styles';
import { isNilOrEmpty } from '../../helpers/utils';
import LinkedinLogo from '../../assets/Images/linkedin-logo.png';
import { ButtonValidate } from '../../views/SuperAdminSettings/styles';

const View = (
    {
        id,
        email,
        firstName = '',
        lastName = '',
        linkedInLink,
        degreeLevel,
        organizationName,
        phoneNumber,
        classes,
        className,
        handleFetchCV,
        isAuthenticatedAsAdminUser,
    }
: {
        id: String,
        classes: Object,
        className: any,
        firstName: String,
        lastName: String,
        email: String,
        linkedInLink: String,
        organizationName: String,
        degreeLevel: String,
        phoneNumber: String,
        handleFetchCV: Function,
        isAuthenticatedAsAdminUser: boolean,
}): FunctionnalComponent => (
    <Body item container alignItems="center" justify="center" direction="column">
        <Grid item>
            <Avatar className={classNames(classes.profilePicture, className)} >
                { firstName.substring(0, 1)}
                { lastName.substring(0, 1)}
            </Avatar>
        </Grid>
        {linkedInLink && (
            <Grid item>
                <IconButton href={linkedInLink} target="_blank">
                    <img src={LinkedinLogo} width={25} title="linkedin" alt="linkedin" />
                </IconButton>
            </Grid>
        )}
        <Grid item>
            <Title gutterBottom variant="h5">
                { `${firstName.toLocaleUpperCase()} ${lastName.toUpperCase()}` }
            </Title>
        </Grid>
        {!isNilOrEmpty(phoneNumber) && (
            <div><PhoneIcon className={classNames(classes.icon, className)} /> { phoneNumber } </div>
        )}
        <hr className={classNames(classes.hr, className)} />
        <div><EmailIcon className={classNames(classes.icon, className)} /> { email } </div>
        <hr className={classNames(classes.hr, className)} />
        <div><SchoolIcon className={classNames(classes.icon, className)} /> { organizationName } </div>
        <hr className={classNames(classes.hr, className)} />
        <div><SchoolIcon className={classNames(classes.icon, className)} /> { degreeLevel || 'N/A' } </div>
        <hr className={classNames(classes.hr, className)} />
        <CenterBlock>
            <ButtonValidate variant="outlined" color="secondary" onClick={handleFetchCV}>
                Télécharger le CV <IconDownload />
            </ButtonValidate>
            {isAuthenticatedAsAdminUser && (
                <Grid item >
                    <ButtonValidate variant={'outlined'} color="secondary">
                        <SeePlanningLink to={`/student/planning?id=${id}`}>Voir Son Planning <IconPlanning /></SeePlanningLink>
                    </ButtonValidate>
                </Grid>
            )}
        </CenterBlock>
    </Body>
);

export default View;
