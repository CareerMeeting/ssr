import { compose, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';

import Content from './content';
import { fetchStudentCV } from '../../redux/student/actions';
import { styles } from './styles';
import { isAdminUser, isCompanyUser, isStudentUser } from '../../helpers/session';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import { getQuery } from '../../helpers/navigation';
import { path } from 'ramda';

const handlers = {
    handleFetchCV: ({ fetchStudentCVAPI, id, firstName, lastName }) => () => {
        if (!id) return;

        fetchStudentCVAPI(id, firstName, lastName);
    },
};


const mapStateToProps = ({ entities } : { entities: EntitiesStructure }) => {
    const userType = path(['session', 'userType'], entities);

    return ({
        isAuthenticatedAsAdminUser: isAdminUser(userType),
    });
};

const connectToStore = connect(
    mapStateToProps,
    {
        fetchStudentCVAPI: fetchStudentCV,
    },
);

const container = compose(
    connectToStore,
    withHandlers(handlers),
    withStyles(styles),
)(Content);

export default container;
