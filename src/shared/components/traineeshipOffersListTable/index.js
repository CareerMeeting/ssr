import withStyles from '@material-ui/core/styles/withStyles';
import { compose, withState, withHandlers, withPropsOnChange } from 'recompose';
import { path } from 'ramda';
import { connect } from 'react-redux';

import traineeshipOffersTable from './content';
import { styles } from './styles';

import {
    addTraineeshipOffers,
    getTraineeshipOffers,
    upadteTraineeshipOffers,
    deleteTraineeshipOffers,
} from '../../redux/company/actions';

const mapStateToProps = ({ entities, companies }, { userID }) => ({
    schools: path(['webConfig', 'schools'], entities),
    userID: userID || path(['user', '_id'], entities),
    isLoading: path(['isLoading'], companies),
    result: path(['result'], companies),
});

const container = compose(
    connect(
        mapStateToProps,
        {
            addTraineeshipOffersAPI: addTraineeshipOffers,
            getTraineeshipOffersAPI: getTraineeshipOffers,
            updateTraineeshipOffersAPI: upadteTraineeshipOffers,
            deleteTraineeshipOffersAPI: deleteTraineeshipOffers,
        },
    ),
    withState('isUpdating', 'setIsUpdating', null),
    withState('isAdding', 'setIsAdding', null),
    withState('currentPos', 'setCurrentPos', -1),
    withState('form', 'setForm', {}),
    withState('schoolsSelected', 'setSchoolsSelection', []),
    withState('isOfferDialogOpen', 'setOfferDialog', false),
    withHandlers({
        handleChangeSchoolSelect: ({ setSchoolsSelection }) => (event) => {
            setSchoolsSelection(event.target.value);
        },
        toggleOfferDialog: ({ setOfferDialog }) => () => {
            setOfferDialog(dialog => !dialog);
        },
        deleteRow: ({ deleteTraineeshipOffersAPI }) => (traineeshipOfferID) => {
            deleteTraineeshipOffersAPI(traineeshipOfferID);
        },
        handleChange: ({ setForm }) => (event) => {
            const target = event.target;
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const name = target.name;

            setForm(form => ({
                ...form,
                [name]: value,
            }));
        },
    }),
    withHandlers({
        selectRow:
            ({
                getTraineeshipOffersAPI,
                userID,
                setCurrentPos,
                setIsUpdating,
                setForm,
                setSchoolsSelection,
                setIsAdding,
            }) => (traineeshipOffer, pos) => {
                getTraineeshipOffersAPI(userID, traineeshipOffer._id, pos);
                const { schoolsTargeted, ...offer } = traineeshipOffer;
                setCurrentPos(pos);
                setForm(offer);
                setSchoolsSelection(schoolsTargeted);
                setIsUpdating(true);
                setIsAdding(false);
            },
        updateTraineeshipOffer: ({ updateTraineeshipOffersAPI, form, schoolsSelected }) =>
            (traineeshipOfferID, pos) => {
                updateTraineeshipOffersAPI(traineeshipOfferID, pos, form, schoolsSelected.join(';'));
            },
        closeOfferDialog: ({ setIsAdding, setIsUpdating, setForm, setSchoolsSelection, toggleOfferDialog }) => () => {
            setIsAdding(false);
            setIsUpdating(false);
            setForm({});
            setSchoolsSelection([]);
            toggleOfferDialog();
        },
    }),
    withHandlers({
        addNewOffer: ({ setIsAdding, setIsUpdating, toggleOfferDialog }) => () => {
            setIsAdding(true);
            setIsUpdating(false);
            toggleOfferDialog();
        },
        addOffer: ({ closeOfferDialog, addTraineeshipOffersAPI }) => (form, schoolsSelected) => {
            addTraineeshipOffersAPI(form, schoolsSelected.join(';'));
            closeOfferDialog();
        },
    }),
    withPropsOnChange(['isLoading'], ({ setForm, currentPos, traineeships, result, toggleOfferDialog, isAdding, isUpdating, isLoading }) => {
        if (!isLoading && result === 'success' && (isAdding || isUpdating)) {
            toggleOfferDialog();
            setForm(traineeships[currentPos]);
        }
    }),
    withStyles(styles),
);

export default container(traineeshipOffersTable);
