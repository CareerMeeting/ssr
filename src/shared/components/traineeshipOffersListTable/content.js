import React from 'react';
import classNames from 'classnames';
import AddIcon from '@material-ui/icons/AddCircleOutline';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper/Paper';
import Table from '@material-ui/core/Table/Table';
import TableHead from '@material-ui/core/TableHead/TableHead';
import TableRow from '@material-ui/core/TableRow/TableRow';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableBody from '@material-ui/core/TableBody/TableBody';
import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import Typography from '@material-ui/core/Typography/Typography';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';
import Dialog from '@material-ui/core/Dialog/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import DialogActions from '@material-ui/core/DialogActions/DialogActions';
import Button from '@material-ui/core/Button/Button';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import FormControl from '@material-ui/core/FormControl/FormControl';
import InputLabel from '@material-ui/core/InputLabel/InputLabel';
import Select from '@material-ui/core/Select/Select';
import { companyType } from '../../helpers/session/constants';

import { Field } from './styles';
import type { FunctionnalComponent } from '../../app/types';


const AddOfferDialog = (
    {
        classes,
        className,
        traineeships = [],
        isOfferDialogOpen,
        closeOfferDialog,
        schools = [],
        schoolsSelected,
        handleChangeSchoolSelect,
        handleChange,
        addOffer,
        form,
        isUpdating,
        isAdding,
        updateTraineeshipOffer,
        currentPos,
        userType,
    }
: {
        classes: Object,
        className: Function,
        traineeships: Array,
        addOffer: Function,
        closeOfferDialog: Function,
        isOfferDialogOpen: Function,
        schools: Array,
        schoolsSelected: Array,
        handleChangeSchoolSelect: Function,
        handleChange: Function,
        form: Object,
        updateTraineeshipOffer: Function,
        isUpdating: Function,
        isAdding: Function,
        currentPos: number,
        userType: string,
}) : FunctionnalComponent => (
    <Dialog
        open={isOfferDialogOpen}
        onClose={closeOfferDialog}
    >
        <DialogTitle>
            {isAdding && 'Ajouter une offre de stage'}
            {isUpdating && 'Modifier l\'offre de stage'}
        </DialogTitle>
        <DialogContent>
            <Field
                autoFocus
                defaultValue={form.title}
                margin="dense"
                name="title"
                label="Titre"
                onChange={handleChange}
                fullWidth
                disabled={userType !== companyType}
            />
            <Field
                defaultValue={form.levelRequired}
                margin="dense"
                name="levelRequired"
                label="Niveau requis"
                onChange={handleChange}
                fullWidth
                disabled={userType !== companyType}
            />
            <FormControl className={classNames(classes.multiSelect, className)} fullWidth disabled={userType !== companyType} >
                <InputLabel>École(s)</InputLabel>
                <Select
                    name="schools"
                    multiple
                    value={schoolsSelected}
                    onChange={handleChangeSchoolSelect}
                    renderValue={selected => selected.join(', ')}
                >
                    {schools.map(school => (
                        <MenuItem key={school} value={school}>
                            <Checkbox checked={schoolsSelected.indexOf(school) > -1} />
                            <ListItemText primary={school} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl><br />
            <Field
                defaultValue={form.description}
                name="description"
                label="Description de l'entreprise"
                multiline
                rows="4"
                fullWidth
                margin="normal"
                onChange={handleChange}
                variant="outlined"
                disabled={userType !== companyType}
            />
        </DialogContent>
        <DialogActions>
            <Button onClick={closeOfferDialog} color="primary">
                { (userType !== companyType) ? 'Fermer' : 'Annuler' }
            </Button>
            { (userType === companyType) && isAdding &&
            <Button color="primary" onClick={() => { addOffer(form, schoolsSelected); }}>
                Ajouter
            </Button>}
            { (userType === companyType) && isUpdating &&
            <Button
                color="primary"
                onClick={() => { updateTraineeshipOffer(form._id, traineeships.indexOf(currentPos)); }}
            >
                Mettre à Jour
            </Button> }
        </DialogActions>
    </Dialog>
);

const TraineeshipOffersTable = (
    {
        classes,
        className,
        traineeships,
        addNewOffer,
        addOffer,
        closeOfferDialog,
        isOfferDialogOpen,
        schools,
        schoolsSelected,
        handleChangeSchoolSelect,
        handleChange,
        form,
        selectRow,
        updateTraineeshipOffer,
        deleteRow,
        isUpdating,
        isAdding,
        currentPos,
        userType,
    }
: {
        classes: Object,
        className: Function,
        traineeships: Array,
        addNewOffer: Function,
        addOffer: Function,
        closeOfferDialog: Function,
        isOfferDialogOpen: Function,
        schools: Array,
        schoolsSelected: Array,
        handleChangeSchoolSelect: Function,
        handleChange: Function,
        form: Object,
        selectRow: Function,
        updateTraineeshipOffer: Function,
        deleteRow: Function,
        isUpdating: Function,
        isAdding: Function,
        currentPos: number,
        userType: string,
}) : FunctionnalComponent => (
    <Paper className={classNames(classes.root, className)}>
        <Toolbar>
            <div>
                <Typography variant="h6" id="tableTitle">
                    Offres de stage
                </Typography>
            </div>
            <div className={classNames(classes.spacer, className)} />
            { userType === companyType &&
            (
                <div>
                    <Tooltip title="Ajouter une Offre">
                        <IconButton onClick={addNewOffer}>
                            <AddIcon />
                        </IconButton>
                    </Tooltip>
                </div>
            )}
        </Toolbar>
        <Table className={classNames(classes.table, className)}>
            <TableHead>
                <TableRow>
                    <TableCell> Titre </TableCell>
                    <TableCell numeric>Niveau requis</TableCell>
                    <TableCell numeric>Écoles</TableCell>
                    { userType === companyType && <TableCell numeric /> }
                </TableRow>
            </TableHead>
            <TableBody>
                {traineeships.map(traineeship => (
                    <TableRow key={traineeship._id} hover>
                        <TableCell
                            component="th"
                            scope="row"
                            onClick={() => selectRow(traineeship, traineeships.indexOf(traineeship))}
                        >
                            {traineeship.title}
                        </TableCell>
                        <TableCell
                            numeric
                            onClick={() => selectRow(traineeship, traineeships.indexOf(traineeship))}
                        >
                            {traineeship.levelRequired}
                        </TableCell>
                        <TableCell
                            numeric
                            onClick={() => selectRow(traineeship, traineeships.indexOf(traineeship))}
                        >
                            {traineeship.schoolsTargeted.join(', ')}
                        </TableCell>
                        { userType === companyType &&
                        <TableCell numeric>
                            <IconButton onClick={() => deleteRow(traineeship._id)}> <DeleteIcon /> </IconButton>
                        </TableCell> }
                    </TableRow>
                ))}
            </TableBody>
        </Table>
        <AddOfferDialog
            classes={classes}
            className={className}
            traineeships={traineeships}
            isOfferDialogOpen={isOfferDialogOpen}
            closeOfferDialog={closeOfferDialog}
            schools={schools}
            handleChange={handleChange}
            handleChangeSchoolSelect={handleChangeSchoolSelect}
            schoolsSelected={schoolsSelected}
            addOffer={addOffer}
            form={form}
            isUpdating={isUpdating}
            isAdding={isAdding}
            updateTraineeshipOffer={updateTraineeshipOffer}
            currentPos={currentPos}
            userType={userType}
        />
    </Paper>
);

export default TraineeshipOffersTable;
