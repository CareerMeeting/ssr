import styled from 'react-emotion';
import TextField from '@material-ui/core/TextField/TextField';

export const Field = styled(TextField)({
    minWidth: '31% !important',
    maxWidth: '97%',
    margin: '0% 1% !important',
});


export const styles = theme => ({
    profilePicture: {
        backgroundColor: theme.palette.secondary.main,
        width: 80,
        height: 80,
        marginLeft: '36%',
    },
    icon: {
        position: 'relative',
        top: 6,
    },
    hr: {
        width: '100%',
    },
    multiSelect: {
        minWidth: '31%',
        maxWidth: '97%',
        margin: '3% 1% !important',
    },
    spacer: {
        flex: '1 1 50%',
    },
});
