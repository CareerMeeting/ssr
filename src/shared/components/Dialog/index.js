import React from 'react';
import { element, func, string } from 'prop-types';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';

const DialogComponent = ({ content, title, handleOk, handleCancel, ...other }) => (
    <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth="xs"
        aria-labelledby="confirmation-dialog-title"
        {...other}
    >
        <DialogTitle id="confirmation-dialog-title">{title}</DialogTitle>
        <DialogContent>
            {content}
        </DialogContent>
        <DialogActions>
            <Button onClick={handleCancel} color="primary">
                Annuler
            </Button>
            <Button onClick={handleOk} color="primary">
                Valider
            </Button>
        </DialogActions>
    </Dialog>
);

DialogComponent.propTypes = {
    content: element.isRequired,
    title: string.isRequired,
    handleOk: func.isRequired,
    handleCancel: func.isRequired,
};

export default DialogComponent;
