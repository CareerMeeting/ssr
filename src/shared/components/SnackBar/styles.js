import styled from 'react-emotion';
import { amber, green } from '@material-ui/core/colors/';
import CloseIcon from '@material-ui/icons/Close';
import { info } from '../../app/colors';

export const variantSnackbar = theme => ({
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: info,
    },
    success: {
        backgroundColor: green[600],
    },
    warning: {
        backgroundColor: amber[700],
    },
});

export const snackbarBody = theme => ({
    margin: {
        margin: theme.spacing.unit,
    },
});

export const Message = styled('span')({
    alignItems: 'center',
    display: 'flex',
});

export const CloseIconStyled = styled(CloseIcon)({
    fontSize: 20,
});

export const withIconStyle = component => styled(component)({
    fontSize: 20,
    marginRight: 5,
    opacity: 0.9,
});
