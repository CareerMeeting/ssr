import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import classNames from 'classnames';
import ErrorIcon from '@material-ui/icons/Error';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import { bool, func, oneOf, string } from 'prop-types';
import { compose, defaultProps, setPropTypes, withHandlers, withState } from 'recompose';
import { withStyles } from '@material-ui/core/styles';

import { CloseIconStyled, Message, variantSnackbar, withIconStyle } from './styles';
import { errorType, infoType, successType, warningType } from './variants';

const variantIcon = {
    error: ErrorIcon,
    info: InfoIcon,
    success: CheckCircleIcon,
    warning: WarningIcon,
};

const MySnackbarContent = (props) => {
    const { classes, className, message, onClose, variant, ...other } = props;
    const Icon = withIconStyle(variantIcon[variant]);

    return (
        <SnackbarContent
            className={classNames(classes[variant], className)}
            message={
                <Message id="client-snackbar" >
                    <Icon />
                    {message}
                </Message>
            }
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={onClose}
                >
                    <CloseIconStyled />
                </IconButton>,
            ]}
            {...other}
        />
    );
};

MySnackbarContent.propTypes = {
    variant: oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(variantSnackbar)(MySnackbarContent);

export const CustomizedSnackbars = ({ message, onClose, open, variant }) => (
    <Snackbar
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        open={open}
        autoHideDuration={3000}
        onClose={onClose}
    >
        <MySnackbarContentWrapper
            onClose={onClose}
            variant={variant}
            message={message}
        />
    </Snackbar>
);

CustomizedSnackbars.propTypes = {
    message: string.isRequired,
    onClose: func.isRequired,
    open: bool,
    variant: oneOf([errorType, infoType, successType, warningType]).isRequired,
};

CustomizedSnackbars.defaultProps = {
    open: false,
    propagateOnClose: Function.prototype,
};

const enhance = compose(
    withState('open', 'setOpen', props => props.isOpen),
    setPropTypes({
        propagateOnClose: func,
    }),
    defaultProps({
        propagateOnClose: Function.prototype,
    }),
    withHandlers({
        onClose: ({ setOpen, propagateOnClose }) => (event, reason) => {
            if (reason === 'clickaway') return;

            setOpen(false);
            propagateOnClose();
        },
    }),
);

export default enhance(CustomizedSnackbars);
