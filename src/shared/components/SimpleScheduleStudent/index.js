import { compose, withState, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { withStyles } from '@material-ui/core';

import Content from './content';
import { styles } from './styles';
import { isAdminUser } from '../../helpers/session';
import { unbindStudentFromSlot } from '../../redux/company/actions';

export const getStartHourFromSlot = (slot = {}) => path(['startHour'], slot);
export const getCompanyNameFromSlot = (slot = {}) => path(['companyName'], slot);

const handlers = {
    handleSelectSlot: ({ setSlotSelected }) => (event, slot) => {
        setSlotSelected(slot);
    },
    handleToggleDialogAdminOpen: ({ setDialogAdminOpen }) => () => {
        setDialogAdminOpen(prev => !prev);
    },
    handleUnsubscribeStudent: ({ setDialogAdminOpen, unbindStudentFromSlotAction, isAuthorizedAsAdmin, queryID, slotSelected }) => () => {
        if (!isAuthorizedAsAdmin) return;

        const { slotID, companyID } = slotSelected;
        unbindStudentFromSlotAction(companyID, slotID, queryID);
        setDialogAdminOpen(prev => !prev);
    },
};

const mapStateToProps = ({ entities }, ownerProps) => {
    const { queryID } = ownerProps;
    const studentDataPath = queryID ? ['students', queryID] : ['user'];
    const userType = path(['session', 'userType'], entities);

    return ({
        userID: path(['user', 'userType'], entities),
        isLoading: path(['students', 'isLoading'], entities),
        isAuthorizedAsAdmin: isAdminUser(userType),
        slots: path([...studentDataPath, 'slots'], entities),
    });
};

const connectToStore = connect(
    mapStateToProps,
    {
        unbindStudentFromSlotAction: unbindStudentFromSlot,
    },
);

const container = compose(
    connectToStore,
    withState('slotSelected', 'setSlotSelected', {}),
    withState('isDialogAdminOpen', 'setDialogAdminOpen', false),
    withHandlers(handlers),
    withStyles(styles),
);

export default container(Content);
