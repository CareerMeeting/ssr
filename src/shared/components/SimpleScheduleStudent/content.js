import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { TableCellCentered, TableWrapper } from './styles';
import SlotRowCompany from './SlotRowCompany';
import { PaperStyled } from '../CompaniesListTable/styles';
import { Body } from '../../views/AdminList/styles';
import Dialog from '../Dialog';
import { getStartHourFromSlot, getCompanyNameFromSlot } from './';

const SchedulerInViewMode = (
    {
        classes,
        handleSelectSlot,
        handleToggleDialogAdminOpen,
        handleUnsubscribeStudent,
        isAuthorizedAsAdmin,
        isDialogAdminOpen,
        studentIdentity,
        slots = [],
        slotSelected,
    } : {
        classes: any,
        studentIdentity: String,
        handleSelectSlot: Function,
        handleToggleDialogAdminOpen: Function,
        handleUnsubscribeStudent: Function,
        isAuthorizedAsAdmin: boolean,
        isDialogAdminOpen: boolean,
        slots: Array,
        slotSelected: Object,
    }) =>
    (<Body>
        <PaperStyled className={classes.paper}>
            <TableWrapper>
                <Table aria-labelledby="tableViewScheduleCompany" >
                    <TableHead className={classes.thead}>
                        <TableRow>
                            <TableCellCentered className={classes.tableCellCenter}>
                                Planning
                            </TableCellCentered>
                            {isAuthorizedAsAdmin && <TableCellCentered className={classes.tableCellCenter}>Actions</TableCellCentered>}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {slots.map(({ slotID, startHour, companyName, companyID }) => (
                            <SlotRowCompany
                                key={slotID}
                                slotID={slotID}
                                companyName={companyName}
                                companyID={companyID}
                                isAuthorizedAsAdmin={isAuthorizedAsAdmin}
                                startHour={startHour}
                                handleSelectSlot={handleSelectSlot}
                                handleToggleDialogAdminOpen={handleToggleDialogAdminOpen}
                            />
                        ))}
                        <Dialog
                            open={isDialogAdminOpen}
                            title={`Êtes vous sur de vouloir désinscrire cet étudiant du créneau de ${getStartHourFromSlot(slotSelected)}`}
                            content={<p>Vous allez désinscrire l'étudiant
                                {studentIdentity} du créneau de l'entreprise {getCompanyNameFromSlot(slotSelected)}</p>}
                            handleOk={handleUnsubscribeStudent}
                            handleCancel={handleToggleDialogAdminOpen}
                        />
                    </TableBody>
                </Table>
            </TableWrapper>
        </PaperStyled>
    </Body>);

export default SchedulerInViewMode;
