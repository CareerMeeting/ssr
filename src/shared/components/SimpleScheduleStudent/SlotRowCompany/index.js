import { withStyles } from '@material-ui/core';

import Content from './content';
import { styles } from './styles';

export default withStyles(styles)(Content);
