export const styles = theme => ({
    editIcon: {
        color: theme.palette.secondary.main,
    },
    tableCellCenter: {
        textAlign: 'center',
    },
});
