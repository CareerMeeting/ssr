import React from 'react';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton/IconButton';
import TableRow from '@material-ui/core/TableRow/TableRow';
import TableCell from '@material-ui/core/TableCell/TableCell';
import { Link } from 'react-router-dom';

const SchedulerInEditMode = (
    {
        slotID,
        classes,
        handleSelectSlot,
        companyName,
        companyID,
        isAuthorizedAsAdmin,
        handleToggleDialogAdminOpen,
        startHour,
    } : {
        slotID: String,
        companyID: String,
        classes: any,
        handleSelectSlot: Function,
        handleToggleDialogAdminOpen: Function,
        isAuthorizedAsAdmin: boolean,
        companyName: String,
        startHour: String,
    }) =>
    (<TableRow
        hover
        tabIndex={-1}
        key={slotID}
    >
        <TableCell
            className={classes.tableCellCenter}
            component="th"
            scope="row"
            padding="none"
        >
            <p>{startHour}</p>
            <p key={companyID}>
                <Link to={`/company/profile?id=${companyID}`}>{companyName}</Link>
            </p>
        </TableCell>
        {isAuthorizedAsAdmin && (
            <TableCell>
                <IconButton onClick={(event) => {
                    handleSelectSlot(event, { slotID, companyID, companyName, startHour });
                    handleToggleDialogAdminOpen();
                }}
                >
                    <EditIcon fontSize="small" className={classes.editIcon} />
                </IconButton>
            </TableCell>
        )}
    </TableRow>);

export default SchedulerInEditMode;
