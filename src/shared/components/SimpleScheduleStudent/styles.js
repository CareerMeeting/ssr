import styled from 'react-emotion';
import { TableCell } from '@material-ui/core';
import { white } from '../../app/colors';

export const styles = theme => ({
    paper: {
        [theme.breakpoints.down('md')]: {
            width: '100%',
        },
    },
    thead: {
        backgroundColor: theme.palette.primary.main,
    },
    tableCellCenter: {
        textAlign: 'center',
        color: white,
    },
});

export const TableCellCentered = styled(TableCell)({
    textAlign: 'center',
});

export const TableWrapper = styled('div')({
    overflowX: 'auto',
});
