import React from 'react';
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import FormControl from '@material-ui/core/FormControl/FormControl';
import InputLabel from '@material-ui/core/InputLabel/InputLabel';
import Select from '@material-ui/core/Select/Select';
import Button from '@material-ui/core/Button/Button';

import { Field } from './styles';
import type { FunctionnalComponent } from '../../app/types';

const SubscribeForm = ({ schoolsSelected, handleChangeSchoolSelect, schools,
                           handleSubmit, classes, className, handleChange, form, signUp, registrationCode }
                           : { schoolsSelected: Array, handleChangeSchoolSelect: Function })
    : FunctionnalComponent => (
    <form>
        <Grid container spacing={24}>
            <Grid item xs={12} >
                <Field
                    id="companyName"
                    onChange={handleChange}
                    label="Nom de l'entreprise"
                />
                <Field
                    id="email"
                    type="email"
                    label="Email"
                    onChange={handleChange}
                />
                <Field
                    id="password"
                    type="password"
                    label="Mot de passe"
                    onChange={handleChange}
                />
            </Grid>
            <Grid item xs={12} >
                <Field
                    id="city"
                    label="Ville"
                    onChange={handleChange}
                />
                <Field
                    id="codeZip"
                    label="code postal"
                    onChange={handleChange}
                />
                <Field
                    id="country"
                    label="Pays"
                    onChange={handleChange}
                />
            </Grid>
            <Grid item xs={12} >
                <Field
                    id="address"
                    label="Adresse"
                    onChange={handleChange}
                />
                <Field
                    id="phoneNumber"
                    label="Numéro de téléphone"
                    onChange={handleChange}
                />
                <FormControl className={classNames(classes.multiSelect, className)}>
                    <InputLabel>École(s)</InputLabel>
                    <Select
                        multiple
                        value={schoolsSelected}
                        onChange={handleChangeSchoolSelect}
                        renderValue={selected => selected.join(', ')}
                    >
                        {schools.map(school => (
                            <MenuItem key={school} value={school}>
                                <Checkbox checked={schoolsSelected.indexOf(school) > -1} />
                                <ListItemText primary={school} />
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={12}>
                <Field
                    id="companyDescription"
                    label="Description de l'entreprise"
                    multiline
                    rows="4"
                    fullWidth
                    margin="normal"
                    onChange={handleChange}
                    variant="outlined"
                />
            </Grid>
            <Grid item xs={12} className={classNames(classes.submitButton, className)}>
                <hr />
                <Button variant="contained" color="secondary" onClick={() => signUp(form, schoolsSelected.join(';'), registrationCode)}>
                    Valider
                </Button>
            </Grid>
        </Grid>
    </form>
);

export default SubscribeForm;
