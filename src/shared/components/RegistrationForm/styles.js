import styled from 'react-emotion';
import TextField from '@material-ui/core/TextField';

export const Field = styled(TextField)({
    minWidth: '31% !important',
    maxWidth: '97%',
    margin: '0% 1% !important',
});

export const styles = () => ({
    multiSelect: {
        minWidth: '31%',
        maxWidth: '97%',
    },
    submitButton: {
        textAlign: 'center',
    },
});
