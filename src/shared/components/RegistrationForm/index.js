import { connect } from 'react-redux';
import { compose, withHandlers, withState } from 'recompose';
import withStyles from '@material-ui/core/styles/withStyles';

import SubscribeForm from './content';
import { styles } from './styles';

const mapStateToProps = ({ session }) => ({
    session,
});

const componentProps = compose(
    connect(
        mapStateToProps,
    ),
    withState('schoolsSelected', 'setSchoolsSelection', []),
    withState('form', 'setForm', {
        email: '',
        password: '',
        companyName: '',
        city: '',
        codeZip: '',
        country: '',
        address: '',
        phoneNumber: '',
        companyDescription: '',
    }),
    withHandlers({
        handleChangeSchoolSelect: ({ setSchoolsSelection }) => (event) => {
            setSchoolsSelection(event.target.value);
        },
        handleChange: ({ setForm }) => (event) => {
            const target = event.target;
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const id = target.id;

            setForm(form => ({
                ...form,
                [id]: value,
            }));
        },
    }),
    withStyles(styles),
);

export default componentProps(SubscribeForm);
