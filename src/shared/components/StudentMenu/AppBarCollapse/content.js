import React from 'react';
import { object, func } from 'prop-types';
import IconButton from '@material-ui/core/IconButton/IconButton';
import ExitIcon from '@material-ui/icons/ExitToApp';

import ButtonAppBarCollapse from '../ButtonAppBarCollapse';
import { NavBar, NavLink, AppBarCollapseContent, ListContent, ListMenu } from './styles';

const AppBarCollapse = ({ classes, logOutSession }) => (
    <AppBarCollapseContent>
        <ButtonAppBarCollapse dataTarget="#appbar-collapse" />
        <div className={classes.wrapper} id="appbar-collapse">
            <ListContent id="list-appbar-collapse">
                <NavBar>
                    <ListMenu>
                        <NavLink className={classes.text} to="/student/companies">Entreprises</NavLink>
                    </ListMenu>
                    <ListMenu>
                        <NavLink className={classes.text} to="/student/planning">Mon Planning</NavLink>
                    </ListMenu>
                    <ListMenu>
                        <NavLink className={classes.text} to="/student/profile">Mon Profil</NavLink>
                    </ListMenu>
                    <ListMenu>
                        <IconButton color="secondary" aria-label="Logout" onClick={logOutSession}>
                            <ExitIcon />
                        </IconButton>
                    </ListMenu>
                </NavBar>
            </ListContent>
        </div>
    </AppBarCollapseContent>
);

AppBarCollapse.propTypes = {
    classes: object.isRequired,
    logOutSession: func.isRequired,
};

export default AppBarCollapse;
