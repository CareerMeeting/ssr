import { branch, compose, renderNothing } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { withStyles } from '@material-ui/core';

import Content from './content';
import type { EntitiesStructure } from '../../../redux/types';
import { studentType } from '../../../helpers/session/constants';
import { styles } from './styles';

const mapStateToProps = ({ entities }: { entities: EntitiesStructure }) => {
    const userType = path(['session', 'userType'], entities);
    const isAuthenticatedAsStudent = userType.toString() === studentType;

    return ({
        isAuthenticatedAsStudent,
    });
};

const connectToStore = connect(mapStateToProps, null);

const container = compose(
    connectToStore,
    withStyles(styles),
    branch(({ isAuthenticatedAsStudent }) => !isAuthenticatedAsStudent, renderNothing),
)(Content);

export default container;
