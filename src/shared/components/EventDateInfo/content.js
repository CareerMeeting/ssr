import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper/Paper';

import { InfoIconStyled } from './styles';
import { Body } from '../../views/AdminList/styles';

const Content = ({ classes, eventDateFormatted = '' } : { classes: any, eventDateFormatted: String }) => (
    <Body>
        <Paper className={classes.paper}>
            <InfoIconStyled />
            <Typography type="content" className={classes.text}>L'évenement Career Meeting a lieu le {eventDateFormatted}</Typography>
        </Paper>
    </Body>
);

export default Content;
