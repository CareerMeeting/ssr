import moment from 'moment';
import withStyles from '@material-ui/core/styles/withStyles';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { pathOr } from 'ramda';

import Content from './content';
import { styles } from './styles';

const mapStateToProps = ({ entities }) => {
    const eventDate = pathOr(new Date(), ['webConfig', 'eventDate'], entities);
    const eventDateFormatted = moment(eventDate).locale('fr').format('Do MMMM YYYY');

    return ({
        eventDateFormatted,
    });
};

const connectToStore = connect(mapStateToProps, null);

const enhance = compose(
    connectToStore,
    withStyles(styles),
);

export default enhance(Content);
