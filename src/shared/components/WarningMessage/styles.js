import styled from 'react-emotion';
import InfoIcon from '@material-ui/icons/Warning';
import { amber } from '@material-ui/core/colors/';

import { white } from '../../app/colors';

export const InfoIconStyled = styled(InfoIcon)({
    fontSize: 20,
    marginRight: 5,
    opacity: 0.9,
});

export const styles = () => ({
    text: {
        color: white,
        padding: '8px 0',
        textAlign: 'left',
    },
    paper: {
        backgroundColor: amber[700],
        color: white,
        padding: 10,
        margin: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
