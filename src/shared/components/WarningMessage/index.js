import withStyles from '@material-ui/core/styles/withStyles';

import Content from './content';
import { styles } from './styles';

export default withStyles(styles)(Content);
