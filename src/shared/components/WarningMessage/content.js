import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper/Paper';

import { InfoIconStyled } from './styles';
import { Body } from '../../views/AdminList/styles';

const Content = ({ classes, message = '' } : { classes: any, message: String }) => (
    <Body>
        <Paper className={classes.paper}>
            <InfoIconStyled />
            <Typography type="content" className={classes.text}>{message}</Typography>
        </Paper>
    </Body>
);

export default Content;
