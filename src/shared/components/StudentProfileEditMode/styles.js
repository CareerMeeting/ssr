import styled from 'react-emotion';
import Paper from '@material-ui/core/Paper/Paper';
import { info, white } from '../../app/colors';

export const Body = styled('div')({
    alignItems: 'center',
    display: 'grid',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'center',
});

export const SettingItem = styled('div')({
    alignItems: 'center',
    display: 'block',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 10,
    paddingTop: 10,
});

export const CenterBlock = styled('div')({
    display: 'flex',
    justifyContent: 'center',
    marginBottom: 10,
});

export const Separator = styled('hr')({
    width: 160,
    marginTop: 10,
});

export const styles = theme => ({
    inputLabel: {
        color: theme.palette.secondary.main,
    },
});

export const PaperStyled = styled(Paper)({
    backgroundColor: info,
    color: white,
    padding: 10,
    margin: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
});
