// @flow
import React from 'react';
import classNames from 'classnames';
import TextField from '@material-ui/core/TextField/TextField';
import InfoIcon from '@material-ui/icons/Info';
import OutlinedInput from '@material-ui/core/OutlinedInput/OutlinedInput';
import Button from '@material-ui/core/Button/Button';

import Title from '../../components/Title';
import type { FunctionnalComponent } from '../../app/types';
import { Body, SettingItem, Separator, PaperStyled, CenterBlock } from './styles';
import Select from '../../components/Select';
import { ButtonValidate, IconCheck } from '../../views/SuperAdminSettings/styles';

const degreeLevels = [
    '1° année',
    '2° année',
    '3° année',
    '4° année',
    '5° année',
];

const View = (
    {
        form: {
            firstName,
            lastName,
            linkedInLink,
            degreeLevel,
            phoneNumber,
        } = {},
        classes,
        className,
        handleChangeDegreeLevel,
        handleValidateProfile,
        handleChange,
        handleChangeDocumentCV,
        hasUpdatedCV,
        hasUpdatedForm,
    }
: {
        handleChange: Function,
        form: Object,
        classes: Object,
        className: Object,
        handleChangeDegreeLevel: Function,
        handleValidateProfile: Function,
        handleChangeDocumentCV: Function,
        hasUpdatedCV: boolean,
        hasUpdatedForm: boolean,
    }): FunctionnalComponent => (
    <Body>
        <SettingItem>
            <TextField
                InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                id="lastName"
                fullWidth
                label="Nom"
                value={lastName}
                margin="normal"
                onChange={handleChange}
                variant="outlined"
            />
        </SettingItem>
        <SettingItem>
            <TextField
                InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                id="firstName"
                label="Prénom"
                value={firstName}
                margin="normal"
                fullWidth
                onChange={handleChange}
                variant="outlined"
            />
        </SettingItem>
        <SettingItem>
            <TextField
                InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                id="phoneNumber"
                label="Numéro de téléphone"
                value={phoneNumber}
                margin="normal"
                fullWidth
                onChange={handleChange}
                variant="outlined"
            />
        </SettingItem>
        <SettingItem>
            <TextField
                InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                id="linkedInLink"
                label="Lien LinkedInLink"
                value={linkedInLink}
                margin="normal"
                fullWidth
                onChange={handleChange}
                variant="outlined"
            />
        </SettingItem>
        <SettingItem>
            <Title>Niveau d'étude</Title>
            <div>
                <Select
                    input={
                        <OutlinedInput
                            fullWidth
                            labelWidth={0}
                            name="Niveau d'étude"
                        />
                    }
                    source={degreeLevels}
                    destination={degreeLevel}
                    handleChange={handleChangeDegreeLevel}
                />
            </div>
        </SettingItem>
        <Separator />
        <SettingItem>
            <Title>Upload Mon CV</Title>
            <PaperStyled>
                <InfoIcon />
                Votre CV doit être au format pdf et ne doit pas excéder 50MB
            </PaperStyled>
            <CenterBlock>
                <Button variant="contained">
                    <input accept="application/pdf" type="file" onChange={handleChangeDocumentCV} />
                </Button>
            </CenterBlock>
        </SettingItem>
        <Separator />
        <CenterBlock>
            <ButtonValidate disabled={!hasUpdatedCV && !hasUpdatedForm} variant="outlined" color="secondary" onClick={handleValidateProfile}>
                Valider mon profil <IconCheck />
            </ButtonValidate>
        </CenterBlock>
    </Body>
);

export default View;
