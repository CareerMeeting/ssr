import { branch, compose, renderNothing, withHandlers, withPropsOnChange, withState } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { withStyles } from '@material-ui/core';
import FormData from 'form-data';

import Content from './content';
import type { EntitiesStructure } from '../../redux/types';
import { uploadStudentCV, updateStudentUser } from '../../redux/user/actions';
import { styles } from './styles';
import { isNilOrEmpty } from '../../helpers/utils';
import { studentType } from '../../helpers/session/constants';

const handlers = {
    handleValidateProfile: ({ handleIsInEditMode, updateStudentUserAPI, uploadStudentCVAPI, form, cvDocument, hasUpdatedForm, hasUpdatedCV }) => () => {
        if (isNilOrEmpty(form)) return;

        if (hasUpdatedForm) {
            updateStudentUserAPI(form);
        }
        if (hasUpdatedCV) {
            const formData = new FormData();
            formData.append('cv', cvDocument);

            uploadStudentCVAPI(formData);
        }
        handleIsInEditMode();
    },
    handleChangeDegreeLevel: ({ setForm, setHasUpdatedForm }) => (event) => {
        const target = event.target;
        const value = target.value;

        setForm(form => ({
            ...form,
            degreeLevel: value,
        }));
        setHasUpdatedForm(true);
    },
    handleChangeDocumentCV: ({ setCvDocument, setHasUpdatedCV }) => (event) => {
        const file = event.target.files[0];
        if (!file) return;

        const fileSize = ((file.size / 1024) / 1024).toFixed(4);
        if (fileSize > 50) return;

        setCvDocument(file);
        setHasUpdatedCV(true);
    },
    handleChange: ({ setForm, setHasUpdatedForm }) => (event) => {
        const target = event.target;
        const value = target.value;
        const id = target.id;

        setForm(form => ({
            ...form,
            [id]: value,
        }));
        setHasUpdatedForm(true);
    },
};

const mapStateToProps = ({ entities }: { entities: EntitiesStructure }) => ({
    userType: path(['session', 'userType'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    {
        uploadStudentCVAPI: uploadStudentCV,
        updateStudentUserAPI: updateStudentUser,
    },
);

const initForm = ({
    setForm,
    firstName,
    lastName,
    linkedInLink,
    degreeLevel,
    phoneNumber,
}) => {
    setForm({
        firstName,
        lastName,
        linkedInLink,
        degreeLevel,
        phoneNumber,
    });
};

const container = compose(
    connectToStore,
    branch(({ userType }) => userType !== studentType, renderNothing),
    withState('hasUpdatedCV', 'setHasUpdatedCV', false),
    withState('hasUpdatedForm', 'setHasUpdatedForm', false),
    withState('cvDocument', 'setCvDocument', null),
    withState('form', 'setForm', {
        firstName: '',
        lastName: '',
        linkedInLink: '',
        degreeLevel: '',
        phoneNumber: '',
    }),
    withPropsOnChange(['firstName'], initForm),
    withHandlers(handlers),
    withStyles(styles),
)(Content);

export default container;
