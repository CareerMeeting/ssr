import Paper from '@material-ui/core/Paper/Paper';
import styled from 'react-emotion';

export const PaperStyled = styled(Paper)({
    margin: '0% 1% !important',
    maxWidth: '97%',
    minWidth: '31% !important',
    overflowX: 'auto',
});
