import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';

import { PaperStyled } from './styles';

const SimpleTable = ({ admins = [] }: { admins: Array }) => (
    <PaperStyled>
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>Email</TableCell>
                    <TableCell>Organisation</TableCell>
                    <TableCell>Périmètre d'actions</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {admins.map(({ _id, email, organizationName, schools }) => (
                    <TableRow key={_id}>
                        <TableCell component="th" scope="row" >
                            <Link to={`/admin/profile?id=${_id}`} >{email}</Link>
                        </TableCell>
                        <TableCell>{organizationName}</TableCell>
                        <TableCell>{schools}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </PaperStyled>
);

export default SimpleTable;
