import { withStyles } from '@material-ui/core';

import Content from './content';
import { styles } from '../SimpleScheduleStudent/styles';

export default withStyles(styles)(Content);
