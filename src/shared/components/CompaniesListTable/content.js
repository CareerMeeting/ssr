import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';

import { PaperStyled, TableCellStyled } from './styles';
import { TableCellCentered } from '../SimpleScheduleStudent/styles';

const SimpleTable = ({ data = [], classes }: { data: Array, classes: any }) => (
    <PaperStyled>
        <Table>
            <TableHead className={classes.thead}>
                <TableRow>
                    <TableCellCentered className={classes.tableCellCenter}>Nom de l'entreprise</TableCellCentered>
                    <TableCellCentered className={classes.tableCellCenter}>Nombre d'offres de stage</TableCellCentered>
                    <TableCellCentered className={classes.tableCellCenter}>Ville</TableCellCentered>
                </TableRow>
            </TableHead>
            <TableBody>
                {data.map(({ _id, companyName, city, traineeshipOffersNumber }) => (
                    <TableRow key={_id}>
                        <TableCell component="th" scope="row" >
                            <Link to={`/company/profile?id=${_id}`} >{companyName}</Link>
                        </TableCell>
                        <TableCell>{traineeshipOffersNumber}</TableCell>
                        <TableCellStyled>{city}</TableCellStyled>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </PaperStyled>
);

export default SimpleTable;
