import Paper from '@material-ui/core/Paper/Paper';
import styled from 'react-emotion';
import TableCell from '@material-ui/core/TableCell/TableCell';

export const PaperStyled = styled(Paper)({
    margin: '0% 1% !important',
    maxWidth: 1500,
    minWidth: '31% !important',
    overflowX: 'auto',
});

export const TableCellStyled = styled(TableCell)({
    textTransform: 'capitalize',
});
