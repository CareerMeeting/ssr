import Paper from '@material-ui/core/Paper/Paper';
import styled from 'react-emotion';
import TableCell from '@material-ui/core/TableCell/TableCell';
import Lens from '@material-ui/icons/Lens';
import { green, red } from '../../app/colors';

export const PaperStyled = styled(Paper)({
    margin: '0% 1% !important',
    maxWidth: '97%',
    minWidth: '31% !important',
    overflowX: 'auto',
});

export const TableCellStyled = styled(TableCell)({
    textTransform: 'capitalize',
});

export const ActiveCircleIcon = styled(Lens)({
    color: green,
});

export const DeactivateCircleIcon = styled(Lens)({
    color: red,
});
