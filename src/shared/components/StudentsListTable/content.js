import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';

import { PaperStyled, TableCellStyled, DeactivateCircleIcon, ActiveCircleIcon } from './styles';

const SimpleTable = ({ data = [] }: { data: Array }) => (
    <PaperStyled>
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>Email</TableCell>
                    <TableCell>Nom</TableCell>
                    <TableCell>Prénom</TableCell>
                    <TableCell>Niveau d'étude</TableCell>
                    <TableCell>Activé</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {data.map(({ _id, firstName, lastName, degreeLevel, email, isDeactivatedByAdmin }) => (
                    <TableRow key={_id}>
                        <TableCell component="th" scope="row" >
                            <Link to={`/student/profile?id=${_id}`} >{email}</Link>
                        </TableCell>
                        <TableCellStyled>{lastName}</TableCellStyled>
                        <TableCellStyled>{firstName}</TableCellStyled>
                        <TableCell>{degreeLevel || 'N/A'}</TableCell>
                        <TableCell>
                            {isDeactivatedByAdmin ? <DeactivateCircleIcon /> : <ActiveCircleIcon />}
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </PaperStyled>
);

export default SimpleTable;
