import { func } from 'prop-types';
import { compose, setPropTypes, withHandlers } from 'recompose';

import ButtonAppBarCollapse from './content';

const withHandlersPayload = {
    toggleCollapse: () => (e) => {
        e.preventDefault();
        const collapsed = document.getElementById('appbar-collapse');
        if (collapsed.style.display === 'block') {
            collapsed.style.display = 'none';
        } else {
            collapsed.style.display = 'block';
            collapsed.style.backgroundColor = '#fff';
            collapsed.style.top = '50px';
            collapsed.style.textAlign = 'center';
            collapsed.style.width = '240px';
            collapsed.style.position = 'absolute';
            collapsed.style.zIndex = 1450;
        }
    },
};

const container = compose(
    withHandlers(withHandlersPayload),
    setPropTypes({
        toggleCollapse: func.isRequired,
    }),
)(ButtonAppBarCollapse);

export default container;
