import { branch, compose, withPropsOnChange, withState, renderNothing } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { withStyles } from '@material-ui/core';

import Content from './content';
import type { EntitiesStructure } from '../../../redux/types';
import { adminType, superAdminType } from '../../../helpers/session/constants';
import { styles } from './styles';

const mapStateToProps = ({ entities }: { entities: EntitiesStructure }) => ({
    userType: path(['session', 'userType'], entities),
});

const connectToStore = connect(mapStateToProps, null);

const initAuthorisation = ({ setIsAuthenticatedAsSuperAdmin, setIsAuthenticatedAsAdmin, userType = '' }) => {
    setIsAuthenticatedAsAdmin(userType.toString() === superAdminType || userType.toString() === adminType);
    setIsAuthenticatedAsSuperAdmin(userType.toString() === superAdminType);
};

const container = compose(
    connectToStore,
    withState('isAuthenticatedAsSuperAdmin', 'setIsAuthenticatedAsSuperAdmin', false),
    withState('isAuthenticatedAsAdmin', 'setIsAuthenticatedAsAdmin', false),
    withPropsOnChange(['userType'], initAuthorisation),
    withStyles(styles),
    branch(({ isAuthenticatedAsAdmin }) => !isAuthenticatedAsAdmin, renderNothing),
)(Content);

export default container;
