import { Link } from 'react-router-dom';
import styled from 'react-emotion';

export const NavBar = styled('nav')({
    display: 'flex',
    right: '1rem',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
});

export const NavLink = styled(Link)({
    display: 'inline-block',
    padding: '6px 8px',
    textAlign: 'center',
    textDecoration: 'none',
});

export const ListContent = styled('ul')({
    listStyle: 'none',
    margin: 0,
    overflow: 'hidden',
    padding: 0,
});

export const ListMenu = styled('li')({
    float: 'left',
});

export const AppBarCollapseContent = styled('div')({
    position: 'absolute',
    right: 0,
});

export const styles = theme => ({
    text: {
        color: '#fff',
        [theme.breakpoints.down('md')]: {
            color: '#000',
        },
    },
    wrapper: {
        [theme.breakpoints.down('md')]: {
            display: 'none',
        },
        background: 'transparent',
        margin: '10px',
        paddingLeft: '16px',
        position: 'relative',
        right: 0,
    },
});
