import React from 'react';
import { bool, func, object } from 'prop-types';
import IconButton from '@material-ui/core/IconButton/IconButton';
import ExitIcon from '@material-ui/icons/ExitToApp';

import ButtonAppBarCollapse from '../ButtonAppBarCollapse';
import { NavBar, NavLink, AppBarCollapseContent, ListContent, ListMenu } from './styles';

const AppBarCollapse = ({ classes, isAuthenticatedAsSuperAdmin, logOutSession }) => (
    <AppBarCollapseContent>
        <ButtonAppBarCollapse dataTarget="#appbar-collapse" />
        <div className={classes.wrapper} id="appbar-collapse">
            <ListContent id="list-appbar-collapse">
                <NavBar>
                    <ListMenu>
                        <NavLink className={classes.text} to="/admin/students">Étudiants</NavLink>
                    </ListMenu>
                    <ListMenu>
                        <NavLink className={classes.text} to="/admin/companies">Entreprises</NavLink>
                    </ListMenu>
                    {isAuthenticatedAsSuperAdmin && (
                        <ListMenu>
                            <NavLink className={classes.text} to="/admin/admins">Admins</NavLink>
                        </ListMenu>
                    )}
                    <ListMenu>
                        <NavLink className={classes.text} to="/admin/profile">Profil</NavLink>
                    </ListMenu>
                    {isAuthenticatedAsSuperAdmin && (
                        <ListMenu>
                            <NavLink className={classes.text} to="/admin/settings">Paramètres</NavLink>
                        </ListMenu>
                    )}
                    <ListMenu>
                        <IconButton color="secondary" aria-label="Logout" onClick={logOutSession}>
                            <ExitIcon />
                        </IconButton>
                    </ListMenu>
                </NavBar>
            </ListContent>
        </div>
    </AppBarCollapseContent>
);

AppBarCollapse.propTypes = {
    classes: object.isRequired,
    isAuthenticatedAsSuperAdmin: bool.isRequired,
    logOutSession: func.isRequired,
};

export default AppBarCollapse;
