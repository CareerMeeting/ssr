import { Link } from 'react-router-dom';
import styled from 'react-emotion';

export const NavBar = styled('nav')({
    right: '1rem',
});

export const NavLink = styled(Link)({
    textDecoration: 'none',
    color: 'white',
});
