import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography/Typography';
import React from 'react';

import AppBarCollapse from './AppBarCollapse';

const Menu = ({ logOutSession }) => (
    <AppBar position="static" color="primary">
        <Toolbar>
            <Typography type="title" color="inherit">
                Career Meeting Admin
            </Typography>
            <AppBarCollapse logOutSession={logOutSession} />
        </Toolbar>
    </AppBar>
);

export default Menu;
