import Button from '@material-ui/core/Button/Button';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import classNames from 'classnames';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel/InputLabel';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import React from 'react';
import Select from '@material-ui/core/Select/Select';
import { map } from 'ramda';
import { withStyles } from '@material-ui/core/styles';

import type { FunctionnalComponent } from '../../app/types';
import { Field, IconPlus, styles, Body } from './styles';

const LoginForm = (
    {
        classes,
        className,
        toggleDialog,
        isDialogOpen,
        email,
        schools = [],
        isLoading,
        handleChangeEmail,
        handleChangeSchoolSelect,
        schoolsSelected,
        handleOkDialog,
    }
        : { email: string, classes: Object, className: Object, schools: Array,
        toggleDialog: Function, isDialogOpen: boolean, email: string, isLoading: boolean,
        handleChangeEmail: Function, handleChangeSchoolSelect: Function, schoolsSelected: Array, handleOkDialog: Function })
    : FunctionnalComponent => (
    <Body>
        <Dialog
            open={isDialogOpen}
            onClose={toggleDialog}
        >
            <DialogTitle>
                Ajouter un administrateur
            </DialogTitle>
            <DialogContent>
                <Field
                    autoFocus
                    margin="dense"
                    type="email"
                    id="email-administrateur-add"
                    value={email}
                    onChange={handleChangeEmail}
                    label="Email"
                    fullWidth
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                {isLoading ? (<CircularProgress size={20} color="secondary" />) : <noscript />}
                            </InputAdornment>
                        ),
                    }}
                />
                <FormControl className={classNames(classes.multiSelect, className)}>
                    <InputLabel>Périmètre d'actions</InputLabel>
                    <Select
                        multiple
                        value={schoolsSelected}
                        onChange={handleChangeSchoolSelect}
                        renderValue={selected => selected.join(', ')}
                    >
                        {map(school => (
                            <MenuItem key={school} value={school}>
                                <Checkbox checked={schoolsSelected.indexOf(school) > -1} />
                                <ListItemText primary={school} />
                            </MenuItem>
                        ), schools)}
                    </Select>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button onClick={toggleDialog} color="primary">
                    Annuler
                </Button>
                <Button onClick={handleOkDialog} color="primary">
                    Ajouter
                </Button>
            </DialogActions>
        </Dialog>
        <Button variant={'outlined'} color="secondary" onClick={toggleDialog}>
            Ajouter un administrateur
            <IconPlus />
        </Button>
    </Body>
);

export default withStyles(styles)(LoginForm);
