import styled from 'react-emotion';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';

export const styles = theme => ({
    root: {
        backgroundColor: theme.palette.secondary.main,
        color: 'white',
        lineHeight: '3',
    },
    multiSelect: {
        width: '100%',
    },
});

export const Body = styled('div')({
    marginBottom: 20,
});

export const IconPlus = styled(AddIcon)({
    marginLeft: 5,
});

export const Field = styled(TextField)({
    minWidth: 300,
    margin: '6% 0',
    display: 'grid',
});
