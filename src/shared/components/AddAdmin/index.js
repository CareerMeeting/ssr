import { compose, withState, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';

import Content from './content';
import { addAdmin } from '../../redux/admins/actions';

const mapStateToProps = ({ entities, admins }) => ({
    isLoading: path(['isLoading'], admins),
    schools: path(['webConfig', 'schools'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    {
        addAdminAPI: addAdmin,
    },
);

const componentData = compose(
    connectToStore,
    withState('email', 'setEmail', ''),
    withState('schoolsSelected', 'setSchoolsSelection', []),
    withState('isDialogOpen', 'setDialogState', false),
    withHandlers({
        handleChangeSchoolSelect: ({ setSchoolsSelection }) => (event) => {
            setSchoolsSelection(event.target.value);
        },
        handleChangeEmail: ({ setEmail }) => (event) => {
            setEmail(event.target.value);
        },
        toggleDialog: ({ setDialogState }) => () => {
            setDialogState(prev => !prev);
        },
        handleOkDialog: ({ addAdminAPI, toggleDialog, email, schoolsSelected }) => () => {
            addAdminAPI(email, schoolsSelected);
            toggleDialog();
        },
    }),
);

export default componentData(Content);
