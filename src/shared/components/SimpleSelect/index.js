import { withStyles } from '@material-ui/core/styles';

import Select from './content';
import { styles } from './styles';

export default withStyles(styles)(Select);
