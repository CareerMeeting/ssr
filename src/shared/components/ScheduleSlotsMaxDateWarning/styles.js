import styled from 'react-emotion';
import InfoIcon from '@material-ui/icons/Info';

import { info, white } from '../../app/colors';

export const InfoIconStyled = styled(InfoIcon)({
    fontSize: 20,
    marginRight: 5,
    opacity: 0.9,
});

export const styles = () => ({
    text: {
        color: white,
        padding: '8px 0',
        textAlign: 'left',
    },
    paper: {
        backgroundColor: info,
        color: white,
        padding: 10,
        margin: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
