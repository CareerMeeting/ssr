import React from 'react';

import WarningMessage from '../WarningMessage';

const Content = ({ closeScheduleEventDateFormatted } : { closeScheduleEventDateFormatted: String }) => (
    <WarningMessage message={`Vous avez jusqu'au ${closeScheduleEventDateFormatted} pour pouvoir modifier vos créneaux.`} />
);

export default Content;
