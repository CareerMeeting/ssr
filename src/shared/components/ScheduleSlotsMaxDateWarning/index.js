import moment from 'moment';
import withStyles from '@material-ui/core/styles/withStyles';
import { branch, compose, renderNothing } from 'recompose';
import { connect } from 'react-redux';
import { path, pathOr } from 'ramda';

import Content from './content';
import { styles } from './styles';
import { isCompanyUser } from '../../helpers/session';

const mapStateToProps = ({ entities }) => {
    const userType = path(['session', 'userType'], entities);
    const eventDate = pathOr(new Date(), ['webConfig', 'eventDate'], entities);
    const maxAbilityToScheduleBeforeEventDate = 4;
    const maxDateToSchedule = moment(eventDate).subtract(maxAbilityToScheduleBeforeEventDate, 'days').utc();
    const minDateToDisplayWarningSchedule = moment(eventDate).subtract(maxAbilityToScheduleBeforeEventDate + 5, 'days').utc();
    const currentDate = moment().utc();
    const closeScheduleEventDateFormatted = isCompanyUser(userType) && currentDate.isSameOrAfter(minDateToDisplayWarningSchedule) &&
        currentDate.isSameOrBefore(maxDateToSchedule, 'day') && moment(maxDateToSchedule).locale('fr').format('Do MMMM YYYY');

    return ({
        closeScheduleEventDateFormatted,
    });
};

const connectToStore = connect(mapStateToProps, null);

const enhance = compose(
    connectToStore,
    branch(({ closeScheduleEventDateFormatted }) => !closeScheduleEventDateFormatted, renderNothing),
    withStyles(styles),
);

export default enhance(Content);
