import React from 'react';
import classNames from 'classnames';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import Select from '@material-ui/core/Select/Select';
import { type } from 'ramda';

import type { FunctionnalComponent } from '../../app/types';

const Index = ({ source, destination = [], handleChange, classes, className, ...other }:
                   { source: Array, destination: destination, handleChange: Function, classes: Object, className: Object }): FunctionnalComponent => (
    <Select
        className={classNames(classes.multiSelect, className)}
        value={destination}
        onChange={handleChange}
        renderValue={selected => (type(selected) === 'Arrray' ? selected.join(', ') : selected)}
        {...other}
    >
        {source.map(elem => (
            <MenuItem key={elem} value={elem}>
                <Checkbox checked={destination && destination.indexOf(elem) > -1} />
                <ListItemText primary={elem} />
            </MenuItem>
        ))}
    </Select>
);

export default Index;
