import React from 'react';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';

import styles from './styles';

const Index = ({ classes, className, children }: { classes: Object, className: any, children: Object }) =>
    <span className={classNames(classes.title, className)}>{children}</span>;

export default withStyles(styles)(Index);
