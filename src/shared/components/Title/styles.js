const styles = theme => ({
    title: {
        fontWeight: 'bold',
        color: theme.palette.secondary.main,
    },
});

export default styles;
