import React from 'react';
import renderer from 'react-test-renderer';

import LargeLoader from '../';

it('renders correctly', () => {
    const tree = renderer
        .create(<LargeLoader />)
        .toJSON();

    expect(tree).toMatchSnapshot();
});
