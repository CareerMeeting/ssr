import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';

const Index = () => <CircularProgress size={100} />;

export default Index;
