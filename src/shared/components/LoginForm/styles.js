import styled from 'react-emotion';
import Card from '@material-ui/core/Card';
import Send from '@material-ui/icons/Send';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Assignment from '@material-ui/icons/Assignment';

export const styles = theme => ({
    loginCardHeader: {
        backgroundColor: theme.palette.secondary.main,
        color: 'white',
        lineHeight: '3',
        marginTop: '-80px',
        position: 'relative',
        padding: '5%',
    },
    iconPos: {
        position: 'relative',
        top: '6px',
        paddingLeft: '2%',
    },
    hr: {
        margin: '5% 0%',
    },
    buttonWrapper: {
        margin: theme.spacing.unit,
        position: 'relative',
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -15,
        marginLeft: -18,
    },
    undoButton: {
        color: '#FFFFFF',
        border: '3px solid white',
        width: 10,
        height: 10,
    },
    undoIcon: {
        position: 'absolute',
    },
    textField: {
        minWidth: 300,
        margin: '6% 0',
        display: 'grid',
    },
    largeButton: {
        width: '100%',
    },
    form: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    content: {
        padding: 'inherit',
    },
    buttonIcon: {
        marginLeft: 5,
    },
    loginCard: {
        padding: '1%',
        textAlign: 'center',
        overflow: 'visible',
    },
});
