import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import WorkIcon from '@material-ui/icons/Work';
import UndoIcon from '@material-ui/icons/Reply';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import AssignmentIcon from '@material-ui/icons/Assignment';
import SendIcon from '@material-ui/icons/Send';

import type { FunctionnalComponent } from '../../app/types';
import { styles } from './styles';

const LoginForm = (
    {
        classes,
        className,
        login,
        email,
        password,
        handleChange,
        isLoading,
        toggleOpen,
        toggleDialog,
        isDialogOpen,
        fetchCodeChecker,
        registrationCode,
        isChecking,
    }
        : { classes: Object, className: Object, isLoading: boolean, toggleOpen: Function,
        toggleDialog: Function, isDialogOpen: boolean, login: Function, email: string, isChecking: boolean,
        password: string, handleChange: Function, fetchCodeChecker: Function, registrationCode: string })
    : FunctionnalComponent => (
    <div>
        <Paper elevation={9} className={classNames(classes.loginCardHeader, className)} >
            Espace Entreprise <WorkIcon className={classNames(classes.iconPos, className)} />
            <br />
            <IconButton className={classNames(classes.undoButton, className)} onClick={toggleOpen}>
                <UndoIcon className={classNames(classes.undoIcon, className)} fontSize="small" />
            </IconButton>
        </Paper>
        <div className={classNames(classes.form, className)}>
            <TextField
                id="email"
                variant="outlined"
                value={email}
                onChange={handleChange}
                type="email"
                label="Email"
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <EmailIcon />
                        </InputAdornment>
                    ),
                }}
                className={classNames(classes.textField, className)}
            />
            <TextField
                id="password"
                variant="outlined"
                value={password}
                onChange={handleChange}
                type="password"
                label="Mot de passe"
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <LockIcon />
                        </InputAdornment>
                    ),
                }}
                className={classNames(classes.textField, className)}
            />
            <Typography variant="caption" align="center">
                <Link to="/reset-password">Mot de passe oublié ?</Link>
            </Typography><br />
            <div className={classNames(classes.buttonWrapper, className)}>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => login(email, password)}
                    disabled={isLoading}
                    className={classNames(classes.largeButton, className)}
                >
                    Connexion
                    <SendIcon className={classNames(classes.buttonIcon, className)} />
                </Button>
                {isLoading &&
                (<CircularProgress
                    size={30}
                    className={classNames(classes.buttonProgress, className)}
                    color="secondary"
                />)}
            </div>
            <hr className={classNames(classes.hr, className)} />
            <Button variant="contained" color="primary" onClick={toggleDialog} className={classNames(classes.largeButton, className)} >
                Inscription
                <AssignmentIcon className={classNames(classes.buttonIcon, className)} />
            </Button>
            <Dialog
                open={isDialogOpen}
                onClose={toggleDialog}
            >
                <DialogTitle>
                    Code d'inscription
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Veuillez saisir le code d'inscription qui vous à été communiqué.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="registrationCode"
                        value={registrationCode}
                        onChange={handleChange}
                        label="Code"
                        fullWidth
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    {isChecking ? (<CircularProgress size={20} color="secondary" />) : <noscript />}
                                </InputAdornment>
                            ),
                        }}
                        className={classNames(classes.textField, className)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={toggleDialog} color="primary">
                        Annuler
                    </Button>
                    <Button onClick={() => fetchCodeChecker(registrationCode)} color="primary">
                        S'inscrire
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    </div>
);

export default withStyles(styles)(LoginForm);
