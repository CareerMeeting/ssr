import { branch, compose, withState, withHandlers, renderComponent } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';

import Content from './content';
import { navigateTo } from '../../helpers/navigation';
import { fetchCodeChecker } from '../../redux/codeChecker/actions';

const mapStateToProps = ({ codeChecker, entities }) => ({
    isChecking: path(['isLoading'], codeChecker),
    token: path(['session', 'token'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    { fetchCodeChecker },
);

const componentData = compose(
    connectToStore,
    withState('email', 'setEmail', ''),
    withState('password', 'setPassword', ''),
    withState('isDialogOpen', 'setDialogState', false),
    withState('registrationCode', 'setCode', ''),
    withHandlers({
        handleChange: ({ setEmail, setPassword, setCode }) => (event) => {
            if (event.target.id === 'email') setEmail(event.target.value);
            if (event.target.id === 'password') setPassword(event.target.value);
            if (event.target.id === 'registrationCode') setCode(event.target.value);
        },
        toggleDialog: ({ setDialogState, isDialogOpen }) => () => {
            setDialogState(!isDialogOpen);
        },
    }),
    branch(
        ({ token }: { session: string }): boolean => !!token ,
        renderComponent(navigateTo('/company/profile')),
    ),
);

export default componentData(Content);
