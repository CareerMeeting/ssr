import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography/Typography';
import React from 'react';

import { NavBar, NavLink } from './styles';

const Menu = () => (
    <AppBar position="static" color="primary">
        <Toolbar>
            <Typography type="title" color="inherit">
                Career Meeting
            </Typography>
            <NavBar>
                <NavLink to="/">Home</NavLink>
                <NavLink to="student/subscribe"> Inscription </NavLink>
            </NavBar>
        </Toolbar>
    </AppBar>
);

export default Menu;
