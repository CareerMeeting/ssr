import Content from './content';

export const getStudent = (firstName, lastName = '', school = '') =>
    (firstName ? `${lastName.toUpperCase()} ${firstName} de ${school.toLocaleUpperCase()}` : '');

export default Content;
