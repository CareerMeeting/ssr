import React from 'react';
import { find, map } from 'ramda';

import { getStudent } from './';
import SimpleSelect from '../../SimpleSelect';
import { CenterBlock } from './styles';

const DialogAdminSlot = ({ slot = {}, studentSelected, handleSelectStudent } :
                             { slot: Object, studentSelected: String, handleSelectStudent: Function }) => {
    const { linkedStudents = [] } = slot;
    const linkedStudentsIdentities = map(
        ({ firstName, lastName = '', school = '', userID }) => ({
            value: userID,
            display: getStudent(firstName, lastName, school),
        }), linkedStudents);
    const { lastName, firstName, school } = find(({ userID }) => userID === studentSelected, linkedStudents) || {};
    const studentSelectedIdentity = getStudent(firstName, lastName, school) || '';

    return (
        <CenterBlock>
            <SimpleSelect source={linkedStudentsIdentities} destination={studentSelectedIdentity} handleChange={handleSelectStudent} />
        </CenterBlock>
    );
};

export default DialogAdminSlot;
