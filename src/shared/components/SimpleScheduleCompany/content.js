import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { TableCellCentered, TableWrapper } from './styles';
import SlotRowCompany from './SlotRowCompany';
import { PaperStyled } from '../CompaniesListTable/styles';
import { Body } from '../../views/AdminList/styles';
import Dialog from '../Dialog';
import { getStartHourFromSlot } from './';
import DialogAdminSlot from './DialogAdminSlot';

const SchedulerInViewMode = (
    {
        classes,
        companyName,
        handleSelectSlot,
        handleSelectStudent,
        handleToggleDialogAdminOpen,
        handleUnsubscribeStudent,
        handleSubscribeSlot,
        handleToggleDialogStudentSubscribe,
        isAuthenticatedAsStudent,
        isAuthorizedAsAdmin,
        isAuthenticatedAsNotCompanyUser,
        isDialogStudentSubscribe,
        isDialogAdminOpen,
        slots = [],
        slotSelected,
        studentSelected,
    } : {
        classes: any,
        companyName: String,
        handleSubscribeSlot: Function,
        handleToggleDialogStudentSubscribe: Function,
        handleSelectSlot: Function,
        handleSelectStudent: Function,
        handleToggleDialogAdminOpen: Function,
        handleUnsubscribeStudent: Function,
        isAuthenticatedAsStudent: boolean,
        isAuthorizedAsAdmin: boolean,
        isAuthenticatedAsNotCompanyUser: boolean,
        isDialogAdminOpen: boolean,
        isDialogStudentSubscribe: boolean,
        slots: Array,
        slotSelected: Object,
        studentSelected: String,
    }) =>
    (<Body>
        <PaperStyled className={classes.paper}>
            <TableWrapper>
                <Table aria-labelledby="tableViewScheduleCompany" >
                    <TableHead className={classes.thead}>
                        <TableRow>
                            <TableCellCentered className={classes.tableCellCenter}>
                                Créneaux {isAuthenticatedAsNotCompanyUser && `de ${companyName}`}
                            </TableCellCentered>
                            {isAuthorizedAsAdmin && <TableCellCentered className={classes.tableCellCenter}>Actions</TableCellCentered>}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {slots.map(({ _id, startHour, linkedStudents = [], isAvailable, hasSubscribed }) => (
                            <SlotRowCompany
                                key={_id}
                                _id={_id}
                                isAvailable={isAvailable}
                                hasSubscribed={hasSubscribed}
                                isAuthorizedAsAdmin={isAuthorizedAsAdmin}
                                isAuthenticatedAsStudent={isAuthenticatedAsStudent}
                                startHour={startHour}
                                linkedStudents={linkedStudents}
                                handleSelectSlot={handleSelectSlot}
                                handleToggleDialogStudentSubscribe={handleToggleDialogStudentSubscribe}
                                handleToggleDialogAdminOpen={handleToggleDialogAdminOpen}
                            />
                        ))}
                        <Dialog
                            open={isDialogAdminOpen}
                            title={`Vous pouvez désinscrire un étudiant du créneau de ${getStartHourFromSlot(slotSelected)}`}
                            content={<DialogAdminSlot
                                slot={slotSelected}
                                studentSelected={studentSelected}
                                handleSelectStudent={handleSelectStudent}
                            />}
                            handleOk={handleUnsubscribeStudent}
                            handleCancel={handleToggleDialogAdminOpen}
                        />
                        <Dialog
                            open={isDialogStudentSubscribe}
                            title={`Vous souhaitez vous inscrire au créneau de ${getStartHourFromSlot(slotSelected)} ?`}
                            content={<p>Une fois l'inscription à ce créneau validée, vous ne pourrez plus vous désinscrire.
                                En vous inscrivant à ce créneau vous vous engagez à y aller.</p>}
                            handleOk={handleSubscribeSlot}
                            handleCancel={handleToggleDialogStudentSubscribe}
                        />
                    </TableBody>
                </Table>
            </TableWrapper>
        </PaperStyled>
    </Body>);

export default SchedulerInViewMode;
