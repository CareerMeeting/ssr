import { compose, withState, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { withStyles } from '@material-ui/core';

import Content from './content';
import { subscribeStudentToSlot, unbindStudentFromSlot } from '../../redux/company/actions';
import { styles } from './styles';
import { isAdminUser, isStudentUser } from '../../helpers/session';

export const getStartHourFromSlot = (slot = {}) => path(['startHour'], slot);

const handlers = {
    handleSelectSlot: ({ setSlotSelected }) => (event, slot) => {
        setSlotSelected(slot);
    },
    handleSelectStudent: ({ seStudentSelected }) => (event) => {
        const userID = path(['target', 'value'], event);

        seStudentSelected(userID);
    },
    handleToggleDialogAdminOpen: ({ setDialogAdminOpen }) => () => {
        setDialogAdminOpen(prev => !prev);
    },
    handleUnsubscribeStudent: ({ unbindStudentFromSlotAction, setDialogAdminOpen, isAuthorizedAsAdmin, slotSelected = {}, studentSelected, companyID }) => () => {
        if (!isAuthorizedAsAdmin) return;

        console.log(slotSelected, studentSelected, companyID);
        const { _id } = slotSelected;
        unbindStudentFromSlotAction(companyID, _id, studentSelected);
        setDialogAdminOpen(prev => !prev);
    },
    handleToggleDialogStudentSubscribe: ({ setDialogStudentSubscribe }) => () => {
        setDialogStudentSubscribe(prev => !prev);
    },
    handleSubscribeSlot: ({ subscribeStudentToSlotAction, setDialogStudentSubscribe, isAuthenticatedAsStudent, slotSelected = {}, companyID }) => () => {
        if (!isAuthenticatedAsStudent || !companyID) return;

        console.log(slotSelected, companyID);
        const { _id } = slotSelected;
        subscribeStudentToSlotAction(companyID, _id);
        setDialogStudentSubscribe(prev => !prev);
    },
};

const mapStateToProps = ({ entities }, ownerProps) => {
    const { queryID } = ownerProps;
    const companyDataPath = queryID ? ['companies', queryID] : ['user'];
    const userType = path(['session', 'userType'], entities);
    const isAuthorizedAsAdmin = isAdminUser(userType);
    const isAuthenticatedAsStudent = isStudentUser(userType);

    return ({
        companyID: path([...companyDataPath, '_id'], entities),
        companyName: path([...companyDataPath, 'companyName'], entities),
        isLoading: path(['companies', 'isLoading'], entities),
        isAuthorizedAsAdmin,
        isAuthenticatedAsStudent,
        isAuthenticatedAsNotCompanyUser: isAuthorizedAsAdmin || isAuthenticatedAsStudent,
        schools: path([...companyDataPath, 'schools'], entities),
        slots: path([...companyDataPath, 'slots'], entities),
    });
};

const connectToStore = connect(
    mapStateToProps,
    {
        unbindStudentFromSlotAction: unbindStudentFromSlot,
        subscribeStudentToSlotAction: subscribeStudentToSlot,
    },
);

const container = compose(
    connectToStore,
    withState('slotSelected', 'setSlotSelected', {}),
    withState('studentSelected', 'seStudentSelected', null),
    withState('isDialogAdminOpen', 'setDialogAdminOpen', false),
    withState('isDialogStudentSubscribe', 'setDialogStudentSubscribe', false),
    withHandlers(handlers),
    withStyles(styles),
);

export default container(Content);
