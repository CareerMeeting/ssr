import styled from 'react-emotion';
import { TableCell } from '@material-ui/core';
import { info, white } from '../../../app/colors';

export const styles = theme => ({
    editIcon: {
        color: theme.palette.secondary.main,
    },
    tableCellCenter: {
        textAlign: 'center',
    },
    buttonValidate: {
        marginBlockEnd: '1em',
    },
});

export const TableCellStyled = styled(TableCell)(({ issubscribed }) => ({
    textAlign: 'center',
    backgroundColor: issubscribed && info,
    color: issubscribed && white,
}));
