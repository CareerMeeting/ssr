import React from 'react';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton/IconButton';
import TableRow from '@material-ui/core/TableRow/TableRow';
import { NavLink } from 'react-router-dom';
import TableCell from '@material-ui/core/TableCell/TableCell';
import { ButtonValidate } from '../../../views/SuperAdminSettings/styles';
import { TableCellStyled } from './styles';

const SchedulerInEditMode = (
    {
        _id,
        classes,
        handleSelectSlot,
        handleToggleDialogStudentSubscribe,
        handleToggleDialogAdminOpen,
        isAvailable,
        canSubscribe,
        hasSubscribed,
        isAuthenticatedAsStudent,
        isAuthorizedAsAdmin,
        linkedStudents = [],
        startHour,
    } : {
        _id: String,
        classes: any,
        handleToggleDialogStudentSubscribe: Function,
        handleSelectSlot: Function,
        handleToggleDialogAdminOpen: Function,
        isAuthenticatedAsStudent: boolean,
        canSubscribe: boolean,
        isAuthorizedAsAdmin: boolean,
        linkedStudents: Array,
        startHour: String,
    }) =>
    (<TableRow
        hover
        tabIndex={-1}
        key={_id}
    >
        <TableCellStyled
            className={classes.tableCellCenter}
            component="th"
            scope="row"
            padding="none"
            issubscribed={(isAuthenticatedAsStudent && hasSubscribed) ? 1 : 0}
        >
            <p>{startHour}</p>
            {!isAuthenticatedAsStudent && (
                <ul>
                    {linkedStudents.map(({ userID, firstName, lastName = '', school = '' }) => (
                        <p key={userID}>
                            <NavLink to={`/student/profile?id=${userID}`}>{lastName.toUpperCase()} {firstName}</NavLink> de {school.toUpperCase()}</p>
                    ))}
                </ul>
            )}
            {isAuthenticatedAsStudent && hasSubscribed && <p> Vous êtes inscris à ce créneau</p>}
            {isAuthenticatedAsStudent && canSubscribe && !hasSubscribed && isAvailable && (
                <ButtonValidate
                    className={classes.buttonValidate}
                    variant="outlined"
                    color="secondary"
                    onClick={(event) => {
                        handleSelectSlot(event, { _id, startHour });
                        handleToggleDialogStudentSubscribe();
                    }}
                >
                    S'inscrire à ce créneau
                </ButtonValidate>
            )}
        </TableCellStyled>
        {isAuthorizedAsAdmin && (
            <TableCell>
                <IconButton onClick={(event) => {
                    handleSelectSlot(event, { _id, linkedStudents, startHour });
                    handleToggleDialogAdminOpen();
                }}
                >
                    <EditIcon fontSize="small" className={classes.editIcon} />
                </IconButton>
            </TableCell>
        )}
    </TableRow>);

export default SchedulerInEditMode;
