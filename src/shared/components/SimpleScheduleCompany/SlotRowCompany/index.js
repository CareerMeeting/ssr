import { withStyles } from '@material-ui/core';
import { compose } from 'recompose';
import { pathOr } from 'ramda';
import moment from 'moment';
import { connect } from 'react-redux';

import Content from './content';
import { styles } from './styles';

const mapStateToProps = ({ entities }) => {
    const eventDate = pathOr(new Date(), ['webConfig', 'eventDate'], entities);
    const maxAbilityToSubscribeBeforeEventDate = 3;
    const maxDateToSubscribe = moment(eventDate).subtract(maxAbilityToSubscribeBeforeEventDate, 'days').utc();
    const currentDate = moment().utc();
    const canSubscribe = currentDate.isSameOrBefore(maxDateToSubscribe, 'day');

    return ({
        canSubscribe,
    });
};

const connectToStore = connect(mapStateToProps, null);

const container = compose(
    connectToStore,
    withStyles(styles),
);

export default container(Content);
