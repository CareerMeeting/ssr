import React from 'react';

import WarningMessage from '../WarningMessage';

const Content = ({ closeSubscribeEventDateFormatted } : { closeSubscribeEventDateFormatted: String }) => (
    <WarningMessage message={`Vous avez jusqu'au ${closeSubscribeEventDateFormatted} pour vous inscrire aux créneaux des entreprises.`} />
);

export default Content;
