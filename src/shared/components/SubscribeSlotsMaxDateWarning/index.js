import moment from 'moment';
import withStyles from '@material-ui/core/styles/withStyles';
import { branch, compose, renderNothing } from 'recompose';
import { connect } from 'react-redux';
import { path, pathOr } from 'ramda';

import Content from './content';
import { styles } from './styles';
import { isStudentUser } from '../../helpers/session';

const mapStateToProps = ({ entities }) => {
    const userType = path(['session', 'userType'], entities);
    const eventDate = pathOr(new Date(), ['webConfig', 'eventDate'], entities);
    const maxAbilityToSubscribeBeforeEventDate = 3;
    const maxDateToSubscribe = moment(eventDate).subtract(maxAbilityToSubscribeBeforeEventDate, 'days').utc();
    const minDateToDisplayWarningSubscribe = moment(eventDate).subtract(maxAbilityToSubscribeBeforeEventDate + 5, 'days').utc();
    const currentDate = moment().utc();
    const closeSubscribeEventDateFormatted = isStudentUser(userType) && currentDate.isSameOrAfter(minDateToDisplayWarningSubscribe) &&
        currentDate.isSameOrBefore(maxDateToSubscribe, 'day') && moment(maxDateToSubscribe).locale('fr').format('Do MMMM YYYY');

    return ({
        closeSubscribeEventDateFormatted,
    });
};

const connectToStore = connect(mapStateToProps, null);

const enhance = compose(
    connectToStore,
    branch(({ closeSubscribeEventDateFormatted }) => !closeSubscribeEventDateFormatted, renderNothing),
    withStyles(styles),
);

export default enhance(Content);
