import { takeEvery } from 'redux-saga/effects';

import {
    ADMIN_REQUESTED,
    ADMINS_REQUESTED,
    ADMIN_CHANGE_PERIMETER_ACTIONS_REQUESTED,
    ADMIN_DEACTIVATION_REQUESTED,
    ADMIN_ADD_REQUESTED,
} from '../actions';
import { fetchAdmin, changePerimeterActions, fetchAdmins, deactivateAdminUser, fetchAddAdmin } from './sagas';

export default function* fetchAdminsSaga() {
    yield takeEvery(ADMIN_REQUESTED, fetchAdmin);
    yield takeEvery(ADMINS_REQUESTED, fetchAdmins);
    yield takeEvery(ADMIN_CHANGE_PERIMETER_ACTIONS_REQUESTED, changePerimeterActions);
    yield takeEvery(ADMIN_DEACTIVATION_REQUESTED, deactivateAdminUser);
    yield takeEvery(ADMIN_ADD_REQUESTED, fetchAddAdmin);
}
