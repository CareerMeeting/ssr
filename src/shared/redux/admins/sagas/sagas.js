import { call, put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';

import { getAPIQueryConfig } from '../../queries';
import { queryGetAdmin, queryChangePerimeterActionsAdminUser, queryDeactivateAdminUser, queryGetAdmins, queryAddAdmin } from '../queries';
import { ADMINS_FAILURE, ADMINS_SUCCESS } from '../actions';
import { isNilOrEmpty } from '../../../helpers/utils';

export function* fetchAdmin({ payload: { id } }) {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetAdmin(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 404) {
                yield put({
                    type: ADMINS_FAILURE,
                    error: {
                        status: 404,
                        message: 'Aucune donnée de votre profil n\'a pu être récupérée.',
                    },
                });
                return;
            }

            yield put({
                type: ADMINS_FAILURE,
                error: {
                    status: 401,
                    message: 'Une erreur d\'authentification est survenue.',
                },
            });
            return;
        }

        yield put({
            type: ADMINS_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: ADMINS_FAILURE,
            error,
        });
    }
}

export function* fetchAdmins() {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetAdmins());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 404) {
                yield put({
                    type: ADMINS_FAILURE,
                    error: {
                        status: 404,
                        message: 'Aucune donnée des administrateurs n\'a pu être récupérée.',
                    },
                });
                return;
            }

            yield put({
                type: ADMINS_FAILURE,
                error: {
                    status: 401,
                    message: 'Une erreur d\'authentification est survenue.',
                },
            });
            return;
        }

        yield put({
            type: ADMINS_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: ADMINS_FAILURE,
            error,
        });
    }
}

export function* fetchAddAdmin({ payload: { email, schools } }) {
    try {
        if (!email || isNilOrEmpty(schools)) {
            yield put({
                type: ADMINS_FAILURE,
                error: {
                    status: 400,
                    message: 'Veuillez rentrer un email valide et au moins une école sur laquelle l\'administrateur aura des actions.',
                },
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryAddAdmin(email, schools));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 404) {
                yield put({
                    type: ADMINS_FAILURE,
                    error: {
                        status: 404,
                        message: 'Aucune donnée de votre profil n\'a pu être récupérée.',
                    },
                });
                return;
            }

            yield put({
                type: ADMINS_FAILURE,
                error: {
                    status: 401,
                    message: 'Une erreur d\'authentification est survenue.',
                },
            });
            return;
        }

        yield put({
            type: ADMINS_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: ADMINS_FAILURE,
            error,
        });
    }
}

export function* changePerimeterActions({ payload: { id, schools } }) {

    try {
        if (!id || isNilOrEmpty(schools)) {
            yield put({
                type: ADMINS_FAILURE,
                error: {
                    status: 500,
                    message: 'Impossible de désactiver cet utilisateur.',
                },
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryChangePerimeterActionsAdminUser(id, schools));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: ADMINS_FAILURE,
                error: {
                    status: 401,
                    message: 'Impossible de changer le périmètre d\'actions.',
                },
            });
            return;
        }

        yield put({
            type: ADMINS_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: ADMINS_FAILURE,
            error,
        });
    }
}

export function* deactivateAdminUser({ payload: { id } }) {
    try {
        if (!id) {
            yield put({
                type: ADMINS_FAILURE,
                error: {
                    status: 500,
                    message: 'Impossible de désactiver cet utilisateur.',
                },
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryDeactivateAdminUser(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: ADMINS_FAILURE,
                error: {
                    status: 401,
                    message: 'Impossible de désactiver cet utilisateur.',
                },
            });
            return;
        }

        yield put({
            type: ADMINS_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: ADMINS_FAILURE,
            error,
        });
    }
}
