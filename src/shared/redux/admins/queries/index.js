import { pathOr } from 'ramda';
import { isNilOrEmpty } from '../../../helpers/utils';

export const queryGetAdmin = id => ({
    url: `/admins/user/${id}`,
    update: {
        admins: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { email, securityLevel, firstName, lastName, organizationName, schools, userID, registrationCodeForCompanies } =
            pathOr({}, ['user'], response);

        return {
            admins: {
                [id]: {
                    id: userID,
                    email,
                    firstName,
                    lastName,
                    organizationName,
                    schools,
                    securityLevel,
                    registrationCodeForCompanies,
                },
            },
        };
    },
});

export const queryGetAdmins = () => ({
    url: '/admins/super/users/',
    update: {
        admins: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const admins = pathOr({}, ['admins'], response);

        return {
            admins,
        };
    },
});

export const queryAddAdmin = (email, schools) => ({
    url: '/admins/super/sign/up',
    method: 'POST',
    body: {
        email,
        schools: !isNilOrEmpty(schools) && schools.join(';'),
    },
    update: {
        admins: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const admin = pathOr({}, ['admin'], response);
        const id = pathOr({}, ['_id'], admin);

        return {
            admins: {
                [id]: admin,
            },
        };
    },
});

export const queryChangePerimeterActionsAdminUser = (id, schools) => ({
    url: `/admins/super/users/${id}/schools-perimeter`,
    method: 'POST',
    body: {
        schools: schools.join(';'),
    },
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
});

export const queryDeactivateAdminUser = id => ({
    url: `/admins/super/users/${id}/deactivate`,
    method: 'POST',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
});
