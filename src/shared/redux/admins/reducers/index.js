import {
    ADMIN_ADD_REQUESTED,
    ADMINS_REQUESTED,
    ADMIN_REQUESTED,
    ADMINS_FAILURE,
    ADMINS_SUCCESS,
    ADMIN_DEACTIVATION_REQUESTED,
    ADMIN_CHANGE_PERIMETER_ACTIONS_REQUESTED,
} from '../actions';
import { failure, success } from '../../resultStates';

const initialState = {};

const AdminsReducers = (previousState = initialState, { type, error, hasRetrievedOnly }) => {
    switch (type) {
        case ADMIN_ADD_REQUESTED:
        case ADMINS_REQUESTED:
        case ADMIN_REQUESTED:
        case ADMIN_DEACTIVATION_REQUESTED:
        case ADMIN_CHANGE_PERIMETER_ACTIONS_REQUESTED:
            return { ...previousState, isLoading: true, error: null, result: null, hasRetrievedOnly: null };
        case ADMINS_FAILURE:
            return {
                ...previousState,
                isLoading: false,
                error,
                result: failure,
            };
        case ADMINS_SUCCESS:
            return { ...previousState, isLoading: false, error: null, result: success, hasRetrievedOnly };
        default:
            return previousState;
    }
};

export default AdminsReducers;
