export const ADMIN_REQUESTED = 'ADMIN_REQUESTED';
export const ADMINS_REQUESTED = 'ADMINS_REQUESTED';
export const ADMIN_CHANGE_PERIMETER_ACTIONS_REQUESTED = 'ADMIN_CHANGE_PERIMETER_ACTIONS_REQUESTED';
export const ADMIN_DEACTIVATION_REQUESTED = 'ADMIN_DEACTIVATION_REQUESTED';
export const ADMIN_ADD_REQUESTED = 'ADMIN_ADD_REQUESTED';

export const ADMINS_SUCCESS = 'ADMINS_SUCCESS';
export const ADMINS_FAILURE = 'ADMINS_FAILURE';

export const fetchAdminById = id => ({
    type: ADMIN_REQUESTED,
    payload: {
        id,
    },
});

export const fetchAdmins = () => ({
    type: ADMINS_REQUESTED,
});

export const addAdmin = (email, schools) => ({
    type: ADMIN_ADD_REQUESTED,
    payload: {
        email,
        schools,
    },
});

export const changeAdminPerimeterActions = (id, schools) => ({
    type: ADMIN_CHANGE_PERIMETER_ACTIONS_REQUESTED,
    payload: {
        id,
        schools,
    },
});

export const deactivateAdmin = id => ({
    type: ADMIN_DEACTIVATION_REQUESTED,
    payload: {
        id,
    },
});
