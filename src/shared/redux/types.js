// @flow

export type StateElementStructure = {
    isLoading: string,
    error: Object,
    result: string,
};

export type EntitiesStructure = Object;

