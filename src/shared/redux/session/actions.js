export const SESSION_SUCCEEDED = 'SESSION_SUCCEEDED';
export const SESSION_FAILED = 'SESSION_FAILED';
export const INIT_SESSION = 'INIT_SESSION';
export const LOG_OUT_SESSION = 'LOG_OUT_SESSION';

export const initSession = () => ({
    type: INIT_SESSION,
});

export const logOutSession = () => ({
    type: LOG_OUT_SESSION,
});
