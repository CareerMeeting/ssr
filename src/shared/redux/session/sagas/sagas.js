import { put } from 'redux-saga/effects';

import { SESSION_FAILED } from '../../session/actions';
import { getStorageLocalToken, getUserType } from '../../../helpers/session';
import { storage } from '../../../helpers/window';
import { tokenKey, userTypeKey } from '../../../helpers/session/constants';

export function* initSession() {
    try {
        const token = getStorageLocalToken();
        const userType = getUserType();
        if (!token || !userType) return;

        yield put({
            type: '@@query/MUTATE_SUCCESS',
            entities: {
                session: {
                    token,
                    userType,
                },
            },
        });
    } catch (error) {
        yield put({
            type: SESSION_FAILED,
            error: {
                status: 500,
                message: 'Une erreur est survenue.',
            },
        });
    }
}

export function* logOutSession() {
    try {
        const storageLocal = storage();
        if (storageLocal) {
            storage().removeItem(tokenKey);
            storage().removeItem(userTypeKey);
        }

        yield put({
            type: '@@query/MUTATE_SUCCESS',
            entities: {
                session: {
                    token: undefined,
                    userType: undefined,
                },
                user: undefined,
                students: undefined,
                companies: undefined,
                admins: undefined,
            },
        });
    } catch (error) {
        yield put({
            type: SESSION_FAILED,
            error: {
                status: 500,
                message: 'Une erreur est survenue.',
            },
        });
    }
}
