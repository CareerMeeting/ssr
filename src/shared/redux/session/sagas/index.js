import { takeEvery } from 'redux-saga/effects';

import { INIT_SESSION, LOG_OUT_SESSION } from '../actions';
import { initSession, logOutSession } from './sagas';

export default function* sessionSagas() {
    yield takeEvery(INIT_SESSION, initSession);
    yield takeEvery(LOG_OUT_SESSION, logOutSession);
}
