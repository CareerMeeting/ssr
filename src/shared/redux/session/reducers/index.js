import {
    SESSION_FAILED,
    SESSION_SUCCEEDED,
} from '../actions';
import {
    CM_TOKEN_WITH_MICROSOFT_ONLINE_AUTH_REQUESTED,
    MICROSOFT_ONLINE_AUTH_REQUESTED,
} from '../../microsoftOnline/actions';
import { failure, success } from '../../resultStates';
import { COMPANY_AUTH_REQUESTED, SEND_PASSWORD_MAIL, SET_COMPANY_PASSWORD } from '../../company/actions';

const initialState = [];

const SessionReducers = (previousState = initialState, { type, error }) => {
    switch (type) {
        case SEND_PASSWORD_MAIL:
        case SET_COMPANY_PASSWORD:
        case COMPANY_AUTH_REQUESTED:
        case MICROSOFT_ONLINE_AUTH_REQUESTED:
        case CM_TOKEN_WITH_MICROSOFT_ONLINE_AUTH_REQUESTED:
            return { ...previousState, isLoading: true, error: null, result: null };
        case SESSION_FAILED:
            return {
                ...previousState,
                isLoading: false,
                error,
                result: failure,
            };
        case SESSION_SUCCEEDED:
            return { ...previousState, isLoading: false, error: null, result: success };
        default:
            return previousState;
    }
};

export default SessionReducers;
