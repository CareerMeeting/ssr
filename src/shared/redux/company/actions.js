export const COMPANY_AUTH_REQUESTED = 'COMPANY_AUTH_REQUESTED';
export const COMPANIES_REQUESTED = 'COMPANIES_REQUESTED';
export const COMPANY_REQUESTED = 'COMPANY_REQUESTED';

export const COMPANY_SIGN_UP = 'COMPANY_SIGN_UP';

export const COMPANY_FAILURE = 'COMPANY_FAILURE';
export const COMPANY_SUCCESS = 'COMPANY_SUCCESS';

export const COMPANY_ADD_OFFERS_REQUESTED = 'COMPANY_ADD_OFFERS_REQUESTED';
export const COMPANY_GET_OFFER_REQUESTED = 'COMPANY_GET_OFFER_REQUESTED';
export const COMPANY_UPDATE_OFFER_REQUESTED = 'COMPANY_UPDATE_OFFER_REQUESTED';
export const COMPANY_DELETE_OFFER_REQUESTED = 'COMPANY_DELETE_OFFER_REQUESTED';
export const COMPANY_DEACTIVATION_REQUESTED = 'COMPANY_DEACTIVATION_REQUESTED';
export const COMPANY_ACTIVATION_REQUESTED = 'COMPANY_ACTIVATION_REQUESTED';

export const COMPANY_SLOTS_BY_ID_REQUESTED = 'COMPANY_SLOTS_BY_ID_REQUESTED';
export const UNBIND_STUDENT_FROM_SLOT_REQUESTED = 'UNBIND_STUDENT_FROM_SLOT_REQUESTED';
export const BIND_STUDENT_TO_SLOT_REQUESTED = 'BIND_STUDENT_TO_SLOT_REQUESTED';

export const SET_COMPANY_PASSWORD = 'SET_COMPANY_PASSWORD';
export const SEND_PASSWORD_MAIL = 'SEND_PASSWORD_MAIL';

export const authenticateCompany = (email, password) => ({
    type: COMPANY_AUTH_REQUESTED,
    payload: {
        email,
        password,
    },
});

export const addTraineeshipOffers = (form, schools) => ({
    type: COMPANY_ADD_OFFERS_REQUESTED,
    payload: {
        form,
        schools,
    },
});

export const deleteTraineeshipOffers = traineeshipOfferID => ({
    type: COMPANY_DELETE_OFFER_REQUESTED,
    payload: {
        traineeshipOfferID,
    },
});

export const getTraineeshipOffers = (id, traineeshipOfferID, pos) => ({
    type: COMPANY_GET_OFFER_REQUESTED,
    payload: {
        id,
        traineeshipOfferID,
        pos,
    },
});

export const upadteTraineeshipOffers = (traineeshipOfferID, pos, form, schools) => ({
    type: COMPANY_UPDATE_OFFER_REQUESTED,
    payload: {
        traineeshipOfferID,
        pos,
        form,
        schools,
    },
});

export const companyDeactivation = id => ({
    type: COMPANY_DEACTIVATION_REQUESTED,
    payload: {
        id,
    },
});

export const companyActivation = id => ({
    type: COMPANY_ACTIVATION_REQUESTED,
    payload: {
        id,
    },
});

export const getCompany = id => ({
    type: COMPANY_REQUESTED,
    payload: {
        id,
    },
});

export const signUpCompany = (form, schools, registrationCode) => ({
    type: COMPANY_SIGN_UP,
    payload: {
        form,
        schools,
        registrationCode,
    },
});

export const fetchCompanies = () => ({
    type: COMPANIES_REQUESTED,
});

export const fetchCompanySlotsByID = id => ({
    type: COMPANY_SLOTS_BY_ID_REQUESTED,
    payload: {
        id,
    },
});

export const unbindStudentFromSlot = (companyId, slotID, studentID) => ({
    type: UNBIND_STUDENT_FROM_SLOT_REQUESTED,
    payload: {
        companyId,
        slotID,
        studentID,
    },
});

export const subscribeStudentToSlot = (companyId, slotID) => ({
    type: BIND_STUDENT_TO_SLOT_REQUESTED,
    payload: {
        companyId,
        slotID,
    },
});


export const querySetCompanyPassword = (code, newPass) => ({
    type: SET_COMPANY_PASSWORD,
    payload: {
        code,
        newPass,
    },
});

export const querySendResetPasswordMail = email => ({
    type: SEND_PASSWORD_MAIL,
    payload: { email },
});
