import {
    COMPANIES_REQUESTED,
    COMPANY_GET_OFFER_REQUESTED,
    COMPANY_UPDATE_OFFER_REQUESTED,
    COMPANY_FAILURE,
    COMPANY_SUCCESS,
    COMPANY_SLOTS_BY_ID_REQUESTED,
    UNBIND_STUDENT_FROM_SLOT_REQUESTED, BIND_STUDENT_TO_SLOT_REQUESTED,
} from '../actions';
import { failure, success } from '../../resultStates';

const initialState = {};

const Reducers = (previousState = initialState, { type, error, hasRetrievedOnly }) => {
    switch (type) {
        case BIND_STUDENT_TO_SLOT_REQUESTED:
        case UNBIND_STUDENT_FROM_SLOT_REQUESTED:
        case COMPANY_SLOTS_BY_ID_REQUESTED:
        case COMPANY_UPDATE_OFFER_REQUESTED:
        case COMPANY_GET_OFFER_REQUESTED:
        case COMPANIES_REQUESTED:
            return { ...previousState, isLoading: true, error: null, result: null, hasRetrievedOnly: null };
        case COMPANY_FAILURE:
            return {
                ...previousState,
                isLoading: false,
                error,
                result: failure,
            };
        case COMPANY_SUCCESS:
            return { ...previousState, isLoading: false, error: null, result: success, hasRetrievedOnly };
        default:
            return previousState;
    }
};

export default Reducers;
