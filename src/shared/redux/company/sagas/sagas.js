import { call, put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';

import { getAPIQueryConfig } from '../../queries';

import {
    queryGetCompanies,
    queryGetCompanyToken,
    querySignUpCompany,
    queryAddTraineeshipOffers,
    queryDeleteTraineeshipOffers,
    queryGetTraineeshipOffer,
    queryUpdateTraineeshipOffer,
    queryDeactivateCompanyUser,
    queryGetCompany,
    queryActivateCompanyUser,
    querySendResetPasswordMail,
    querySetUserPassword,
} from '../queries';

import { SESSION_FAILED, SESSION_SUCCEEDED } from '../../session/actions';
import { COMPANY_FAILURE, COMPANY_SUCCESS } from '../actions';
import { USER_FAILURE, USER_SUCCESS } from '../../user/actions';

const delay = ms => new Promise(res => setTimeout(res, ms));

export function* authenticateCompany({ payload: { email, password } }) {
    if (!email && !password) {
        yield put({ type: SESSION_FAILED, error: { status: 400, message: 'Email ou Mot de passe manquant.' } });
        return;
    }

    try {
        yield delay(2000);
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetCompanyToken(email, password));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 404) {
                yield put({
                    type: SESSION_FAILED,
                    error: {
                        status: 404,
                        message: 'Aucun compte lié à cette adresse email.',
                    },
                });
                return;
            }

            yield put({
                type: SESSION_FAILED,
                error: {
                    status: 401,
                    message: 'Une erreur d\'authentification est survenue.',
                },
            });
            return;
        }

        yield put({
            type: SESSION_SUCCEEDED,
        });
    } catch (error) {
        yield put({
            type: SESSION_FAILED,
            error,
        });
    }
}

export function* addTraineeshipOffers({ payload: { form, schools } }) {
    if (!form && !schools) {
        yield put({ type: COMPANY_FAILURE, error: { status: 400, message: 'Formulaire incomplet' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryAddTraineeshipOffers(form, schools));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    message: 'Paramètres manquants',
                },
            });
            return;
        }


        yield put({
            type: COMPANY_SUCCESS,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error,
        });
    }
}

export function* getTraineeshipOffer({ payload: { id, traineeshipOfferID, pos } }) {
    if (!id && !traineeshipOfferID && !pos) {
        yield put({ type: COMPANY_FAILURE, error: { status: 400, message: 'Impossible de récupérer l\'offre sélectionné' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetTraineeshipOffer(id, traineeshipOfferID, pos));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    message: 'Impossible de récupérer l\'offre sélectionné',
                },
            });
            return;
        }

        yield put({
            type: COMPANY_SUCCESS,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error,
        });
    }
}

export function* deleteTraineeshipOffer({ payload: { traineeshipOfferID } }) {
    if (!traineeshipOfferID) {
        yield put({ type: COMPANY_FAILURE, error: { status: 400, message: 'Impossible de supprimer l\'offre sélectionné' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryDeleteTraineeshipOffers(traineeshipOfferID));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    message: 'Impossible de supprimer l\'offre sélectionné',
                },
            });
            return;
        }

        yield put({
            type: COMPANY_SUCCESS,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error,
        });
    }
}

export function* updateTraineeshipOffer({ payload: { traineeshipOfferID, pos, form, schools } }) {
    if (!traineeshipOfferID && !pos && !form && !schools) {
        yield put({ type: COMPANY_FAILURE, error: { status: 400, message: 'Impossible de mettre à jour l\'offre' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryUpdateTraineeshipOffer(traineeshipOfferID, pos, form, schools));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    message: 'Impossible de mettre à jour l\'offre',
                },
            });
            return;
        }


        yield put({
            type: COMPANY_SUCCESS,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error,
        });
    }
}

export function* signUpCompany({ payload: { form, schools, registrationCode } }) {
    if (!form && !schools && !registrationCode) {
        yield put({ type: SESSION_FAILED, error: { status: 400, message: 'Formulaire d\'inscription incomplet' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, querySignUpCompany(form, schools, registrationCode));
        const { status, body } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: SESSION_FAILED,
                error: body,
            });
            return;
        }


        yield put({
            type: SESSION_SUCCEEDED,
        });
    } catch (error) {
        yield put({
            type: SESSION_FAILED,
            error,
        });
    }
}

export function* fetchCompanies() {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetCompanies());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    status: 401,
                    message: 'Impossible de récupérer les entreprises.',
                },
            });
            return;
        }


        yield put({
            type: COMPANY_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error,
        });
    }
}

export function* deactivateCompanyUser({ payload: { id } }) {
    try {
        if (!id) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    status: 500,
                    message: 'Impossible de désactiver cet utilisateur.',
                },
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryDeactivateCompanyUser(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    status: 401,
                    message: 'Impossible de désactiver cet utilisateur.',
                },
            });
            return;
        }

        yield put({
            type: COMPANY_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error,
        });
    }
}


export function* activateCompanyUser({ payload: { id } }) {
    try {
        if (!id) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    status: 500,
                    message: 'Impossible d\'activer cet utilisateur.',
                },
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryActivateCompanyUser(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    status: 401,
                    message: 'Impossible d\'activer cet utilisateur.',
                },
            });
            return;
        }

        yield put({
            type: COMPANY_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error,
        });
    }
}

export function* getCompany({ payload: { id } }) {
    try {
        if (!id) {
            yield put({
                type: USER_FAILURE,
                error: {
                    status: 500,
                    message: 'Impossible de récupérer cet utilisateur.',
                },
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetCompany(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    status: 401,
                    message: 'Impossible de récupérer cet utilisateur.',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* sendResetPasswordMail({ payload: { email } }) {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, querySendResetPasswordMail(email));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: SESSION_FAILED,
                error: {
                    status: 401,
                    message: 'Email incorrecte',
                },
            });
            return;
        }

        yield put({
            type: SESSION_SUCCEEDED,
        });
    } catch (error) {
        yield put({
            type: SESSION_FAILED,
            error,
        });
    }
}

export function* setUserPassword({ payload: { code, newPass } }) {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, querySetUserPassword(code, newPass));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: SESSION_FAILED,
                error: {
                    status: 401,
                    message: 'Impossible de mettre à jour le mot de passe',
                },
            });
            return;
        }

        yield put({
            type: SESSION_SUCCEEDED,
        });
    } catch (error) {
        yield put({
            type: SESSION_FAILED,
            error,
        });
    }
}
