import { call, put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';

import { getAPIQueryConfig } from '../../queries';
import { queryGetSlots, queryStudentSubscribeToSlot, queryUnbindStudentFromSlot } from '../queries';

import { COMPANY_FAILURE, COMPANY_SUCCESS } from '../actions';

export function* getSlots({ payload: { id } }) {
    if (!id) {
        yield put({ type: COMPANY_FAILURE, error: { status: 400, message: 'Impossible de récupérer le planning l\'entreprise.' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetSlots(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    message: 'Impossible de récupérer le planning.',
                },
            });
            return;
        }

        yield put({
            type: COMPANY_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error,
        });
    }
}

export function* unbindStudentFromSlot({ payload: { companyId, slotID, studentID } }) {
    if (!companyId || !slotID || !studentID) {
        yield put({ type: COMPANY_FAILURE, error: { status: 400, message: 'Impossible de désinscrire cet étudiant du créneau voulu.' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryUnbindStudentFromSlot(companyId, slotID, studentID));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    message: 'Impossible de désinscrire cet étudiant du créneau voulu.',
                },
            });
            return;
        }

        yield put({
            type: COMPANY_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error: { status: 400, message: 'Impossible de désinscrire cet étudiant du créneau voulu.' },
        });
    }
}

export function* bindStudentToSlot({ payload: { companyId, slotID } }) {
    if (!companyId || !slotID) {
        yield put({ type: COMPANY_FAILURE, error: { status: 400, message: 'Impossible de vous inscrire à ce créneau pour le moment.' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryStudentSubscribeToSlot(companyId, slotID));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: COMPANY_FAILURE,
                error: {
                    message: 'Impossible de vous inscrire à ce créneau pour le moment.',
                },
            });
            return;
        }

        yield put({
            type: COMPANY_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: COMPANY_FAILURE,
            error: { status: 400, message: 'Impossible de vous inscrire à ce créneau pour le moment.' },
        });
    }
}
