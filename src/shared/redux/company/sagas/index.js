import { takeEvery } from 'redux-saga/effects';

import {
    COMPANY_AUTH_REQUESTED,
    COMPANY_SIGN_UP,
    COMPANY_REQUESTED,
    COMPANIES_REQUESTED,
    COMPANY_ADD_OFFERS_REQUESTED,
    COMPANY_DELETE_OFFER_REQUESTED,
    COMPANY_GET_OFFER_REQUESTED,
    COMPANY_UPDATE_OFFER_REQUESTED,
    COMPANY_DEACTIVATION_REQUESTED,
    COMPANY_ACTIVATION_REQUESTED,
    COMPANY_SLOTS_BY_ID_REQUESTED,
    UNBIND_STUDENT_FROM_SLOT_REQUESTED,
    BIND_STUDENT_TO_SLOT_REQUESTED,
    SEND_PASSWORD_MAIL,
    SET_COMPANY_PASSWORD,
} from '../actions';
import {
    authenticateCompany,
    signUpCompany,
    getCompany,
    fetchCompanies,
    addTraineeshipOffers,
    deleteTraineeshipOffer,
    getTraineeshipOffer,
    updateTraineeshipOffer,
    deactivateCompanyUser,
    activateCompanyUser,
    setUserPassword,
    sendResetPasswordMail,
} from './sagas';
import { bindStudentToSlot, getSlots, unbindStudentFromSlot } from './slots';

export default function* fetchCompanySaga() {
    yield takeEvery(COMPANY_AUTH_REQUESTED, authenticateCompany);
    yield takeEvery(COMPANY_SIGN_UP, signUpCompany);
    yield takeEvery(COMPANY_REQUESTED, getCompany);
    yield takeEvery(COMPANIES_REQUESTED, fetchCompanies);
    yield takeEvery(COMPANY_ADD_OFFERS_REQUESTED, addTraineeshipOffers);
    yield takeEvery(COMPANY_GET_OFFER_REQUESTED, getTraineeshipOffer);
    yield takeEvery(COMPANY_UPDATE_OFFER_REQUESTED, updateTraineeshipOffer);
    yield takeEvery(COMPANY_DELETE_OFFER_REQUESTED, deleteTraineeshipOffer);
    yield takeEvery(COMPANY_DEACTIVATION_REQUESTED, deactivateCompanyUser);
    yield takeEvery(COMPANY_ACTIVATION_REQUESTED, activateCompanyUser);
    yield takeEvery(COMPANY_SLOTS_BY_ID_REQUESTED, getSlots);
    yield takeEvery(UNBIND_STUDENT_FROM_SLOT_REQUESTED, unbindStudentFromSlot);
    yield takeEvery(BIND_STUDENT_TO_SLOT_REQUESTED, bindStudentToSlot);
    yield takeEvery(SEND_PASSWORD_MAIL, sendResetPasswordMail);
    yield takeEvery(SET_COMPANY_PASSWORD, setUserPassword);
}
