import { filter, pathOr, reject, } from 'ramda';

import { storage } from '../../../helpers/window';
import { tokenKey, userTypeKey } from '../../../helpers/session/constants';

export const queryGetCompanyToken = (email, password) => ({
    url: '/companies/sign/in',
    method: 'POST',
    body: {
        email,
        password,
    },
    update: {
        session: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { token, userType = '' } = response;
        const storageLocal = storage();
        if (storageLocal) {
            storage().setItem(tokenKey, token);
            storage().setItem(userTypeKey, userType);
        }

        return {
            session: {
                token,
                userType: userType.toString(),
            },
        };
    },
});

export const queryAddTraineeshipOffers = (form, schoolsTargeted) => ({
    url: '/companies/traineeship-offer',
    method: 'POST',
    body: {
        ...form,
        schoolsTargeted,
    },
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { company: { traineeshipOffers } } = response;

        return {
            user: {
                traineeshipOffers,
            },
        };
    },
});

export const queryDeleteTraineeshipOffers = traineeshipOfferID => ({
    url: `/companies/traineeship-offer/${traineeshipOfferID}`,
    method: 'DELETE',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { user } = response;

        return {
            user: {
                ...user,
            },
        };
    },
});

export const queryGetTraineeshipOffer = (id, traineeshipOfferID, pos) => ({
    url: `/companies/${id}/traineeship-offer/${traineeshipOfferID}`,
    update: {
        user: (prev, nextOffer) => {
            const traineeshipOffers = pathOr({}, ['traineeshipOffers'], prev);
            if (traineeshipOffers) traineeshipOffers[pos] = nextOffer;

            return {
                ...prev,
                traineeshipOffers: [
                    ...traineeshipOffers,
                ],
            };
        },
        companies: (prev, next) => {
            const company = pathOr({}, [id], prev);
            const traineeshipOffers = pathOr([], ['traineeshipOffers'], company);
            if (traineeshipOffers) traineeshipOffers[pos] = next[id];

            return ({
                ...prev,
                [id]: {
                    ...company,
                    traineeshipOffers: [
                        ...traineeshipOffers,
                    ],
                },
            });
        },
    },
    transform: (response: ?Object) => {
        const { traineeshipOffer } = response;

        return {
            user: {
                ...traineeshipOffer,
            },
            companies: {
                [id]: {
                    ...traineeshipOffer,
                },
            },
        };
    },
});

export const queryUpdateTraineeshipOffer = (traineeshipOfferID, pos, form, schoolsTargeted) => ({
    url: `/companies/traineeship-offer/${traineeshipOfferID}`,
    method: 'PUT',
    body: {
        ...form,
        schoolsTargeted,
    },
    update: {
        user: (prev, nextOffer) => {
            const { traineeshipOffers } = prev;
            traineeshipOffers[pos] = nextOffer;

            return {
                ...prev,
                traineeshipOffers: [
                    ...traineeshipOffers,
                ],
            };
        },
    },
    transform: (response: ?Object) => {
        const { traineeshipOffer } = response;

        return {
            user: {
                ...traineeshipOffer,
            },
        };
    },
});

export const querySignUpCompany = (form, schools, registrationCode) => ({
    url: '/companies/sign/up',
    method: 'POST',
    body: {
        ...form,
        schools,
        registrationCode,
    },
    update: {
        session: (prev, next) => ({
            ...prev,
            ...next,
        }),
        codeChecker: () => {},
        resetPass: () => {},
    },
    transform: () => ({
        session: {
            isSignedUp: true,
        },
    }),
});

export const queryGetCompanies = () => ({
    url: '/companies/users/',
    update: {
        companies: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { companies } = response;

        return {
            companies,
        };
    },
});

export const queryGetCompany = id => ({
    url: `/companies/user/${id}`,
    update: {
        companies: (prev = {}, next = {}) => {
            const company = pathOr({}, [id], prev);

            return ({
                ...prev,
                [id]: {
                    ...company,
                    ...next[id],
                },
            });
        },
    },
    transform: (response: ?Object) => {
        const { company } = response;

        return {
            companies: {
                [id]: {
                    ...company,
                },
            },
        };
    },
});

export const queryDeactivateCompanyUser = id => ({
    url: `/companies/users/${id}/deactivate`,
    method: 'POST',
    update: {
        companies: (prev = {}, next = {}) => {
            const company = pathOr({}, [id], prev);

            return ({
                ...prev,
                [id]: {
                    ...company,
                    ...next[id],
                },
            });
        },
    },
    transform: () => ({
        companies: {
            [id]: {
                isActivated: false,
                isDeactivatedBySuperAdmin: true,
            },
        },
    }),
});

export const queryActivateCompanyUser = id => ({
    url: `/companies/users/${id}/activate`,
    method: 'POST',
    update: {
        companies: (prev = {}, next = {}) => {
            const company = pathOr({}, [id], prev);

            return ({
                ...prev,
                [id]: {
                    ...company,
                    ...next[id],
                },
            });
        },
    },
    transform: () => ({
        companies: {
            [id]: {
                isActivated: true,
                isDeactivatedBySuperAdmin: false,
            },
        },
    }),
});

export const queryGetSlots = id => ({
    url: `/companies/${id}/slot/`,
    update: {
        companies: (prev = {}, next = {}) => {
            const company = pathOr({}, [id], prev);
            const { slots = [] } = next[id] || {};

            return ({
                ...prev,
                [id]: {
                    ...company,
                    slots,
                },
            });
        },
    },
    transform: (response: ?Object) => {
        const { slots } = response;

        return {
            companies: {
                [id]: {
                    slots,
                },
            },
        };
    },
});

export const queryUnbindStudentFromSlot = (companyId, slotID, studentID) => ({
    url: `/companies/${companyId}/slot/${slotID}/unbind-student/${studentID}`,
    method: 'POST',
    update: {
        companies: (prev = {}) => {
            const company = pathOr({}, [companyId], prev);
            const { slots = [] } = company || {};

            return ({
                ...prev,
                [companyId]: {
                    ...company,
                    slots: filter((slot = {}) => slot._id !== slotID, slots),
                },
            });
        },
        students: (prev = {}) => {
            const student = pathOr({}, [studentID], prev);
            const { slots } = student;
            if (!slots) return prev;

            return ({
                ...prev,
                [studentID]: {
                    ...student,
                    slots: reject((slot = {}) => slot.slotID === slotID, slots),
                },
            });
        },
    },
    transform: (response: ?Object) => {
        const { slot } = response;

        return {
            companies: {
                [companyId]: {
                    slot,
                },
            },
        };
    },
});

export const queryStudentSubscribeToSlot = (companyId, slotID) => ({
    url: `/companies/${companyId}/slot/${slotID}/bind-student`,
    method: 'POST',
    update: {
        companies: (prev = {}, next = {}) => {
            const company = pathOr({}, [companyId], prev);
            const { slots = {} } = next[companyId] || {};

            return ({
                ...prev,
                [companyId]: {
                    ...company,
                    slots,
                },
            });
        },
    },
    transform: (response: ?Object) => {
        const { slots } = response;

        return {
            companies: {
                [companyId]: {
                    slots,
                },
            },
        };
    },
});

export const querySetUserPassword = (code, newPass) => ({
    url: '/companies/sign/pass/reset',
    method: 'POST',
    body: {
        code,
        newPass,
    },
    update: {
        resetPass: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: () => ({
        resetPass: {
            hasSentEmail: true,
        },
    }),
});

export const querySendResetPasswordMail = email => ({
    url: '/companies/sign/pass/send',
    method: 'POST',
    body: {
        email,
    },
    update: {
        resetPass: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: () => ({
        resetPass: {
            hasUpdatedPass: true,
        },
    }),
});

