import { select } from 'redux-saga/effects';
import { path } from 'ramda';
import axios from "axios";

const domainAPI = process.env.DOMAIN_API || 'http://localhost:4000';

export const apiBaseUrl = `${domainAPI}/api/v1`;

const getToken = state => path(['entities', 'session', 'token'], state);

export function* getAPIQueryConfig({ url, body, method, update, transform, isSendingFile, ...others }) {
    const token = yield select(getToken);
    const contentTypeHeader = isSendingFile ? {} : {
        'Content-Type': 'application/json',
    };

    return ({
        url: `${apiBaseUrl}${url}`,
        body,
        options: {
            method: method || 'GET',
            headers: {
                ...contentTypeHeader,
                'x-access-token': token,
            },
        },
        update,
        transform,
        ...others,
    });
}

export const fetchDocumentWithAxios = async ({ url, options: { headers } }) => axios.get(url, {
    headers,
    responseType: 'blob',
});
