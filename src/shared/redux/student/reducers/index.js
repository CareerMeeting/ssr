import {
    STUDENT_BY_ID_REQUESTED,
    STUDENTS_REQUESTED,
    STUDENT_FAILURE,
    STUDENT_SUCCESS,
    STUDENT_CV,
    STUDENT_DEACTIVATE,
    STUDENT_ACTIVATE,
    STUDENT_SLOTS_BY_ID_REQUESTED,
} from '../actions';
import { failure, success } from '../../resultStates';

const initialState = {};

const Reducers = (previousState = initialState, { type, error, hasRetrievedOnly }) => {
    switch (type) {
        case STUDENT_SLOTS_BY_ID_REQUESTED:
        case STUDENT_DEACTIVATE:
        case STUDENT_ACTIVATE:
        case STUDENT_BY_ID_REQUESTED:
        case STUDENTS_REQUESTED:
        case STUDENT_CV:
            return { ...previousState, isLoading: true, error: null, result: null, hasRetrievedOnly: null };
        case STUDENT_FAILURE:
            return {
                ...previousState,
                isLoading: false,
                error,
                result: failure,
            };
        case STUDENT_SUCCESS:
            return { ...previousState, isLoading: false, error: null, result: success, hasRetrievedOnly };
        default:
            return previousState;
    }
};

export default Reducers;
