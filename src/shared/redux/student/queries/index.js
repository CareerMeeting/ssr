import { pathOr } from 'ramda';

export const queryGetStudents = () => ({
    url: '/students/users/',
    update: {
        students: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { students } = response;

        return {
            students,
        };
    },
});

export const queryGetStudent = id => ({
    url: `/students/user/${id}`,
    update: {
        students: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const student = pathOr({}, ['student'], response);

        return {
            students: {
                [id]: {
                    ...student,
                },
            },
        };
    },
});

export const queryGetStudentCV = id => ({
    url: `/students/user/${id}/download/cv`,
});

export const queryActivateStudent = id => ({
    url: `/students/user/${id}/activate`,
    method: 'POST',
    update: {
        students: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const student = pathOr({}, ['student'], response);

        return {
            students: {
                [id]: {
                    ...student,
                },
            },
        };
    },
});

export const queryDeactivateStudent = id => ({
    url: `/students/user/${id}/deactivate`,
    method: 'POST',
    update: {
        students: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const student = pathOr({}, ['student'], response);

        return {
            students: {
                [id]: {
                    ...student,
                },
            },
        };
    },
});

export const queryGetStudentSlots = id => ({
    url: `/students/${id}/slot/`,
    update: {
        students: (prev, next) => {
            const student = pathOr({}, [id], prev);
            const { slots = [] } = next[id] || {};

            return ({
                ...prev,
                [id]: {
                    ...student,
                    slots,
                },
            });
        },
    },
    transform: (response: ?Object) => {
        const { slots } = response;

        return {
            students: {
                [id]: {
                    slots,
                },
            },
        };
    },
});
