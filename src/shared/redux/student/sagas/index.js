import { takeEvery } from 'redux-saga/effects';

import {
    STUDENTS_REQUESTED,
    STUDENT_BY_ID_REQUESTED,
    STUDENT_CV,
    STUDENT_ACTIVATE,
    STUDENT_DEACTIVATE,
    STUDENT_SLOTS_BY_ID_REQUESTED,
} from '../actions';
import { fetchStudents, fetchStudent, fetchStudentCV, activateStudent, deactivateStudent } from './sagas';
import { getSlots } from './slots';

export default function* fetchStudentSaga() {
    yield takeEvery(STUDENTS_REQUESTED, fetchStudents);
    yield takeEvery(STUDENT_BY_ID_REQUESTED, fetchStudent);
    yield takeEvery(STUDENT_CV, fetchStudentCV);
    yield takeEvery(STUDENT_ACTIVATE, activateStudent);
    yield takeEvery(STUDENT_DEACTIVATE, deactivateStudent);
    yield takeEvery(STUDENT_SLOTS_BY_ID_REQUESTED, getSlots);
}
