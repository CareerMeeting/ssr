import { call, put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';
import FileDownload from 'js-file-download';

import { getAPIQueryConfig, fetchDocumentWithAxios } from '../../queries';
import { queryGetStudents, queryGetStudent, queryGetStudentCV, queryActivateStudent, queryDeactivateStudent } from '../queries';
import { STUDENT_FAILURE, STUDENT_SUCCESS } from '../actions';

export function* fetchStudents() {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetStudents());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: STUDENT_FAILURE,
                error: {
                    status: 401,
                    message: 'Impossible de récupérer la liste des étudiants.',
                },
            });
            return;
        }


        yield put({
            type: STUDENT_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: STUDENT_FAILURE,
            error,
        });
    }
}

export function* fetchStudent({ payload: { id } }) {
    if (!id) return;

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetStudent(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 404) {
                yield put({
                    type: STUDENT_FAILURE,
                    error: {
                        status: 404,
                        message: 'Aucune donnée de votre profil n\'a pu être récupérée.',
                    },
                });
                return;
            }

            yield put({
                type: STUDENT_FAILURE,
                error: {
                    status: 401,
                    message: 'Une erreur d\'authentification est survenue.',
                },
            });
            return;
        }

        yield put({
            type: STUDENT_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: STUDENT_FAILURE,
            error,
        });
    }
}

export function* fetchStudentCV({ payload: { id, firstName = '', lastName = '' } }) {
    if (!id) return;

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetStudentCV(id));
        const { status, data } = yield call(fetchDocumentWithAxios, apiQueryConfig);
        if (status < 200 || status >= 300) {
            if (status === 401) {
                yield put({
                    type: STUDENT_FAILURE,
                    error: {
                        status: 401,
                        message: 'Une erreur d\'authentification est survenue.',
                    },
                });
                return;
            }

            yield put({
                type: STUDENT_FAILURE,
                error: {
                    status: 404,
                    message: 'Aucun CV pour le moment.',
                },
            });
            return;
        }

        FileDownload(data, `cv.${lastName}.${firstName}.pdf`);
        yield put({
            type: STUDENT_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: STUDENT_FAILURE,
            error: {
                status: 404,
                message: 'Aucun CV pour le moment.',
            },
        });
    }
}

export function* activateStudent({ payload: { id } }) {
    if (!id) return;

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryActivateStudent(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 401) {
                yield put({
                    type: STUDENT_FAILURE,
                    error: {
                        status: 401,
                        message: 'Une erreur d\'authentification est survenue.',
                    },
                });
                return;
            }

            yield put({
                type: STUDENT_FAILURE,
                error: {
                    status: 404,
                    message: 'Impossible d\'activer l\'étudiant our le moment.',
                },
            });
            return;
        }

        yield put({
            type: STUDENT_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: STUDENT_FAILURE,
            error,
        });
    }
}

export function* deactivateStudent({ payload: { id } }) {
    if (!id) return;

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryDeactivateStudent(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 401) {
                yield put({
                    type: STUDENT_FAILURE,
                    error: {
                        status: 401,
                        message: 'Une erreur d\'authentification est survenue.',
                    },
                });
                return;
            }

            yield put({
                type: STUDENT_FAILURE,
                error: {
                    status: 404,
                    message: 'Impossible de désactiver l\'étudiant our le moment.',
                },
            });
            return;
        }

        yield put({
            type: STUDENT_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: STUDENT_FAILURE,
            error,
        });
    }
}
