import { call, put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';

import { getAPIQueryConfig } from '../../queries';
import { queryGetStudentSlots } from '../queries';

import { STUDENT_FAILURE, STUDENT_SUCCESS } from '../actions';

export function* getSlots({ payload: { id } }) {
    if (!id) {
        yield put({ type: STUDENT_FAILURE, error: { status: 400, message: 'Impossible de récupérer le planning l\'étudiant.' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetStudentSlots(id));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: STUDENT_FAILURE,
                error: {
                    message: 'Impossible de récupérer le planning.',
                },
            });
            return;
        }

        yield put({
            type: STUDENT_SUCCESS,
            hasRetrievedOnly: true,

        });
    } catch (error) {
        yield put({
            type: STUDENT_FAILURE,
            error,
        });
    }
}
