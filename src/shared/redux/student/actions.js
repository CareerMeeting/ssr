export const STUDENTS_REQUESTED = 'STUDENTS_REQUESTED';
export const STUDENT_BY_ID_REQUESTED = 'STUDENT_BY_ID_REQUESTED';
export const STUDENT_CV = 'STUDENT_CV';
export const STUDENT_ACTIVATE = 'STUDENT_ACTIVATE';
export const STUDENT_DEACTIVATE = 'STUDENT_DEACTIVATE';

export const STUDENT_SLOTS_BY_ID_REQUESTED = 'STUDENT_SLOTS_BY_ID_REQUESTED';

export const STUDENT_FAILURE = 'STUDENT_FAILURE';
export const STUDENT_SUCCESS = 'STUDENT_SUCCESS';

export const fetchStudents = () => ({
    type: STUDENTS_REQUESTED,
});

export const fetchStudentById = id => ({
    type: STUDENT_BY_ID_REQUESTED,
    payload: {
        id,
    },
});

export const fetchStudentCV = (id, firstName, lastName) => ({
    type: STUDENT_CV,
    payload: {
        id,
        firstName,
        lastName,
    },
});

export const activateStudent = id => ({
    type: STUDENT_ACTIVATE,
    payload: {
        id,
    },
});

export const deactivateStudent = id => ({
    type: STUDENT_DEACTIVATE,
    payload: {
        id,
    },
});

export const fetchStudentSlotsByID = id => ({
    type: STUDENT_SLOTS_BY_ID_REQUESTED,
    payload: {
        id,
    },
});
