import { all } from 'redux-saga/effects';

import fetchMicrosoftOnlineSaga from './microsoftOnline/sagas';
import fetchWebConfigSagas from './webConfig/sagas';
import fetchCompanySagas from './company/sagas';
import fetchUserSagas from './user/sagas';
import sessionSagas from './session/sagas';
import fetchCodeCheckerSaga from './codeChecker/sagas';
import fetchAdminsSaga from './admins/sagas';
import fetchStudentSaga from './student/sagas';

export default function* rootSaga() {
    yield all([
        fetchMicrosoftOnlineSaga(),
        fetchWebConfigSagas(),
        fetchCompanySagas(),
        fetchUserSagas(),
        sessionSagas(),
        fetchCodeCheckerSaga(),
        fetchAdminsSaga(),
        fetchStudentSaga(),
    ]);
}

