import {
    CHANGE_DEACTIVATION_TEXT_REQUESTED, CHANGE_SITE_STATE_REQUESTED,
    WEB_CONFIG_FAILED,
    WEB_CONFIG_REQUESTED,
    WEB_CONFIG_SUCCEEDED,
} from '../actions';
import { failure, success } from '../../resultStates';

const initialState = [];

const MOAuthReducers = (previousState = initialState, { type, error, hasRetrievedOnly }) => {
    switch (type) {
        case CHANGE_SITE_STATE_REQUESTED:
        case CHANGE_DEACTIVATION_TEXT_REQUESTED:
        case WEB_CONFIG_REQUESTED:
            return { ...previousState, isLoading: true, error: null, result: null, hasRetrievedOnly: null };
        case WEB_CONFIG_FAILED:
            return {
                ...previousState,
                isLoading: false,
                error,
                result: failure,
            };
        case WEB_CONFIG_SUCCEEDED:
            return { ...previousState, isLoading: false, error: null, result: success, hasRetrievedOnly };
        default:
            return previousState;
    }
};

export default MOAuthReducers;
