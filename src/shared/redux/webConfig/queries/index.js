import { pathOr } from 'ramda';

export const queryGetWebConfig = () => ({
    url: '/web/get-config',
    update: {
        webConfig: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const webConfig = pathOr({}, ['webConfig'], response);
        const {
            isActivated,
            deactivatedText,
            companyCodeRegistration,
            eventDate,
            schools,
            features = {},
        } = webConfig;

        return {
            webConfig: {
                features,
                eventDate,
                isActivated,
                deactivatedText,
                companyCodeRegistration,
                schools,
            },
        };
    },
});

export const querychangeDeactivationText = deactivatedText => ({
    method: 'POST',
    url: '/admins/super/web/deactivate-text',
    body: {
        deactivatedText,
    },
    update: {
        webConfig: prev => ({
            ...prev,
            deactivatedText: deactivatedText || '',
        }),
    },
});

export const querySetEventDate = eventDate => ({
    method: 'POST',
    url: '/admins/super/web/set-event-date',
    body: {
        eventDate,
    },
    update: {
        webConfig: prev => ({
            ...prev,
            eventDate: eventDate || '',
        }),
    },
});

const getUrlActionActivation = isActivated => (isActivated ? 'activate' : 'deactivate');

export const queryStateSite = isActivated => ({
    method: 'POST',
    url: `/admins/super/web/${getUrlActionActivation(isActivated)}`,
    update: {
        webConfig: prev => ({
            ...prev,
            isActivated,
        }),
    },
});
