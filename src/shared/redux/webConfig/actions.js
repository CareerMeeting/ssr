export const WEB_CONFIG_REQUESTED = 'WEB_CONFIG_REQUESTED';
export const WEB_CONFIG_SUCCEEDED = 'WEB_CONFIG_SUCCEEDED';
export const WEB_CONFIG_FAILED = 'WEB_CONFIG_FAILED';

export const CHANGE_DEACTIVATION_TEXT_REQUESTED = 'CHANGE_DEACTIVATION_TEXT_REQUESTED';
export const CHANGE_SITE_STATE_REQUESTED = 'CHANGE_SITE_STATE_REQUESTED';
export const SET_EVENT_DATE_REQUESTED = 'SET_EVENT_DATE_REQUESTED';

export const fetchWebConfig = () => ({
    type: WEB_CONFIG_REQUESTED,
});

export const changeDeactivationText = deactivationText => ({
    type: CHANGE_DEACTIVATION_TEXT_REQUESTED,
    payload: {
        deactivationText,
    },
});

export const setEventDate = eventDate => ({
    type: SET_EVENT_DATE_REQUESTED,
    payload: {
        eventDate,
    },
});

export const changeSiteState = isActivated => ({
    type: CHANGE_SITE_STATE_REQUESTED,
    payload: {
        isActivated,
    },
});
