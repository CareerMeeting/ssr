import { takeEvery } from 'redux-saga/effects';

import { CHANGE_DEACTIVATION_TEXT_REQUESTED, CHANGE_SITE_STATE_REQUESTED, WEB_CONFIG_REQUESTED, SET_EVENT_DATE_REQUESTED } from '../actions';
import { changeDeactivationText, fetchWebConfig, changeSiteState, setEventDate } from './sagas';

export default function* fetchSaga() {
    yield takeEvery(WEB_CONFIG_REQUESTED, fetchWebConfig);
    yield takeEvery(CHANGE_DEACTIVATION_TEXT_REQUESTED, changeDeactivationText);
    yield takeEvery(CHANGE_SITE_STATE_REQUESTED, changeSiteState);
    yield takeEvery(SET_EVENT_DATE_REQUESTED, setEventDate);
}
