import { call, put, select } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';
import { path } from 'ramda';

import {
    WEB_CONFIG_FAILED,
    WEB_CONFIG_SUCCEEDED,
} from '../actions';
import { getAPIQueryConfig } from '../../queries';
import { querychangeDeactivationText, queryGetWebConfig, queryStateSite, querySetEventDate } from '../queries';
import { isNilOrEmpty } from '../../../helpers/utils';

const getWebConfig = state => path(['entities', 'webConfig'], state);

export function* fetchWebConfig() {
    try {
        const isAlreadyInStore = yield select(getWebConfig);
        if (isAlreadyInStore) {
            yield put({
                type: WEB_CONFIG_SUCCEEDED,
                hasRetrievedOnly: true,
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetWebConfig());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: WEB_CONFIG_FAILED,
                error: {
                    status,
                    message: 'Impossible de récupérer les paramètres du site.',
                },
            });
            return;
        }

        yield put({
            type: WEB_CONFIG_SUCCEEDED,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: WEB_CONFIG_FAILED,
            error: {
                status: 500,
                message: 'Impossible de récupérer les paramètres du site.',
            },
        });
    }
}

export function* changeDeactivationText({ payload: { deactivationText } }) {
    if (isNilOrEmpty(deactivationText)) {
        yield put({
            type: WEB_CONFIG_FAILED,
            error: {
                status: 400,
                message: 'Veuillez entrer du texte dans le texte d\'affichage',
            },
        });
        return;
    }

    const errorMessage = 'Impossible de modifier le texte d\'affichage lorsque le site est désactivé. Veuillez réessayer plus tard.';
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, querychangeDeactivationText(deactivationText));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: WEB_CONFIG_FAILED,
                error: {
                    status,
                    message: errorMessage,
                },
            });
            return;
        }

        yield put({
            type: WEB_CONFIG_SUCCEEDED,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: WEB_CONFIG_FAILED,
            error: {
                status: 401,
                message: errorMessage,
            },
        });
    }
}

export function* setEventDate({ payload: { eventDate } }) {
    if (isNilOrEmpty(eventDate)) {
        yield put({
            type: WEB_CONFIG_FAILED,
            error: {
                status: 400,
                message: 'Veuillez insérer une date correcte',
            },
        });
        return;
    }

    const errorMessage = 'Impossible de modifier la date de l\'évenement lorsque le site est désactivé. Veuillez réessayer plus tard.';
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, querySetEventDate(eventDate));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: WEB_CONFIG_FAILED,
                error: {
                    status,
                    message: errorMessage,
                },
            });
            return;
        }

        yield put({
            type: WEB_CONFIG_SUCCEEDED,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: WEB_CONFIG_FAILED,
            error: {
                status: 401,
                message: errorMessage,
            },
        });
    }
}

export function* changeSiteState({ payload: { isActivated } }) {
    const errorMessage = 'Impossible d\'activer/désactiver le site pour le moment.';

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryStateSite(isActivated));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: WEB_CONFIG_FAILED,
                error: {
                    status,
                    message: errorMessage,
                },
            });
            return;
        }

        yield put({
            type: WEB_CONFIG_SUCCEEDED,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: WEB_CONFIG_FAILED,
            error: {
                status: 401,
                message: errorMessage,
            },
        });
    }
}
