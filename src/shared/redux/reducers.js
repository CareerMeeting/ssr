import SessionReducers from './session/reducers';
import WebConfigReducer from './webConfig/reducers';
import UserReducers from './user/reducers';
import CodeCheckerReducers from './codeChecker/reducers';
import AdminsReducers from './admins/reducers';
import CompaniesReducers from './company/reducers';
import StudentsReducers from './student/reducers';

const rootReducer = {
    session: SessionReducers,
    user: UserReducers,
    webConfig: WebConfigReducer,
    codeChecker: CodeCheckerReducers,
    admins: AdminsReducers,
    companies: CompaniesReducers,
    students: StudentsReducers,
};

export default rootReducer;
