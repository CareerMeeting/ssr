import {
    USER_SUCCESS,
    USER_FAILURE,
    ADMIN_USER_REQUESTED,
    COMPANY_USER_REQUESTED,
    STUDENT_USER_REQUESTED,
    STUDENT_USER_UPDATE,
    STUDENT_USER_UPDATE_CV,
    STUDENT_SLOTS_REQUESTED,
    COMPANY_SLOTS_REQUESTED,
    COMPANY_UPDATE_SLOT_REQUESTED,
    COMPANY_SCHOOLS_REQUESTED,
    COMPANY_ENABLE_SLOTS_REQUESTED,
} from '../actions';
import { failure, success } from '../../resultStates';

const initialState = {};

const UserReducers = (previousState = initialState, { type, error, hasRetrievedOnly }) => {
    switch (type) {
        case COMPANY_SCHOOLS_REQUESTED:
        case COMPANY_ENABLE_SLOTS_REQUESTED:
        case COMPANY_UPDATE_SLOT_REQUESTED:
        case COMPANY_SLOTS_REQUESTED:
        case STUDENT_SLOTS_REQUESTED:
        case STUDENT_USER_REQUESTED:
        case COMPANY_USER_REQUESTED:
        case STUDENT_USER_UPDATE:
        case STUDENT_USER_UPDATE_CV:
        case ADMIN_USER_REQUESTED:
            return { ...previousState, isLoading: true, error: null, result: null, hasRetrievedOnly: null };
        case USER_FAILURE:
            return {
                ...previousState,
                isLoading: false,
                error,
                result: failure,
            };
        case USER_SUCCESS:
            return { ...previousState, isLoading: false, error: null, result: success, hasRetrievedOnly };
        default:
            return previousState;
    }
};

export default UserReducers;
