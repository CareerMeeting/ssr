import { call, put, select } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';
import { path } from 'ramda';

import { getAPIQueryConfig } from '../../queries';
import {
    queryGetAdminUser,
    queryGetCompanyUser,
    queryUpdateCompanyUser,
    queryGetStudentUser,
    queryUpdateStudentUser,
    queryUploadStudentCV,
    queryGetCompanySchools,
} from '../queries';
import { USER_FAILURE, USER_SUCCESS } from '../actions';

const getUser = state => path(['entities', 'user', '_id'], state);

export function* fetchAdminUser() {
    try {
        const isAlreadyInStore = yield select(getUser);
        if (isAlreadyInStore) {
            yield put({
                type: USER_SUCCESS,
                hasRetrievedOnly: true,
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetAdminUser());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 404) {
                yield put({
                    type: USER_FAILURE,
                    error: {
                        status: 404,
                        message: 'Aucune donnée de votre profil n\'a pu être récupérée.',
                    },
                });
                return;
            }

            yield put({
                type: USER_FAILURE,
                error: {
                    status: 401,
                    message: 'Une erreur d\'authentification est survenue.',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* fetchCompanyUser() {
    try {
        const isAlreadyInStore = yield select(getUser);
        if (isAlreadyInStore) {
            yield put({
                type: USER_SUCCESS,
                hasRetrievedOnly: true,
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetCompanyUser());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Accès interdit au compte utilisateur',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* fetchStudentUser() {
    try {
        const isAlreadyInStore = yield select(getUser);
        if (isAlreadyInStore) {
            yield put({
                type: USER_SUCCESS,
                hasRetrievedOnly: true,
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetStudentUser());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Accès interdit au compte utilisateur',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* fetchCompanyUserUpdate({ payload: { data } }) {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryUpdateCompanyUser(data));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Mise à jour échoué',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* studentUserUpdate({ payload: { data } }) {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryUpdateCompanyUser(data));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Mise à jour échoué',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* updateStudentUser({ payload: { data } }) {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryUpdateStudentUser(data));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Mise à jour échouée',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* uploadStudentCV({ payload: { data } }) {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryUploadStudentCV(data));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            if (status === 401) {
                yield put({
                    type: USER_FAILURE,
                    error: {
                        status: 401,
                        message: 'Une erreur d\'authentification est survenue.',
                    },
                });
                return;
            }

            yield put({
                type: USER_FAILURE,
                error: {
                    status: 404,
                    message: 'Impossible d\'envoyer le CV pour le moment.',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

const getUserSchools = state => path(['entities', 'user', 'schools'], state);

export function* fetchCompanyUserSchools() {
    try {
        const isAlreadyInStore = yield select(getUserSchools);
        if (isAlreadyInStore) {
            yield put({
                type: USER_SUCCESS,
                hasRetrievedOnly: true,
            });
            return;
        }

        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetCompanySchools());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Impossible de récuperer vos données pour le moment.',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error: {
                message: 'Impossible de récuperer vos données pour le moment.',
            },
        });
    }
}
