import { takeEvery } from 'redux-saga/effects';

import {
    ADMIN_USER_REQUESTED,
    COMPANY_USER_REQUESTED,
    COMPANY_USER_UPDATE,
    STUDENT_USER_REQUESTED,
    STUDENT_USER_UPDATE,
    STUDENT_USER_UPDATE_CV,
    STUDENT_SLOTS_REQUESTED,
    COMPANY_SLOTS_REQUESTED,
    COMPANY_UPDATE_SLOT_REQUESTED,
    COMPANY_ENABLE_SLOTS_REQUESTED,
    COMPANY_SCHOOLS_REQUESTED,
} from '../actions';
import {
    fetchAdminUser,
    fetchCompanyUser,
    fetchCompanyUserUpdate,
    fetchStudentUser,
    uploadStudentCV,
    updateStudentUser,
    fetchCompanyUserSchools,
} from './sagas';
import { enableCompanySlots, fetchCompanySlots, fetchStudentSlots, updateCompanySlot } from './slots';

export default function* fetchCompanySaga() {
    yield takeEvery(ADMIN_USER_REQUESTED, fetchAdminUser);
    yield takeEvery(COMPANY_USER_REQUESTED, fetchCompanyUser);
    yield takeEvery(COMPANY_USER_UPDATE, fetchCompanyUserUpdate);
    yield takeEvery(STUDENT_USER_REQUESTED, fetchStudentUser);
    yield takeEvery(STUDENT_USER_UPDATE_CV, uploadStudentCV);
    yield takeEvery(STUDENT_USER_UPDATE, updateStudentUser);
    yield takeEvery(COMPANY_SLOTS_REQUESTED, fetchCompanySlots);
    yield takeEvery(STUDENT_SLOTS_REQUESTED, fetchStudentSlots);
    yield takeEvery(COMPANY_UPDATE_SLOT_REQUESTED, updateCompanySlot);
    yield takeEvery(COMPANY_ENABLE_SLOTS_REQUESTED, enableCompanySlots);
    yield takeEvery(COMPANY_SCHOOLS_REQUESTED, fetchCompanyUserSchools);
}
