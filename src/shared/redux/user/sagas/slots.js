import { call, put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';

import { getAPIQueryConfig } from '../../queries';
import { queryEnableCompanySlots, queryGetCompanySlots, queryGetStudentSlots, queryUpdateCompanySlot } from '../queries';

import { USER_FAILURE, USER_SUCCESS } from '../actions';
import { isNilOrEmpty } from '../../../helpers/utils';

export function* fetchStudentSlots() {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetStudentSlots());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Accès interdit au compte utilisateur',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* fetchCompanySlots() {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetCompanySlots());
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Accès interdit au compte utilisateur',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: true,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error,
        });
    }
}

export function* updateCompanySlot({ payload: { id, slot } }) {
    if (!id) {
        yield put({ type: USER_FAILURE, error: { status: 400, message: 'Impossible de modifier le créneau.' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryUpdateCompanySlot(id, slot));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Accès interdit au compte utilisateur',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error: {
                status: 500,
                message: 'Impossible de modifier le créneau.',
            },
        });
    }
}

export function* enableCompanySlots({ payload: { slots } }) {
    if (isNilOrEmpty(slots)) {
        yield put({ type: USER_FAILURE, error: { status: 400, message: 'Impossible d\'activer ces créneaux.' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryEnableCompanySlots(slots));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));

        if (status < 200 || status >= 300) {
            yield put({
                type: USER_FAILURE,
                error: {
                    message: 'Accès interdit au compte utilisateur',
                },
            });
            return;
        }

        yield put({
            type: USER_SUCCESS,
            hasRetrievedOnly: false,
        });
    } catch (error) {
        yield put({
            type: USER_FAILURE,
            error: {
                status: 500,
                message: 'Impossible d\'activer ces créneaux.',
            },
        });
    }
}
