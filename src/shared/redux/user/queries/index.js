import { pathOr, map } from 'ramda';

export const queryGetAdminUser = () => ({
    url: '/admins/user/',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { email, securityLevel, firstName, lastName, organizationName, schools, userID, registrationCodeForCompanies } =
            pathOr({}, ['user'], response);

        return {
            user: {
                id: userID,
                email,
                firstName,
                lastName,
                organizationName,
                schools,
                securityLevel,
                registrationCodeForCompanies,
            },
        };
    },
});

export const queryGetCompanyUser = () => ({
    url: '/companies/user/',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response : ?Object) => {
        const company = pathOr({}, ['company'], response);

        return {
            user: {
                ...company,
            },
        };
    },
});

export const queryGetCompanySchools = () => ({
    url: '/companies/user/schools/',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response : ?Object) => {
        const schools = pathOr([], ['schools'], response);

        return {
            user: {
                schools,
            },
        };
    },
});

export const queryGetCompanySlots = () => ({
    url: '/companies/slot/',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response : ?Object) => {
        const slots = pathOr([], ['slots'], response);

        return {
            user: {
                slots,
            },
        };
    },
});

export const queryUpdateCompanyUser = data => ({
    url: '/companies/user/update',
    body: {
        ...data,
    },
    method: 'PUT',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response : ?Object) => {
        const user = pathOr({}, ['user'], response);

        return {
            user,
        };
    },
});

export const queryGetStudentUser = () => ({
    url: '/students/user/',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response : ?Object) => {
        const student = pathOr({}, ['student'], response);

        return {
            user: {
                ...student,
            },
        };
    },
});

export const queryUpdateStudentUser = data => ({
    url: '/students/user/',
    body: {
        ...data,
    },
    method: 'PUT',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response : ?Object) => {
        const user = pathOr({}, ['student'], response);

        return {
            user,
        };
    },
});

export const queryUploadStudentCV = data => ({
    url: '/students/user/upload/cv',
    body: data,
    method: 'POST',
    isSendingFile: true,
});

export const queryGetStudentSlots = () => ({
    url: '/students/slot/',
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response : ?Object) => {
        const slots = pathOr({}, ['slots'], response);

        return {
            user: {
                slots,
            },
        };
    },
});

export const queryUpdateCompanySlot = (id, slot) => ({
    url: `/companies/slot/${id}`,
    method: 'PUT',
    body: {
        ...slot,
    },
    update: {
        user: prev => ({
            ...prev,
            slots: map(s => (s._id === id ? ({ ...s, ...slot }) : s), prev.slots),
        }),
    },
});

export const queryEnableCompanySlots = slotsIds => ({
    url: '/companies/slot/enable',
    method: 'PUT',
    body: {
        slots: slotsIds,
    },
    update: {
        user: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { slots } = response;

        return ({
            user: {
                slots,
            },
        });
    },
});
