export const ADMIN_USER_REQUESTED = 'ADMIN_USER_REQUESTED';

export const COMPANY_USER_REQUESTED = 'COMPANY_USER_REQUESTED';
export const COMPANY_USER_UPDATE = 'COMPANY_USER_UPDATE';
export const COMPANY_SLOTS_REQUESTED = 'COMPANY_SLOTS_REQUESTED';
export const COMPANY_UPDATE_SLOT_REQUESTED = 'COMPANY_UPDATE_SLOT_REQUESTED';
export const COMPANY_ENABLE_SLOTS_REQUESTED = 'COMPANY_ENABLE_SLOTS_REQUESTED';
export const COMPANY_SCHOOLS_REQUESTED = 'COMPANY_SCHOOLS_REQUESTED';

export const STUDENT_USER_REQUESTED = 'STUDENT_USER_REQUESTED';
export const STUDENT_USER_UPDATE = 'STUDENT_USER_UPDATE';
export const STUDENT_USER_UPDATE_CV = 'STUDENT_USER_UPDATE_CV';
export const STUDENT_SLOTS_REQUESTED = 'STUDENT_SLOTS_REQUESTED';

export const USER_SUCCESS = 'USER_SUCCESS';
export const USER_FAILURE = 'USER_FAILURE';

export const fetchAdminUser = () => ({
    type: ADMIN_USER_REQUESTED,
});

export const fetchCompanyUser = () => ({
    type: COMPANY_USER_REQUESTED,
});

export const fetchCompanyUserUpdate = data => ({
    type: COMPANY_USER_UPDATE,
    payload: {
        data,
    },
});

export const fetchCompanySlots = () => ({
    type: COMPANY_SLOTS_REQUESTED,
});

export const fetchCompanySchools = () => ({
    type: COMPANY_SCHOOLS_REQUESTED,
});

export const fetchStudentUser = () => ({
    type: STUDENT_USER_REQUESTED,
});

export const updateStudentUser = data => ({
    type: STUDENT_USER_UPDATE,
    payload: {
        data,
    },
});

export const uploadStudentCV = data => ({
    type: STUDENT_USER_UPDATE_CV,
    payload: {
        data,
    },
});

export const fetchStudentSlots = () => ({
    type: STUDENT_SLOTS_REQUESTED,
});

export const updateCompanySlotByID = (id, slot) => ({
    type: COMPANY_UPDATE_SLOT_REQUESTED,
    payload: {
        id,
        slot,
    },
});

export const enableCompanySlots = slots => ({
    type: COMPANY_ENABLE_SLOTS_REQUESTED,
    payload: {
        slots,
    },
});
