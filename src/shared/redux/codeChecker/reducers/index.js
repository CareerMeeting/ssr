import { CODE_CHECK_REQUESTED, CODE_CHECK_SUCCEEDED, CODE_CHECK_FAILED } from '../actions';
import { failure, success } from '../../resultStates';

const initialState = [];

const CodeCheckerReducers = (previousState = initialState, { type, error }) => {
    switch (type) {
        case CODE_CHECK_REQUESTED:
            return { ...previousState, isLoading: true, error: null, result: null };
        case CODE_CHECK_FAILED:
            return { ...previousState, isLoading: false, error, result: failure };
        case CODE_CHECK_SUCCEEDED:
            return { ...previousState, isLoading: false, error: null, result: success };
        default:
            return previousState;
    }
};

export default CodeCheckerReducers;
