
export const queryCodeChecker = registrationCode => ({
    url: '/companies/sign/code/check',
    method: 'POST',
    body: {
        registrationCode,
    },
    update: {
        codeChecker: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { code } = response;

        return {
            codeChecker: {
                code,
            },
        };
    },
});
