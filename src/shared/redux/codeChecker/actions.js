export const CODE_CHECK_REQUESTED = 'CODE_CHECK_REQUESTED';
export const CODE_CHECK_SUCCEEDED = 'CODE_CHECK_SUCCEEDED';
export const CODE_CHECK_FAILED = 'CODE_CHECK_FAILED';

export const fetchCodeChecker = code => ({
    type: CODE_CHECK_REQUESTED,
    payload: {
        code,
    },
});
