import { takeEvery } from 'redux-saga/effects';

import { CODE_CHECK_REQUESTED } from '../actions';
import { fetchCodeChecker } from './sagas';

export default function* fetchCodeCheckerSaga() {
    yield takeEvery(CODE_CHECK_REQUESTED, fetchCodeChecker);
}
