import { call, put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';

import { getAPIQueryConfig } from '../../queries';
import { queryCodeChecker } from '../queries';
import { CODE_CHECK_SUCCEEDED, CODE_CHECK_FAILED } from '../actions';

export function* fetchCodeChecker({ payload: { code } }) {
    if (!code) {
        yield put({ type: CODE_CHECK_FAILED, error: { status: 400, message: 'Code manquant' } });
        return;
    }

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryCodeChecker(code));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: CODE_CHECK_FAILED,
                error: {
                    status,
                    message: 'Code erroné',
                },
            });
            return;
        }

        yield put({
            type: CODE_CHECK_SUCCEEDED,
        });
    } catch (error) {
        yield put({
            type: CODE_CHECK_FAILED,
            error,
        });
    }
}
