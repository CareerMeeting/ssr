import { takeEvery } from 'redux-saga/effects';

import { CM_TOKEN_WITH_MICROSOFT_ONLINE_AUTH_REQUESTED, MICROSOFT_ONLINE_AUTH_REQUESTED } from '../actions';
import { fetchCMWithMOAuth, fetchMOAuth } from './sagas';

export default function* fetchMicrosoftOnlineSaga() {
    yield takeEvery(MICROSOFT_ONLINE_AUTH_REQUESTED, fetchMOAuth);
    yield takeEvery(CM_TOKEN_WITH_MICROSOFT_ONLINE_AUTH_REQUESTED, fetchCMWithMOAuth);
}
