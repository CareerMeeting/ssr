import { call, put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';

import { SESSION_FAILED, SESSION_SUCCEEDED } from '../../session/actions';
import { getAPIQueryConfig } from '../../queries';
import { queryGetCMToken, queryGetMicrosoftOnlineAuthUrl } from '../queries';

export function* fetchMOAuth({ payload: { userType } }) {
    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetMicrosoftOnlineAuthUrl(userType));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: SESSION_FAILED,
                error: {
                    status: 401,
                    message: 'Une erreur avec MicrosoftOnline est survenue.',
                },
            });
            return;
        }

        yield put({
            type: SESSION_SUCCEEDED,
        });
    } catch (error) {
        yield put({
            type: SESSION_FAILED,
            error: {
                status: 401,
                message: 'Une erreur avec MicrosoftOnline est survenue.',
            },
        });
    }
}

export function* fetchCMWithMOAuth({ payload: { code, userType } }) {
    if (!code) return;

    try {
        const apiQueryConfig = yield call(getAPIQueryConfig, queryGetCMToken(code, userType));
        const { status } = yield put.resolve(mutateAsync(apiQueryConfig));
        if (status < 200 || status >= 300) {
            yield put({
                type: SESSION_FAILED,
                error: {
                    status: 401,
                    message: 'Une erreur d\'authentification est survenue.',
                },
            });
            return;
        }

        yield put({
            type: SESSION_SUCCEEDED,
        });
    } catch (error) {
        yield put({
            type: SESSION_FAILED,
            error: {
                status: 401,
                message: 'Une erreur d\'authentification est survenue.',
            },
        });
    }
}
