import { storage } from '../../../helpers/window';
import { tokenKey, userTypeKey } from '../../../helpers/session/constants';

export const queryGetCMToken = (code, userTypeRessource) => ({
    url: `/${userTypeRessource}s/sign/microsoft-online/authorization`,
    body: {
        code,
    },
    update: {
        session: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { token, userType } = response;
        const storageLocal = storage();
        if (storageLocal) {
            storage().setItem(tokenKey, token);
            storage().setItem(userTypeKey, userType);
        }

        return {
            session: {
                token,
                userType: userType >= 0 && userType.toString(),
            },
        };
    },
});

export const queryGetMicrosoftOnlineAuthUrl = userType => ({
    url: `/${userType}s/sign/microsoft-online/token`,
    update: {
        session: (prev, next) => ({
            ...prev,
            ...next,
        }),
    },
    transform: (response: ?Object) => {
        const { microsoftOnlineAuthUrl } = response;

        return {
            session: {
                microsoftOnlineAuthUrl,
            },
        };
    },
});
