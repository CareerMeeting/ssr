// @flow
import qs from 'qs';
import { pathOr } from 'ramda';
import { Redirect } from 'react-router-dom';
import { withProps } from 'recompose';

import type { FunctionnalComponent } from '../../app/types';

export const getQuery = (location: Object): Object => qs.parse(pathOr('""', ['search'], location), { ignoreQueryPrefix: true });

export const navigateTo = (url : string): FunctionnalComponent => withProps({ to: url })(Redirect);
