import { isEmpty, isNil, keys, map } from 'ramda';

export const isNilOrEmpty = val => isNil(val) || isEmpty(val);

export const convertHashmapToArray = (hashmap = {}) => (isNilOrEmpty(hashmap) ? [] : map(key => hashmap[key], keys(hashmap)));
