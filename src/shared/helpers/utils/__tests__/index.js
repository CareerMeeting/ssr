import React from 'react';
import renderer from 'react-test-renderer';

import { isNilOrEmpty } from '../';

it('with an empty array', () => {
    const tree = isNilOrEmpty([]);

    expect(tree).toMatchSnapshot();
});

it('with null', () => {
    const tree = isNilOrEmpty(null);

    expect(tree).toMatchSnapshot();
});

it('with undefined', () => {
    const tree = isNilOrEmpty(undefined);

    expect(tree).toMatchSnapshot();
});

it('with a filled array', () => {
    const tree = isNilOrEmpty([2]);

    expect(tree).toMatchSnapshot();
});
