export const storage = () => {
    if (typeof window !== 'undefined' && window.localStorage) return window.localStorage;
    else if (typeof localStorage !== 'undefined') return localStorage;
    return null;
};
