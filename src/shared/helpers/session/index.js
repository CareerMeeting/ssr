import { storage } from '../window';
import { adminType, companyType, studentType, superAdminType, tokenKey, userTypeKey } from './constants';

export const getStorageLocalToken = () => {
    const storageLocal = storage();
    if (!storageLocal) return null;

    return storageLocal.getItem(tokenKey);
};

export const getUserType = () => {
    const storageLocal = storage();
    if (!storageLocal) return null;

    return storageLocal.getItem(userTypeKey);
};

export const getIsAuthenticated = () => {
    const token = getStorageLocalToken();

    return !!token;
};

export const getIsAuthenticatedAsSuperAdmin = () => {
    const token = getStorageLocalToken();
    const userType = getUserType();

    return !!token && userType === superAdminType;
};

export const getIsAuthenticatedAsAdmin = () => {
    const token = getStorageLocalToken();
    const userType = getUserType();

    return !!token && userType === adminType;
};

export const getIsAuthenticatedAsAdminOrSuperAdmin = () => {
    const token = getStorageLocalToken();
    const userType = getUserType();

    return !!token && (userType === adminType || userType === superAdminType);
};

export const getIsAuthenticatedAsCompany = () => {
    const token = getStorageLocalToken();
    const userType = getUserType();

    return !!token && userType === companyType;
};

export const getIsAuthenticatedAsStudent = () => {
    const token = getStorageLocalToken();
    const userType = getUserType();

    return !!token && userType === studentType;
};

export const isStudentUser = userType => userType === studentType;
export const isAdminUser = userType => userType === superAdminType || userType === adminType;
export const isCompanyUser = userType => userType === companyType;
export const isAdminOrStudentUser = userType => isAdminUser(userType) || isStudentUser(userType);
