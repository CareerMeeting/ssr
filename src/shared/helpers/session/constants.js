export const tokenKey = 'AUTH_ACCESS_TOKEN';
export const userTypeKey = 'USER_TYPE';

export const superAdminType = '1';
export const adminType = '2';
export const studentType = '3';
export const companyType = '0';
