import React from 'react';

import AdminMenu from '../../components/AdminMenu';
import AppContent from './styles';

const withAdminMenu = WrappedComponent => props => (
    <AppContent>
        <AdminMenu />
        <WrappedComponent {...props} />
    </AppContent>
);

export default withAdminMenu;
