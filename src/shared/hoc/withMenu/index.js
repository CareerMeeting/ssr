import React from 'react';
import { connect } from 'react-redux';
import { branch, compose, withHandlers, withState } from 'recompose';

import AdminMenu from '../../components/AdminMenu';
import AppContent from './styles';
import CompanyMenu from '../../components/CompanyMenu';
import StudentMenu from '../../components/StudentMenu';
import { adminType, companyType, studentType, superAdminType } from '../../helpers/session/constants';
import { getUserType, getIsAuthenticatedAsAdminOrSuperAdmin, getIsAuthenticatedAsStudent, getIsAuthenticatedAsCompany } from '../../helpers/session';
import { logOutSession } from '../../redux/session/actions';
import { navigateTo } from '../../helpers/navigation';

const getMenuByUserType = ({ handleLogOutSession }) => {
    const userType = getUserType();

    switch (userType) {
        case superAdminType:
        case adminType:
            return getIsAuthenticatedAsAdminOrSuperAdmin() && <AdminMenu logOutSession={handleLogOutSession} />;
        case studentType:
            return getIsAuthenticatedAsStudent() && <StudentMenu logOutSession={handleLogOutSession} />;
        case companyType:
            return getIsAuthenticatedAsCompany() && <CompanyMenu logOutSession={handleLogOutSession} />;
        default:
            return null;
    }
};

const withMenu = WrappedComponent => ({ handleLogOutSession, ...others }) => (
    <AppContent>
        {getMenuByUserType(({ handleLogOutSession }))}
        <WrappedComponent {...others} />
    </AppContent>
);

const handlers = {
    handleLogOutSession: ({ logOutSessionAction, setHasLogOut }) => () => {
        logOutSessionAction();
        setHasLogOut(true);
    },
};

const connectToStore = connect(null, {
    logOutSessionAction: logOutSession,
});

const hoc = compose(
    connectToStore,
    withState('hasLogOut', 'setHasLogOut', false),
    withHandlers(handlers),
    branch(
        ({ hasLogOut }: { hasLogOut: boolean }): boolean => hasLogOut,
        () => ({ setHasLogOut }) => {
            setHasLogOut(false);
            return navigateTo('/')();
        },
    ),
    withMenu,
);

export default hoc;
