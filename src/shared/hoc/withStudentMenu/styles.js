import styled from 'react-emotion';

const AppContent = styled('div')({
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
});

export default AppContent;
