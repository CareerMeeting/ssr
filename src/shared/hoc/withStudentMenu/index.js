import React from 'react';

import StudentMenu from '../../components/StudentMenu';
import AppContent from './styles';

const withCompanyMenu = WrappedComponent => props => (
    <AppContent>
        <StudentMenu />
        <WrappedComponent {...props} />
    </AppContent>
);

export default withCompanyMenu;
