import React from 'react';
import { withStyles } from '@material-ui/core';
import { compose } from 'recompose';
import Slide from '@material-ui/core/Slide/Slide';

import { Background, CardBody, CardFull, styles } from './styles';

const withBackgroundCard = WrappedComponent => ({ classes, ...others }) => (
    <Background>
        <Slide direction="right" in mountOnEnter unmountOnExit timeout={400}>
            <CardFull className={classes.cardFull} >
                <CardBody>
                    <WrappedComponent {...others} />
                </CardBody>
            </CardFull>
        </Slide>
    </Background>
);

export default compose(
    withStyles(styles),
    withBackgroundCard,
);

