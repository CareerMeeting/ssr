import styled from 'react-emotion';

import { greenLight, white } from '../../app/colors';

export const CardBody = styled('div')({
    overflowY: 'auto',
    overflowX: 'hidden',
    backgroundColor: white,
    height: '100%',
    padding: 24,
    paddingTop: 16,
    borderRadius: 4,
    boxShadow: '0px 1px 3px 0px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12)',
});

export const CardFull = styled('div')({
    height: '86%',
    paddingBottom: 14,
    paddingTop: 30,
});

export const Background = styled('div')({
    backgroundColor: greenLight,
    height: '100%',
});

export const styles = theme => ({
    cardFull: {
        paddingLeft: 100,
        paddingRight: 100,
        [theme.breakpoints.down('md')]: {
            paddingRight: 0,
            paddingLeft: 0,
        },
    },
});
