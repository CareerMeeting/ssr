import React from 'react';

import CompanyMenu from '../../components/CompanyMenu';
import AppContent from './styles';

const withCompanyMenu = WrappedComponent => props => (
    <AppContent>
        <CompanyMenu />
        <WrappedComponent {...props} />
    </AppContent>
);

export default withCompanyMenu;
