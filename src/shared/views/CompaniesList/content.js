// @flow
import Helmet from 'react-helmet';
import React from 'react';
import Typography from '@material-ui/core/Typography/Typography';

import Snackbar from '../../components/SnackBar';
import type { FunctionnalComponent } from '../../app/types';
import { Cover, Body } from './styles';
import { errorType } from '../../components/SnackBar/variants';
import CompaniesListTable from '../../components/CompaniesListTable';
import Select from '../../components/Select';
import { superAdminType } from '../../helpers/session/constants';
import { TitleAndInfoBody } from '../StudentPlanning/styles';
import EventDateInfo from '../../components/EventDateInfo';
import SubscribeSlotsMaxDateWarning from '../../components/SubscribeSlotsMaxDateWarning';

const View = (
    {
        companiesToView,
        schoolSelected,
        schoolsFromWebConfig,
        handleChangeSchoolToView,
        userType,
        error,
    }
: {
        companiesToView: Array,
        schoolsFromWebConfig: Array,
        schoolSelected: String,
        handleChangeSchoolToView: Function,
        error: Object,
}): FunctionnalComponent => (
    <Cover>
        <Helmet title="Liste des entreprises" />
        {error && <Snackbar isOpen variant={errorType} message={error.message} />}

        <TitleAndInfoBody>
            <Typography gutterBottom variant="h4">Liste des entreprises</Typography>
            <EventDateInfo />
            <SubscribeSlotsMaxDateWarning />
        </TitleAndInfoBody>
        <Body>
            {userType === superAdminType && (
                <Select source={schoolsFromWebConfig} destination={schoolSelected} handleChange={handleChangeSchoolToView} />
            )}
            <CompaniesListTable data={companiesToView} />
        </Body>
    </Cover>
);

export default View;
