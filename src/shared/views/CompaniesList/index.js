import { branch, compose, lifecycle, renderComponent, withHandlers, withPropsOnChange, withState } from 'recompose';
import { connect } from 'react-redux';
import { contains, filter, path, pathOr } from 'ramda';

import Content from './content';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchCompanies } from '../../redux/company/actions';
import LargeLoader from '../../components/LargeLoader';
import { convertHashmapToArray } from '../../helpers/utils';
import { superAdminType } from '../../helpers/session/constants';

const handlers = {
    handleChangeSchoolToView: ({ setSchoolSelected }) => (event) => {
        setSchoolSelected(path(['target', 'value'], event));
    },
};

const lifecyclePlayload = {
    componentDidMount() {
        this.props.fetchCompanies();
    },
};

const mapStateToProps = ({ entities, companies }: { entities: EntitiesStructure, companies: StateElementStructure }) => ({
    companies: convertHashmapToArray(path(['companies'], entities)),
    userType: path(['session', 'userType'], entities),
    error: path(['error'], companies),
    isLoading: path(['isLoading'], companies),
    schoolsFromWebConfig: pathOr([], ['webConfig', 'schools'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    {
        fetchCompanies,
    },
);

const changeCompanies = ({ userType, schoolSelected, setCompaniesToView, companies }) => {
    if (userType !== superAdminType) {
        setCompaniesToView(companies);
        return;
    }

    setCompaniesToView(filter(company => contains(schoolSelected, pathOr([], ['schools'], company)), companies));
};

const container = compose(
    connectToStore,
    lifecycle(lifecyclePlayload),
    withState('schoolSelected', 'setSchoolSelected', ''),
    withState('companiesToView', 'setCompaniesToView', ({ companies }) => companies),
    withHandlers(handlers),
    withPropsOnChange(['schoolSelected', 'companies'], changeCompanies),
    withMenu,
    withBackgroundCard,
    branch(({ isLoading }) => isLoading, renderComponent(LargeLoader)),
)(Content);

export default container;
