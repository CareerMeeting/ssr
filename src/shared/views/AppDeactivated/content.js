/* eslint-disable react/prop-types */
// @flow
import React from 'react';
import Helmet from 'react-helmet';
import classNames from 'classnames';

import type { FunctionnalComponent } from '../../app/types';
import { Cover, SpaceCard, GrowStyled } from './styles';
import { ParticlesStyled } from '../Home/styles';
import particlesParams from '../../assets/particlesConfig/particles-home-background.json';

const View = ({ deactivatedText, classes, className } : { deactivatedText: string, classes: any, className: any }): FunctionnalComponent => (
    <Cover>
        <Helmet title="Career Meeting IONIS | À Bientôt" />
        <GrowStyled in className={classNames(classes.spaceCard, className)}>
            <SpaceCard raised>
                {deactivatedText}
            </SpaceCard>
        </GrowStyled>
        <ParticlesStyled params={particlesParams} canvasClassName={classes.canvasParticles} />
    </Cover>
);

export default View;
