import withStyles from '@material-ui/core/styles/withStyles';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { pathOr } from 'ramda';

import Content from './content';
import type { EntitiesStructure } from '../../redux/types';
import { styles } from './styles';

const mapStateToProps = ({ entities } : { entities: EntitiesStructure }) => ({
    deactivatedText: pathOr('Le site est désactivé pour le moment, à bientôt 👋', ['webConfig', 'deactivatedText'], entities),
});

const connectToStore = connect(mapStateToProps);

const container = compose(
    connectToStore,
    withStyles(styles),
);

export default container(Content);
