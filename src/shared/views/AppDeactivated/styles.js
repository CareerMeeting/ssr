import Card from '@material-ui/core/Card';
import styled from 'react-emotion';
import Grow from '@material-ui/core/Grow/Grow';

export const Cover = styled('div')({
    alignItems: 'center',
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
});

export const GrowStyled = styled(Grow)({
    position: 'absolute',
    display: 'grid',
});

export const SpaceCard = styled(Card)({
    padding: '1%',
    display: 'inherit',
});

export const styles = () => ({
    spaceCard: {
        overflow: 'visible',
    },
});
