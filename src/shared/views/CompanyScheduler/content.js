import React from 'react';
import { Helmet } from 'react-helmet';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import Tabs from '@material-ui/core/Tabs/Tabs';
import Tab from '@material-ui/core/Tab/Tab';

import { Cover, AppBarStyled } from './styles';
import EditScheduleCompany from '../../components/EditScheduleCompany';
import SimpleScheduleCompany from '../../components/SimpleScheduleCompany';
import { errorType } from '../../components/SnackBar/variants';
import Snackbar from '../../components/SnackBar';
import { TitleAndInfoBody } from '../StudentPlanning/styles';
import EventDateInfo from '../../components/EventDateInfo';
import SubscribeSlotsMaxDateWarning from '../../components/SubscribeSlotsMaxDateWarning';
import ScheduleSlotsMaxDateWarning from '../../components/ScheduleSlotsMaxDateWarning';

const View = (
    {
        currentTab,
        error,
        handleChangeCurrentTab,
        isAuthenticatedAsCompanyUser,
        isInEditTab,
        isInViewTab,
        queryID,
    }
: {
        currentTab: boolean,
        error: Object,
        handleChangeCurrentTab: Function,
        isAuthenticatedAsCompanyUser: boolean,
        isInEditTab: boolean,
        isInViewTab: boolean,
        queryID: String,
}) => (
    <Cover>
        <Helmet title="Planning" />
        { error && <Snackbar isOpen variant={errorType} message={error.message} /> }
        {isAuthenticatedAsCompanyUser && (
            <AppBarStyled position="static" color="default">
                <Tabs
                    value={currentTab}
                    onChange={handleChangeCurrentTab}
                    fullWidth
                    indicatorColor="secondary"
                    textColor="secondary"
                >
                    <Tab icon={<EditIcon />} label="Editer Mon Planning" />
                    <Tab icon={<VisibilityIcon />} label="Voir Mon Planning" />
                </Tabs>
            </AppBarStyled>
        )}
        <TitleAndInfoBody>
            <EventDateInfo />
            <SubscribeSlotsMaxDateWarning />
            <ScheduleSlotsMaxDateWarning />
        </TitleAndInfoBody>
        {isAuthenticatedAsCompanyUser && isInEditTab() && <EditScheduleCompany />}
        {isInViewTab() && <SimpleScheduleCompany queryID={queryID} />}
    </Cover>
);

export default View;
