import Button from '@material-ui/core/Button';
import styled from 'react-emotion';
import TextField from '@material-ui/core/TextField/TextField';
import AppBar from '@material-ui/core/AppBar/AppBar';

export const AppBarStyled = styled(AppBar)({
    marginBottom: 20,
});

export const Cover = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'start-flex',
});

export const Field = styled(TextField)({
    minWidth: '47% !important',
    maxWidth: '97%',
    margin: '2% 1% !important',
});

export const ButtonLogin = styled(Button)({
    width: '46%',
    margin: '4% 2%',
});

export const styles = theme => ({
    profilePicture: {
        backgroundColor: theme.palette.secondary.main,
        width: 80,
        height: 80,
    },
    icon: {
        position: 'relative',
        top: 6,
        color: theme.palette.secondary.main,
    },
    container: {
        margin: '3%',
        overflowX: 'auto',
    },
    hr: {
        width: '50%',
        margin: '5% 0%',
    },
    multiSelect: {
        width: '96%',
        maxWidth: '97%',
        margin: '2% 1%',
    },
    description: {
        margin: '4% 0%',
    },
    iconButton: {
        marginLeft: '5%',
    },
    switchButton: {
        position: 'relative',
        right: '48%',
    },
    switchIcon: {
        width: '1.5em',
        height: '1.5em',
    },
    inputLabel: {
        color: theme.palette.secondary.main,
    },
});
