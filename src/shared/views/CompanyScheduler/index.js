import withStyles from '@material-ui/core/styles/withStyles';
import { compose, withState, withHandlers, lifecycle } from 'recompose';
import { path, pathOr } from 'ramda';
import { connect } from 'react-redux';
import moment from 'moment';

import Content from './content';
import { styles } from './styles';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchCompanySlots } from '../../redux/user/actions';
import { fetchCompanySlotsByID, getCompany } from '../../redux/company/actions';
import { getQuery } from '../../helpers/navigation';
import { isCompanyUser } from '../../helpers/session';

const mapStateToProps = ({ entities, user, companies }, ownerProps) => {
    const { id, mode } = getQuery(path(['location'], ownerProps));
    const userType = path(['session', 'userType'], entities);

    return ({
        mode,
        queryID: id,
        schools: path(['webConfig', 'schools'], entities),
        user: path(['companies', id], entities) || path(['user'], entities),
        userType,
        isAuthenticatedAsCompanyUser: isCompanyUser(userType),
        isLoading: path(['isLoading'], user),
        error: path(['error'], user) || path(['error'], companies),
    });
};

const lifecyclePayload = {
    componentDidMount() {
        if (this.props.mode === 'edit') {
            this.props.setCurrentTab(0);
        }
        const { queryID } = this.props;
        if (queryID) {
            this.props.getCompany(queryID);
            this.props.fetchCompanySlotsByID(queryID);
            return;
        }

        this.props.fetchCompanySlots();
    },
};

const handlers = {
    handleChangeCurrentTab: ({ setCurrentTab }) => (event, value) => {
        console.log(value);
        setCurrentTab(value);
    },
    isInEditTab: ({ currentTab }) => () => currentTab === 0,
    isInViewTab: ({ currentTab }) => () => currentTab === 1,
};

const container = compose(
    withState('currentTab', 'setCurrentTab', 1),
    connect(
        mapStateToProps,
        {
            fetchCompanySlotsByID,
            fetchCompanySlots,
            getCompany,
        },
    ),
    lifecycle(lifecyclePayload),
    withHandlers(handlers),
    withMenu,
    withBackgroundCard,
    withStyles(styles),
);

export default container(Content);
