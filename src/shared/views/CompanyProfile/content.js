import React from 'react';
import { Helmet } from 'react-helmet';
import Grid from '@material-ui/core/Grid/Grid';
import Avatar from '@material-ui/core/Avatar';
import classNames from 'classnames';
import LocationIcon from '@material-ui/icons/LocationOnOutlined';
import CheckCircleIcon from '@material-ui/icons/DoneOutline';
import ToggleOffIcon from '@material-ui/icons/ToggleOffOutlined';
import ToggleOnIcon from '@material-ui/icons/ToggleOnOutlined';
import ClearIcon from '@material-ui/icons/CloseOutlined';
import EmailIcon from '@material-ui/icons/MailOutline';
import PhoneIcon from '@material-ui/icons/PhoneOutlined';
import ReceiptIcon from '@material-ui/icons/ReceiptOutlined';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import FormControl from '@material-ui/core/FormControl/FormControl';
import InputLabel from '@material-ui/core/InputLabel/InputLabel';
import Select from '@material-ui/core/Select/Select';
import { pathOr } from 'ramda';

import { Cover, Field, ButtonLogin } from './styles';
import TraineeshipOffersTable from '../../components/traineeshipOffersListTable';
import { companyType, superAdminType } from '../../helpers/session/constants';
import { errorType } from '../../components/SnackBar/variants';
import Snackbar from '../../components/SnackBar';
import { NavLink } from 'react-router-dom';
import { ButtonValidate, IconCheck } from '../SuperAdminSettings/styles';
import Dialog from '../../components/Dialog';

const ProfileFormDataView = (
    {
        classes,
        className,
        schools,
        user,
        form,
        handleFormChange,
        updateUser,
        toggleUpdate,
    }
        : {
        classes: Object,
        className: Object,
        toggleUpdate: Function,
        handleFormChange: Function,
        schools: Array,
        user: Object,
        form: Object,
        updateUser: Function,
    }) =>
    (
        <Grid item container justify="center" direction="row" xs={12} sm={4}>
            <Grid item xs={12} >
                <Field
                    defaultValue={user.companyName}
                    name="companyName"
                    variant="outlined"
                    onChange={handleFormChange}
                    InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                    label="Nom de l'entreprise"
                />
                <Field
                    defaultValue={user.email}
                    variant="outlined"
                    name="email"
                    type="email"
                    label="Email"
                    InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                    onChange={handleFormChange}
                />
            </Grid>
            <Grid item xs={12} >
                <Field
                    defaultValue={user.city}
                    variant="outlined"
                    name="city"
                    label="Ville"
                    InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                    onChange={handleFormChange}
                />
                <Field
                    defaultValue={user.codeZip}
                    variant="outlined"
                    name="codeZip"
                    label="code postal"
                    InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                    onChange={handleFormChange}
                />
            </Grid>
            <Grid item xs={12} >
                <Field
                    defaultValue={user.country}
                    variant="outlined"
                    name="country"
                    label="Pays"
                    InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                    onChange={handleFormChange}
                />
                <Field
                    defaultValue={user.phoneNumber}
                    variant="outlined"
                    name="phoneNumber"
                    label="Numéro de téléphone"
                    InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                    onChange={handleFormChange}
                />
            </Grid>
            <Grid item xs={12}>
                <Field
                    fullWidth
                    defaultValue={user.address}
                    variant="outlined"
                    name="address"
                    label="Adresse"
                    InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                    onChange={handleFormChange}
                />
            </Grid>
            <Grid item xs={12}>
                <FormControl
                    className={classNames(classes.multiSelect, className)}
                >
                    <InputLabel className={classNames(classes.inputLabel, className)}>École(s)</InputLabel>
                    <Select
                        name="schools"
                        multiple
                        value={form.schools}
                        onChange={handleFormChange}
                        variant="outlined"
                        renderValue={selected => selected.join(', ')}
                    >
                        {schools.map(school => (
                            <MenuItem key={school} value={school}>
                                <Checkbox checked={form.schools.indexOf(school) > -1} />
                                <ListItemText primary={school} />
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={12} className={classNames(classes.description, className)} >
                <Field
                    defaultValue={user.companyDescription}
                    name="companyDescription"
                    label="Description de l'entreprise"
                    multiline
                    rows="6"
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{ classes: { root: classNames(classes.inputLabel, className) } }}
                    onChange={handleFormChange}
                />
                <ButtonLogin variant="outlined" color="primary" onClick={toggleUpdate} >
                    Annuler <ClearIcon className={classNames(classes.iconButton, className)} />
                </ButtonLogin>
                <ButtonLogin variant="outlined" color="secondary" onClick={() => updateUser(form)} >
                    Valider <CheckCircleIcon className={classNames(classes.iconButton, className)} />
                </ButtonLogin>
            </Grid>
        </Grid>
    );

const ProfileDataView = (
    {
        classes,
        className,
        user,
        toggleUpdate,
        userType,
        isAuthenticatedAsNotCompanyUser,
        handleRedirectToPlanning,
    }: {
        classes: Object,
        className: Object,
        user: Object,
        toggleUpdate: Function,
        userType: string,
        isAuthenticatedAsNotCompanyUser: boolean,
        handleRedirectToPlanning: Function,
    }) =>
    (<Grid item container alignItems="center" justify="center" direction="column" xs={12} sm={6}>
        <Grid item>
            <Avatar className={classNames(classes.profilePicture, className)} >
                { user && pathOr('', ['companyName'], user).substring(0, 2) }
            </Avatar>
        </Grid>
        <Grid item>
            <h1>
                { user && user.companyName }
            </h1>
        </Grid>
        {isAuthenticatedAsNotCompanyUser && (
            <Grid item >
                <ButtonValidate variant={'outlined'} color="secondary" onClick={handleRedirectToPlanning}>
                    Voir Son Planning
                </ButtonValidate>
            </Grid>
        )}
        { user && user.phoneNumber && <hr className={classNames(classes.hr, className)} /> }
        { userType === companyType &&
        <Grid item >
            <IconButton onClick={toggleUpdate} > <EditIcon /> </IconButton>
        </Grid> }
        <div> { user && user.phoneNumber && <PhoneIcon className={classNames(classes.icon, className)} /> } { user && user.phoneNumber }  </div>
        { user && user.phoneNumber && <hr className={classNames(classes.hr, className)} /> }
        <div> { user && user.email && <EmailIcon className={classNames(classes.icon, className)} /> } { user && user.email } </div>
        { user && user.email && <hr className={classNames(classes.hr, className)} />}
        <div>
            <LocationIcon
                className={classNames(classes.icon, className)}
            /> {user && `${user.codeZip} ${user.city}, ${user.address}, ${user.country} `}
        </div>
        { user && user.codeZip && user.city && user.address && user.country && <hr className={classNames(classes.hr, className)} /> }
        <div><ReceiptIcon className={classNames(classes.icon, className)} />  </div>
        <br />
        <div>
            { user && user.companyDescription }
        </div>
    </Grid>);

const View = (
    {
        classes,
        className,
        isUpdating,
        toggleUpdate,
        schoolsSelected,
        handleFormChange,
        schools,
        user,
        userID,
        form,
        error,
        updateUser,
        userType,
        companyActivationAPI,
        isAuthenticatedAsNotCompanyUser,
        handleToogleDeactivationDialog,
        handleOkDeactivateCompany,
        handleRedirectToPlanning,
        openDeactivationCompanyDialog,
    }
: {
        classes: Object,
        handleToogleDeactivationDialog: Function,
        handleOkDeactivateCompany: Function,
        className: Object,
        isUpdating: boolean,
        openDeactivationCompanyDialog: boolean,
        toggleUpdate: Function,
        schoolsSelected: Array,
        handleFormChange: Function,
        isAuthenticatedAsNotCompanyUser: boolean,
        schools: Array,
        user: Object,
        userID: number,
        form: Object,
        error: Object,
        updateUser: Function,
        userType: string,
        companyActivationAPI: Function,
        handleRedirectToPlanning: Function,
}) => (
    <Cover>
        <Helmet title="Profile entreprise" />
        { error && <Snackbar isOpen variant={errorType} message={error.message} /> }
        { userType === superAdminType && user && user.isActivated && (
            <div className={classNames(classes.switchButton, className)} >
                <IconButton color="secondary" onClick={handleToogleDeactivationDialog} >
                    <ToggleOffIcon className={classNames(classes.switchIcon, className)} />
                </IconButton>
                <Dialog
                    open={openDeactivationCompanyDialog}
                    title={`Êtes vous sur de vouloir désactiver l'entreprise ${pathOr('', ['companyName'], user)} ?`}
                    content={(
                        <div>
                            <p><strong>Attention ! </strong>En désactivant cette entreprise, vous supprimerez tout son planning et désinscrirez tous les étudiants liés à son planning.</p>
                        </div>
                    )}
                    handleOk={handleOkDeactivateCompany}
                    handleCancel={handleToogleDeactivationDialog}
                />
            </div>
        )}
        { userType === superAdminType && user && !user.isActivated &&
        <IconButton className={classNames(classes.switchButton, className)} onClick={() => companyActivationAPI(user._id)} >
            <ToggleOnIcon className={classNames(classes.switchIcon, className)} />
        </IconButton> }
        <Grid container spacing={8} >
            <Grid
                container
                item
                spacing={8}
                justify="center"
                alignItems="center"
                className={classNames(classes.container, className)}
            >
                { !isUpdating && user &&
                <ProfileDataView
                    classes={classes}
                    className={className}
                    user={user}
                    userID={userID}
                    toggleUpdate={toggleUpdate}
                    userType={userType}
                    handleRedirectToPlanning={handleRedirectToPlanning}
                    isAuthenticatedAsNotCompanyUser={isAuthenticatedAsNotCompanyUser}
                /> }
                { isUpdating && user &&
                <ProfileFormDataView
                    updateUser={updateUser}
                    classes={classes}
                    className={className}
                    schools={schools}
                    handleFormChange={handleFormChange}
                    schoolsSelected={schoolsSelected}
                    user={user}
                    form={form}
                    toggleUpdate={toggleUpdate}
                /> }
            </Grid>
            <Grid
                container
                item
                spacing={8}
                justify="center"
                alignItems="center"
                className={classNames(classes.container, className)}
            >
                {user &&
                <TraineeshipOffersTable
                    userType={userType}
                    userID={userID}
                    traineeships={pathOr([], ['traineeshipOffers'], user)}
                />}
            </Grid>
        </Grid>
    </Cover>
);

export default View;
