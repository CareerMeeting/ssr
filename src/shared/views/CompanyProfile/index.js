import withStyles from '@material-ui/core/styles/withStyles';
import { branch, compose, withState, withHandlers, lifecycle } from 'recompose';
import { path } from 'ramda';
import { connect } from 'react-redux';

import Content from './content';
import { styles } from './styles';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchWebConfig } from '../../redux/webConfig/actions';
import { fetchCompanyUser, fetchCompanyUserUpdate } from '../../redux/user/actions';
import { getCompany, companyDeactivation, companyActivation } from '../../redux/company/actions';
import { getQuery, navigateTo } from '../../helpers/navigation';
import { companyType } from '../../helpers/session/constants';
import { isAdminOrStudentUser } from '../../helpers/session';


const mapStateToProps = ({ entities, user, companies }, ownerProps) => {
    const { id } = getQuery(path(['location'], ownerProps));
    const userType = path(['session', 'userType'], entities);
    const companyDataPath = id ? ['companies', id] : ['user'];

    return ({
        userID: id,
        isAuthenticatedAsNotCompanyUser: isAdminOrStudentUser(userType),
        schools: path(['webConfig', 'schools'], entities),
        user: path(companyDataPath, entities),
        userType: path(['session', 'userType'], entities),
        isLoading: path(['isLoading'], user),
        error: path(['error'], user) || path(['error'], companies),
    });
};

const lifecyclePayload = {
    componentDidMount() {
        if (this.props.userType !== companyType && path(['id'], getQuery(this.props.location))) {
            this.props.getCompanyAPI(path(['id'], getQuery(this.props.location)));
        } else {
            this.props.fetchCompanyUser();
        }
    },
};

const container = compose(
    connect(
        mapStateToProps,
        {
            getCompanyAPI: getCompany,
            companyDeactivationAPI: companyDeactivation,
            companyActivationAPI: companyActivation,
            fetchWebConfig,
            fetchCompanyUser,
            fetchCompanyUserUpdateAPI: fetchCompanyUserUpdate,
        },
    ),
    lifecycle(lifecyclePayload),
    withState('openDeactivationCompanyDialog', 'setOpenDeactivationCompanyDialog', false),
    withState('isRedirectToPlanning', 'setIsRedirectToPlaning', false),
    branch(({ isRedirectToPlanning }) => isRedirectToPlanning, () => ({ userID }) => navigateTo(`/company/planning?id=${userID}`)()),
    withState('form', 'setForm', {
        email: '',
        companyName: '',
        city: '',
        codeZip: '',
        country: '',
        address: '',
        phoneNumber: '',
        companyDescription: '',
        schools: [],
    }),
    withState('isUpdating', 'setUpdating', false),
    withHandlers({
        handleToogleDeactivationDialog: ({ setOpenDeactivationCompanyDialog }) => () => {
            setOpenDeactivationCompanyDialog(prev => !prev);
        },
        handleOkDeactivateCompany: ({ setOpenDeactivationCompanyDialog, companyDeactivationAPI, user: { _id } = {} }) => () => {
            setOpenDeactivationCompanyDialog(false);
            if (!_id) return;

            companyDeactivationAPI(_id);
        },
        toggleUpdate: ({ isUpdating, setUpdating, setForm, user }) => () => {
            setForm(form => ({
                ...form,
                ...user,
            }));
            setUpdating(!isUpdating);
        },
        handleFormChange: ({ setForm }) => (event) => {
            const target = event.target;
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const name = target.name;

            setForm(form => ({
                ...form,
                [name]: value,
            }));
        },
        handleRedirectToPlanning: ({ setIsRedirectToPlaning }) => () => {
            setIsRedirectToPlaning(true);
        },
    }),
    withHandlers({
        updateUser: ({ fetchCompanyUserUpdateAPI, toggleUpdate }) => (form) => {
            fetchCompanyUserUpdateAPI(form);
            toggleUpdate();
        },
    }),
    withMenu,
    withBackgroundCard,
    withStyles(styles),
);

export default container(Content);
