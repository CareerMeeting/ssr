import React from 'react';
import Helmet from 'react-helmet';

import { Content, ResponsiveImage } from './styles';

import BadRequestImage from '../../assets/Images/bad-request.svg';
import withMenu from '../../hoc/withMenu';

const Index = () => (
    <Content>
        <Helmet title="Oops you're lost" />
        <ResponsiveImage src={BadRequestImage} alt="not found" />
    </Content>
);

export default withMenu(Index);
