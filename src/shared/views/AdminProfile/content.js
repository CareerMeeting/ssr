// @flow
import Helmet from 'react-helmet';
import React from 'react';
import IconButton from '@material-ui/core/IconButton/IconButton';
import classNames from 'classnames';
import EditIcon from '@material-ui/icons/Edit';

import Snackbar from '../../components/SnackBar';
import Title from '../../components/Title';
import type { FunctionnalComponent } from '../../app/types';
import { Cover, Body, SettingItem, Separator, List } from './styles';
import { errorType, successType } from '../../components/SnackBar/variants';
import { isNilOrEmpty } from '../../helpers/utils';
import Select from '../../components/Select';
import { ButtonValidate, IconCheck, WarningIcon, WarningMessage } from '../SuperAdminSettings/styles';
import Dialog from '../../components/Dialog';

const getDialogDeactivationAdminContent = () => (
    <WarningMessage>
        <strong><WarningIcon />Attention ! </strong>
        Une fois désactiver, cet administrateur ne pourra plus se connecter. Si par la suite, vous souhaitez le débloquer,
        il faudra le rajouter comme nouvel administrateur.
    </WarningMessage>
);

const renderSchools = (source = []) => !isNilOrEmpty(source) && (
    <List>
        {source.map((school, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <li key={`schopl-${i}`}>{school}</li>
        ))}
    </List>
);

const renderPerimeterActions = (
    {
        canEditPerimeterActions,
        perimeterActions = [],
        handleChangePerimeterActions,
        handleOkPerimeterActions,
        schoolsFromWebConfig = [],
    }) => {
    if (!canEditPerimeterActions) return renderSchools(perimeterActions);

    return (
        <div>
            <Select multiple source={schoolsFromWebConfig} destination={perimeterActions} handleChange={handleChangePerimeterActions} />
            <ButtonValidate variant={'outlined'} color="secondary" onClick={handleOkPerimeterActions}>
                Valider <IconCheck />
            </ButtonValidate>
        </div>
    );
};

const View = (
    {
        registrationCodeForCompanies,
        email,
        firstName,
        lastName,
        organizationName,
        perimeterActions = [],
        status,
        error,
        classes,
        className,
        handleCanEditPerimeterActions,
        canEditPerimeterActions,
        handleChangePerimeterActions,
        isAuthorizedToEdit,
        handleOkPerimeterActions,
        hasPostedSuccessfully,
        handleOkDeactivateAdmin,
        handleCancelDeactivationAdmin,
        handleOpenDeactivationDialog,
        openDeactivationAdminDialog,
        schoolsFromWebConfig,
    }
: {
        registrationCodeForCompanies: String,
        email: String,
        firstName: String,
        lastName: String,
        organizationName: String,
        perimeterActions: Array,
        status: String,
        error: Object,
        classes: Object,
        className: Object,
        handleCanEditPerimeterActions: Function,
        handleChangePerimeterActions: Function,
        canEditPerimeterActions: Boolean,
        isAuthorizedToEdit: Boolean,
        hasPostedSuccessfully: Boolean,
        openDeactivationAdminDialog: Boolean,
        handleOkPerimeterActions: Function,
        handleOkDeactivateAdmin: Function,
        handleCancelDeactivationAdmin: Function,
        handleOpenDeactivationDialog: Function,
        schoolsFromWebConfig: Array,
}): FunctionnalComponent => (
    <Cover>
        <Helmet title="Profil administrateur" />
        {error && <Snackbar isOpen variant={errorType} message={error.message} />}
        {hasPostedSuccessfully && <Snackbar isOpen variant={successType} message={'Votre action a été validée.'} />}

        <h1>Profil Administrateur</h1>
        <Body>
            <SettingItem>
                <Title>Code d'inscription à fournir aux entreprises</Title>
                <span>{registrationCodeForCompanies}</span>
            </SettingItem>
            <Separator />
            <SettingItem>
                <Title>Nom</Title>
                <span>{lastName}</span>
            </SettingItem>
            <Separator />
            <SettingItem>
                <Title>Prénom</Title>
                <span>{firstName}</span>
            </SettingItem>
            <Separator />
            <SettingItem>
                <Title>Email</Title>
                <span>{email}</span>
            </SettingItem>
            <Separator />
            <SettingItem>
                <Title>Organisation</Title>
                <span>{organizationName}</span>
            </SettingItem>
            <Separator />
            <SettingItem>
                <Title> Périmètre d'actions
                    {isAuthorizedToEdit && <IconButton className={classNames(classes.editButton, className)} onClick={handleCanEditPerimeterActions} >
                        <EditIcon className={classNames(classes.editIcon, className)} fontSize="small" />
                    </IconButton>}
                </Title>
                {renderPerimeterActions({
                    canEditPerimeterActions,
                    perimeterActions,
                    handleChangePerimeterActions,
                    handleOkPerimeterActions,
                    schoolsFromWebConfig,
                })}
            </SettingItem>
            <Separator />
            <SettingItem>
                <Title>Statut</Title>
                <span>{status}</span>
            </SettingItem>
            <Separator />
            {isAuthorizedToEdit && (
                <div>
                    <ButtonValidate variant={'outlined'} color="secondary" onClick={handleOpenDeactivationDialog}>
                        Désactiver cet administrateur <IconCheck />
                    </ButtonValidate>
                    <Dialog
                        open={openDeactivationAdminDialog}
                        title={'Êtes vous sur de vouloir désactiver cet administrateur ?'}
                        content={getDialogDeactivationAdminContent()}
                        handleOk={handleOkDeactivateAdmin}
                        handleCancel={handleCancelDeactivationAdmin}
                    />
                </div>
            )}
        </Body>
    </Cover>
);

export default View;
