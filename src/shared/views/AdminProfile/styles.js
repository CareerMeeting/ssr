import styled from 'react-emotion';

export const Cover = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'start-flex',
});

export const Body = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'center',
});

export const SettingItem = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 20,
    paddingTop: 20,
});

export const Separator = styled('hr')({
    width: 160,
});

export const List = styled('ul')({
    margin: 0,
});

export const styles = () => ({
    editButton: {
        border: '3px solid white',
        width: 10,
        height: 10,
    },
    editIcon: {
        position: 'absolute',
    },
});
