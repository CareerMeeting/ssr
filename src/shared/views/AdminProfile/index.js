import { branch, compose, lifecycle, renderComponent, withHandlers, withPropsOnChange, withState } from 'recompose';
import { connect } from 'react-redux';
import { path, pathOr } from 'ramda';
import { withStyles } from '@material-ui/core';

import Content from './content';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchAdminUser } from '../../redux/user/actions';
import LargeLoader from '../../components/LargeLoader';
import { styles } from './styles';
import { isNilOrEmpty } from '../../helpers/utils';
import { success } from '../../redux/resultStates';
import { getQuery } from '../../helpers/navigation';
import { superAdminType } from '../../helpers/session/constants';
import { fetchAdminById, changeAdminPerimeterActions, deactivateAdmin } from '../../redux/admins/actions';

const handlers = {
    handleCanEditPerimeterActions: ({ setCanEditPerimeterActions }) => () => {
        setCanEditPerimeterActions(prev => !prev);
    },
    handleOkPerimeterActions: ({ changeAdminPerimeterActionsAPI, id, perimeterActions }) => () => {
        if (isNilOrEmpty(perimeterActions)) return;

        changeAdminPerimeterActionsAPI(id, perimeterActions);
    },
    handleChangePerimeterActions: ({ setPerimeterActions }) => (event) => {
        setPerimeterActions(pathOr([], ['target', 'value'], event));
    },
    handleOpenDeactivationDialog: ({ setOpenDeactivationAdminDialog }) => () => {
        setOpenDeactivationAdminDialog(true);
    },
    handleCancelDeactivationAdmin: ({ setOpenDeactivationAdminDialog }) => () => {
        setOpenDeactivationAdminDialog(false);
    },
    handleOkDeactivateAdmin: ({ setOpenDeactivationAdminDialog, deactivateAdminAPI, id }) => () => {
        setOpenDeactivationAdminDialog(false);
        if (!id) return;

        deactivateAdminAPI(id);
    },
};

const lifecyclePlayload = {
    componentDidMount() {
        const { id } = getQuery(path(['location'], this.props));
        if (!id) {
            this.props.fetchAdminUser();
            return;
        }

        this.props.fetchAdminById(id);
    },
};

const getStatusBySecurityLevel = securityLevel => (securityLevel === 2 ? 'Super administrateur' : 'Administrateur');

const getHasPostedSuccessfully = (result, hasRetrievedOnly) => result === success && !hasRetrievedOnly;

const getUserById = (id, entities) => {
    const adminDataPath = id ? ['admins', id] : ['user'];

    return ({
        registrationCodeForCompanies: path([...adminDataPath, 'registrationCodeForCompanies'], entities),
        email: path([...adminDataPath, 'email'], entities),
        id: path([...adminDataPath, 'id'], entities),
        firstName: path([...adminDataPath, 'firstName'], entities),
        lastName: path([...adminDataPath, 'lastName'], entities),
        organizationName: path([...adminDataPath, 'organizationName'], entities),
        schools: path([...adminDataPath, 'schools'], entities),
        status: getStatusBySecurityLevel(path([...adminDataPath, 'securityLevel'], entities)),
        schoolsFromWebConfig: pathOr([], ['webConfig', 'schools'], entities),
    });
};

const mapStateToProps = ({ entities, admins }, ownerProps:
    { entities: EntitiesStructure, admins: StateElementStructure }) => {
    const { id } = getQuery(path(['location'], ownerProps));
    const userData = getUserById(id, entities);

    return ({
        ...userData,
        error: path(['error'], admins),
        isLoading: path(['isLoading'], admins),
        userType: path(['session', 'userType'], entities),
        hasPostedSuccessfully: getHasPostedSuccessfully(path(['result'], admins), path(['hasRetrievedOnly'], admins)),
    });
};

const connectToStore = connect(
    mapStateToProps,
    {
        fetchAdminUser,
        fetchAdminById,
        changeAdminPerimeterActionsAPI: changeAdminPerimeterActions,
        deactivateAdminAPI: deactivateAdmin,
    },
);

const initPerimeterActions = ({ schools, setIsAuthorizedToEdit, setPerimeterActions, userType }) => {
    setPerimeterActions(schools);
    setIsAuthorizedToEdit(userType === superAdminType);
};

const closeEditMode = ({ hasPostedSuccessfully, setCanEditPerimeterActions }) => {
    if (!hasPostedSuccessfully) return;

    setCanEditPerimeterActions(false);
};

const container = compose(
    connectToStore,
    lifecycle(lifecyclePlayload),
    withState('isAuthorizedToEdit', 'setIsAuthorizedToEdit', false),
    withState('openDeactivationAdminDialog', 'setOpenDeactivationAdminDialog', false),
    withState('canEditPerimeterActions', 'setCanEditPerimeterActions', false),
    withState('perimeterActions', 'setPerimeterActions', []),
    withPropsOnChange(['schools', 'userType'], initPerimeterActions),
    withPropsOnChange(['hasPostedSuccessfully'], closeEditMode),
    withHandlers(handlers),
    withMenu,
    withBackgroundCard,
    branch(({ isLoading }) => isLoading, renderComponent(LargeLoader)),
    withStyles(styles),
)(Content);

export default container;
