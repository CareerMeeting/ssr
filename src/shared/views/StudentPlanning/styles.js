import styled from 'react-emotion';

export const Cover = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'start-flex',
});

export const TitleAndInfoBody = styled('div')({
    textAlign: 'center',
});
