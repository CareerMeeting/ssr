import React from 'react';
import { Helmet } from 'react-helmet';
import Typography from '@material-ui/core/Typography';

import { Cover, TitleAndInfoBody } from './styles';
import SimpleScheduleStudent from '../../components/SimpleScheduleStudent';
import { errorType } from '../../components/SnackBar/variants';
import Snackbar from '../../components/SnackBar';
import EventDateInfo from '../../components/EventDateInfo';
import SubscribeSlotsMaxDateWarning from '../../components/SubscribeSlotsMaxDateWarning';

const View = (
    {
        error,
        queryID,
        isAuthenticatedAsNotStudentUser,
        studentIdentity,
    } : {
        error: Object,
        queryID: String,
        isAuthenticatedAsNotStudentUser: boolean,
        studentIdentity: String,
    }) =>
    (<Cover>
        <Helmet title="Planning Étudiant" />
        { error && <Snackbar isOpen variant={errorType} message={error.message} /> }

        <TitleAndInfoBody>
            <Typography gutterBottom variant="h4">{isAuthenticatedAsNotStudentUser ? `Planning de ${studentIdentity}` : 'Mon Planning'}</Typography>
            <EventDateInfo />
            <SubscribeSlotsMaxDateWarning />
        </TitleAndInfoBody>
        <SimpleScheduleStudent queryID={queryID} studentIdentity={studentIdentity} />
    </Cover>);

export default View;
