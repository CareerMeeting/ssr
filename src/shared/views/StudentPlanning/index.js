import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';

import Content from './content';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchStudentSlots } from '../../redux/user/actions';
import { fetchStudentById, fetchStudentSlotsByID } from '../../redux/student/actions';

import { getQuery } from '../../helpers/navigation';
import { isAdminUser } from '../../helpers/session';

const lifecyclePayload = {
    componentDidMount() {
        const { queryID } = this.props;
        if (queryID) {
            this.props.fetchStudentById(queryID);
            this.props.fetchStudentSlotsByID(queryID);
            return;
        }

        this.props.fetchStudentSlots();
    },
};

const mapStateToProps = ({ entities }, ownerProps) => {
    const { id } = getQuery(path(['location'], ownerProps));
    const studentDataPath = id ? ['students', id] : ['user'];
    const studentIdentity = ` ${path([...studentDataPath, 'lastName'], entities)} ${path([...studentDataPath, 'firstName'], entities)}`;
    const userType = path(['session', 'userType'], entities);

    return ({
        queryID: id,
        studentIdentity,
        isAuthenticatedAsNotStudentUser: isAdminUser(userType),
    });
};

const container = compose(
    connect(
        mapStateToProps,
        {
            fetchStudentSlots,
            fetchStudentSlotsByID,
            fetchStudentById,
        },
    ),
    lifecycle(lifecyclePayload),
    withMenu,
    withBackgroundCard,
);

export default container(Content);
