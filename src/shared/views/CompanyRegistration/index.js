import React from 'react';
import Helmet from 'react-helmet';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';
import { branch, compose, renderComponent, lifecycle } from 'recompose';
import { path, pathOr } from 'ramda';
import { connect } from 'react-redux';

import particlesParams from '../../assets/particlesConfig/particles-home-background.json';
import { getQuery, navigateTo } from '../../helpers/navigation';
import SubscribeForm from '../../components/RegistrationForm';
import { Cover, RegistrationCard, RegistrationCardHeader, ParticlesStyled, styles } from './styles';
import { signUpCompany } from '../../redux/company/actions';
import { fetchWebConfig } from '../../redux/webConfig/actions';
import Snackbar from '../../components/SnackBar';
import { errorType } from '../../components/SnackBar/variants';

const Index = ({ classes, className, signUp, registrationCode, error, schools } :
                   { classes: Object, className: Function, signUp: Function,
                       registrationCode: string, error: Object, schools: Array }) => (
    <Cover>
        <Helmet title="Registration" />
        {error && <Snackbar isOpen variant={errorType} message={error.response} />}
        <RegistrationCard raised>
            <RegistrationCardHeader className={classNames(classes.cardHeader, className)} elevation={9} >
                Inscription
            </RegistrationCardHeader>
            <SubscribeForm signUp={signUp} registrationCode={registrationCode} schools={schools} />
        </RegistrationCard>
        <ParticlesStyled params={particlesParams} />
    </Cover>
);

const lifecyclePayload = {
    componentDidMount() {
        this.props.fetchWebConfig();
    },
};

const mapStateToProps = ({ entities, session }) => ({
    isSignedUp: path(['session', 'isSignedUp'], entities),
    error: path(['error'], session),
    registrationCode: path(['codeChecker', 'code'], entities),
    schools: path(['webConfig', 'schools'], entities),
});

const container = compose(
    connect(
        mapStateToProps,
        {
            signUp: signUpCompany,
            fetchWebConfig,
        },
    ),
    lifecycle(lifecyclePayload),
    branch(
        ({ location, registrationCode } : { location: Object}): boolean => (
            pathOr(null, ['code'], getQuery(location)) !== registrationCode
        ),
        renderComponent(navigateTo('/')),
    ),
    branch(
        ({ isSignedUp } : { isSignedUp: boolean }): boolean => isSignedUp,
        renderComponent(navigateTo('/')),
    ),
    withStyles(styles),
);

export default container(Index);
