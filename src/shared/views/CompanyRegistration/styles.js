import styled from 'react-emotion';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper/Paper';
import Particles from 'react-particles-js';

export const Cover = styled('div')({
    alignItems: 'center',
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
});

export const RegistrationCard = styled(Card)({
    padding: '1%',
    maxWidth: '60%',
    overflow: 'visible !important',
    position: 'absolute',
});

export const RegistrationCardHeader = styled(Paper)({
    marginTop: '-80px',
    position: 'relative',
    padding: '3%',
});

export const ParticlesStyled = styled(Particles)({
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    height: '100%',
    display: 'flex',
    flex: 1,
});

export const styles = theme => ({
    cardHeader: {
        backgroundColor: theme.palette.secondary.main,
        color: 'white',
        marginBottom: '3%',
    },
});
