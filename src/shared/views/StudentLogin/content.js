// @flow
import Helmet from 'react-helmet';
import React from 'react';

import LargeLoader from '../../components/LargeLoader';
import type { FunctionnalComponent } from '../../app/types';
import { Content } from './styles';

const view = (): FunctionnalComponent => (
    <Content>
        <Helmet title="Authentication to student space" />
        <LargeLoader />
    </Content>
);

export default view;
