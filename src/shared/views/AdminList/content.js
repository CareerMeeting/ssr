// @flow
import Helmet from 'react-helmet';
import React from 'react';
import Typography from '@material-ui/core/Typography';

import Snackbar from '../../components/SnackBar';
import type { FunctionnalComponent } from '../../app/types';
import { Cover, Body } from './styles';
import { errorType, successType } from '../../components/SnackBar/variants';
import AdminsListTable from '../../components/AdminsListTable';
import AddAdmin from '../../components/AddAdmin';

const View = (
    {
        admins,
        error,
        hasPostedSuccessfully,
    }
: {
        hasPostedSuccessfully: Boolean,
        admins: Array,
        error: Object,
}): FunctionnalComponent => (
    <Cover>
        <Helmet title="Profil administrateur" />
        {error && <Snackbar isOpen variant={errorType} message={error.message} />}
        {hasPostedSuccessfully && <Snackbar isOpen variant={successType} message={'Votre action a été validée.'} />}

        <Typography gutterBottom variant="h4">Liste des administrateurs</Typography>
        <Body>
            <AddAdmin />
            <AdminsListTable admins={admins} />
        </Body>
    </Cover>
);

export default View;
