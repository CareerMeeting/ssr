import { branch, compose, lifecycle, renderComponent } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';

import Content from './content';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchAdmins } from '../../redux/admins/actions';
import LargeLoader from '../../components/LargeLoader';
import { convertHashmapToArray } from '../../helpers/utils';
import { success } from '../../redux/resultStates';

const lifecyclePlayload = {
    componentDidMount() {
        this.props.fetchAdmins();
    },
};

const getHasPostedSuccessfully = (result, hasRetrievedOnly) => result === success && !hasRetrievedOnly;

const mapStateToProps = ({ entities, admins }:
    { entities: EntitiesStructure, admins: StateElementStructure }) => ({
    admins: convertHashmapToArray(path(['admins'], entities)),
    error: path(['error'], admins),
    isLoading: path(['isLoading'], admins),
    hasPostedSuccessfully: getHasPostedSuccessfully(path(['result'], admins), path(['hasRetrievedOnly'], admins)),
});

const connectToStore = connect(
    mapStateToProps,
    {
        fetchAdmins,
    },
);

const container = compose(
    connectToStore,
    lifecycle(lifecyclePlayload),
    withMenu,
    withBackgroundCard,
    branch(({ isLoading }) => isLoading, renderComponent(LargeLoader)),
)(Content);

export default container;
