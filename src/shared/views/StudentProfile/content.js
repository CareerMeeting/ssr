// @flow
import Helmet from 'react-helmet';
import React from 'react';

import Snackbar from '../../components/SnackBar';
import type { FunctionnalComponent } from '../../app/types';
import { Cover, Body, EditIconStyled } from './styles';
import { errorType, successType } from '../../components/SnackBar/variants';
import Select from '../../components/Select';
import { ButtonValidate, IconCheck, WarningMessage } from '../SuperAdminSettings/styles';
import Dialog from '../../components/Dialog';
import StudentProfileEditMode from '../../components/StudentProfileEditMode';
import StudentProfileViewMode from '../../components/StudentProfileViewMode';
import Grid from "@material-ui/core/Grid/Grid";
import { NavLink } from 'react-router-dom';
import classNames from "classnames";

const getDialogDeactivationAdminContent = () => (
    <WarningMessage>
        Une fois désactiver, l'étudiant(e) ne pourra plus se connecter.
        Si par la suite, vous souhaitez le débloquer, vous pourrez.
    </WarningMessage>
);

const renderDegreeLevel = (degreeLevel = 'N/A') => (
    <span>{degreeLevel}</span>
);

const View = (
    {
        id,
        email,
        firstName,
        lastName,
        linkedInLink,
        degreeLevel,
        organizationName,
        phoneNumber,
        isDeactivatedByAdmin,
        status,
        error,
        classes,
        className,
        handleIsInEditMode,
        isInEditMode,
        handleChangeDegreeLevel,
        isAuthorizedToEdit,
        handleOkPerimeterActions,
        hasPostedSuccessfully,
        handleOkDeactivateStudent,
        handleCancelDeactivationStudent,
        handleOpenDeactivationDialog,
        openDeactivationStudentDialog,
        schoolsFromWebConfig,
        isAuthorizedTAsAdmin,
        handleOkActivateStudent,
    }
: {
        handleChange: Function,
        status: String,
        error: Object,
        classes: Object,
        className: Object,
        handleIsInEditMode: Function,
        handleChangeDegreeLevel: Function,
        isInEditMode: Boolean,
        isAuthorizedToEdit: Boolean,
        isAuthorizedTAsAdmin: Boolean,
        hasPostedSuccessfully: Boolean,
        openDeactivationStudentDialog: Boolean,
        handleOkPerimeterActions: Function,
        handleOkDeactivateStudent: Function,
        handleCancelDeactivationStudent: Function,
        handleOpenDeactivationDialog: Function,
        schoolsFromWebConfig: Array,
}): FunctionnalComponent => {
    const ProfileView = isInEditMode ? (
        <StudentProfileEditMode
            firstName={firstName}
            lastName={lastName}
            linkedInLink={linkedInLink}
            degreeLevel={degreeLevel}
            phoneNumber={phoneNumber}
            handleIsInEditMode={handleIsInEditMode}
        />
    ) : (
        <StudentProfileViewMode
            id={id}
            firstName={firstName}
            lastName={lastName}
            linkedInLink={linkedInLink}
            degreeLevel={degreeLevel}
            phoneNumber={phoneNumber}
            email={email}
            organizationName={organizationName}
        />
    );

    return (
        <Cover>
            <Helmet title="Profil étudiant" />
            {error && <Snackbar isOpen variant={errorType} message={error.message} />}
            {hasPostedSuccessfully && <Snackbar isOpen variant={successType} message={'Votre action a été validée.'} />}

            <Body>
                <Grid container spacing={8} >
                    {isAuthorizedToEdit && (
                        <Grid
                            container
                            item
                            spacing={8}
                            justify="center"
                            alignItems="center"
                        >
                            <ButtonValidate variant={'outlined'} color="primary" onClick={handleIsInEditMode} >
                                {isInEditMode ? 'Quitter le mode éditon' : 'Éditer Mon Profil'} <EditIconStyled />
                            </ButtonValidate>
                        </Grid>
                    )}
                    <Grid
                        container
                        item
                        spacing={8}
                        justify="center"
                        alignItems="center"
                    >
                        {ProfileView}
                    </Grid>
                    {isAuthorizedTAsAdmin && (
                        <Grid
                            container
                            item
                            spacing={8}
                            justify="center"
                            alignItems="center"
                        >
                            {!isDeactivatedByAdmin ? (
                                <Grid item>
                                    <ButtonValidate variant={'outlined'} color="secondary" onClick={handleOpenDeactivationDialog}>
                                        Désactiver cet étudiant
                                    </ButtonValidate>
                                    <Dialog
                                        open={openDeactivationStudentDialog}
                                        title={`Êtes vous sur de vouloir désactiver ${firstName} ${lastName} ?`}
                                        content={getDialogDeactivationAdminContent()}
                                        handleOk={handleOkDeactivateStudent}
                                        handleCancel={handleCancelDeactivationStudent}
                                    />
                                </Grid>
                            ) : (
                                <div>
                                    <ButtonValidate variant={'outlined'} color="secondary" onClick={handleOkActivateStudent}>
                                        Activer cet étudiant
                                    </ButtonValidate>
                                </div>
                            )}
                        </Grid>
                    )}
                </Grid>
            </Body>
        </Cover>
    );
};

export default View;
