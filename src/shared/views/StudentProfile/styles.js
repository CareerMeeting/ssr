import styled from 'react-emotion';
import EditIcon from '@material-ui/icons/Edit';
import { NavLink } from 'react-router-dom';

export const Cover = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'start-flex',
});

export const Body = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'center',
});

export const SettingItem = styled('div')({
    alignItems: 'center',
    display: 'block',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 10,
    paddingTop: 10,
});

export const Separator = styled('hr')({
    width: 160,
});

export const List = styled('ul')({
    margin: 0,
});

export const EditIconStyled = styled(EditIcon)({
    color: '#f50057',
});

export const styles = theme => ({
    editButton: {
        border: '3px solid white',
        width: 10,
        height: 10,
    },
    editIcon: {
        position: 'absolute',
    },
    inputLabel: {
        color: theme.palette.secondary.main,
    },
});
