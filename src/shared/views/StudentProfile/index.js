import { branch, compose, lifecycle, renderComponent, withHandlers, withPropsOnChange, withState } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { withStyles } from '@material-ui/core';

import Content from './content';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchStudentUser } from '../../redux/user/actions';
import LargeLoader from '../../components/LargeLoader';
import { styles } from './styles';
import { success } from '../../redux/resultStates';
import { getQuery } from '../../helpers/navigation';
import { fetchStudentById, activateStudent, deactivateStudent } from '../../redux/student/actions';
import { isAdminUser, isStudentUser } from '../../helpers/session';

const handlers = {
    handleIsInEditMode: ({ setIsInEditMode }) => () => {
        setIsInEditMode(prev => !prev);
    },
    handleOpenDeactivationDialog: ({ setOpenDeactivationStudentDialog }) => () => {
        setOpenDeactivationStudentDialog(true);
    },
    handleCancelDeactivationStudent: ({ setOpenDeactivationStudentDialog }) => () => {
        setOpenDeactivationStudentDialog(false);
    },
    handleOkDeactivateStudent: ({ setOpenDeactivationStudentDialog, deactivateStudentAPI, id }) => () => {
        setOpenDeactivationStudentDialog(false);
        if (!id) return;

        deactivateStudentAPI(id);
    },
    handleOkActivateStudent: ({ activateStudentAPI, id }) => () => {
        if (!id) return;

        activateStudentAPI(id);
    },
    handleChangeDegreeLevel: ({ setForm }) => (event) => {
        const target = event.target;
        const value = target.value;

        setForm(form => ({
            ...form,
            degreeLevel: value,
        }));
    },
    handleChange: ({ setForm }) => (event) => {
        const target = event.target;
        const value = target.value;
        const id = target.id;

        setForm(form => ({
            ...form,
            [id]: value,
        }));
    },
};

const lifecyclePlayload = {
    componentDidMount() {
        const { id } = getQuery(path(['location'], this.props));
        if (!id) {
            this.props.fetchStudentUser();
            return;
        }

        this.props.fetchStudentById(id);
    },
};

const getHasPostedSuccessfully = (result, hasRetrievedOnly) => result === success && hasRetrievedOnly === false;

const getUserById = (id, entities) => {
    const studentDataPath = id ? ['students', id] : ['user'];

    return ({
        id: path([...studentDataPath, '_id'], entities),
        email: path([...studentDataPath, 'email'], entities),
        firstName: path([...studentDataPath, 'firstName'], entities),
        lastName: path([...studentDataPath, 'lastName'], entities),
        organizationName: path([...studentDataPath, 'organizationName'], entities),
        degreeLevel: path([...studentDataPath, 'degreeLevel'], entities),
        linkedInLink: path([...studentDataPath, 'linkedInLink'], entities),
        phoneNumber: path([...studentDataPath, 'phoneNumber'], entities),
        isDeactivatedByAdmin: path([...studentDataPath, 'isDeactivatedByAdmin'], entities),
    });
};


const mapStateToProps = ({ entities, students, user }, ownerProps:
    { entities: EntitiesStructure, students: StateElementStructure }) => {
    const { id } = getQuery(path(['location'], ownerProps));
    const userData = getUserById(id, entities);
    const log = id ? students : user;
    const userType = path(['session', 'userType'], entities);

    return ({
        ...userData,
        error: path(['error'], user) || path(['error'], students),
        isLoading: path(['isLoading'], user) || path(['isLoading'], students),
        userType,
        isAuthorizedToEdit: isStudentUser(userType),
        isAuthorizedTAsAdmin: isAdminUser(userType),
        hasPostedSuccessfully: getHasPostedSuccessfully(path(['result'], log), path(['hasRetrievedOnly'], log)),
    });
};

const connectToStore = connect(
    mapStateToProps,
    {
        fetchStudentUser,
        fetchStudentById,
        activateStudentAPI: activateStudent,
        deactivateStudentAPI: deactivateStudent,
    },
);

const closeEditMode = ({ hasPostedSuccessfully, setCanEditPerimeterActions }) => {
    if (!hasPostedSuccessfully) return;

    setCanEditPerimeterActions(false);
};

const container = compose(
    connectToStore,
    lifecycle(lifecyclePlayload),
    withState('isInEditMode', 'setIsInEditMode', false),
    withState('openDeactivationStudentDialog', 'setOpenDeactivationStudentDialog', false),
    withState('canEditPerimeterActions', 'setCanEditPerimeterActions', false),
    withState('perimeterActions', 'setPerimeterActions', []),
    withPropsOnChange(['hasPostedSuccessfully'], closeEditMode),
    withHandlers(handlers),
    withMenu,
    withBackgroundCard,
    branch(({ isLoading }) => isLoading, renderComponent(LargeLoader)),
    withStyles(styles),
)(Content);

export default container;
