// @flow
import React from 'react';
import Helmet from 'react-helmet';

import Snackbar from '../../components/SnackBar';
import type { FunctionnalComponent } from '../../app/types';
import { errorType } from '../../components/SnackBar/variants';
import { IconLogIn, ButtonLogIn, Cover } from './styles';

const View = ({ microsoftOnlineAuthUrl, error, removeError }
: { microsoftOnlineAuthUrl: string, error: Object, removeError: Function }): FunctionnalComponent => (
    <Cover>
        <Helmet title="Espace Admin" />
        {error && <Snackbar isOpen variant={errorType} message={error.message} propagateOnClose={removeError} />}
        <ButtonLogIn variant="contained" color="primary" href={microsoftOnlineAuthUrl}>
            Espace Admin
            <IconLogIn />
        </ButtonLogIn>
    </Cover>
);

export default View;
