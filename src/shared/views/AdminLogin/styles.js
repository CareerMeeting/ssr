import Button from '@material-ui/core/Button';
import InputIcon from '@material-ui/icons/Input';
import styled from 'react-emotion';

export const Cover = styled('div')({
    alignItems: 'center',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
});

export const ButtonLogIn = styled(Button)({
    margin: 5,
});

export const IconLogIn = styled(InputIcon)({
    marginLeft: 5,
});
