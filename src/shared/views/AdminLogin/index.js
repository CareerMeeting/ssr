import { compose, lifecycle, setPropTypes } from 'recompose';
import { connect } from 'react-redux';
import { string } from 'prop-types';
import { path } from 'ramda';

import Content from './content';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import { adminType } from '../../redux/microsoftOnline/user-types';
import { fetchMOAuth } from '../../redux/microsoftOnline/actions';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';

const lifecyclePlayload = {
    componentDidMount() {
        this.props.fetchMOAuth(adminType);
    },
};

const mapStateToProps = ({ entities, session }
                             : { entities: EntitiesStructure, session: StateElementStructure }) => ({
    error: path(['error'], session),
    microsoftOnlineAuthUrl: path(['session', 'microsoftOnlineAuthUrl'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    {
        fetchMOAuth,
    },
);

const container = compose(
    connectToStore,
    lifecycle(lifecyclePlayload),
    setPropTypes({
        microsoftOnlineAuthUrl: string,
    }),
    withMenu,
    withBackgroundCard,
)(Content);

export default container;
