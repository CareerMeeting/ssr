// @flow
import Helmet from 'react-helmet';
import React from 'react';
import Typography from '@material-ui/core/Typography/Typography';

import Snackbar from '../../components/SnackBar';
import type { FunctionnalComponent } from '../../app/types';
import { Cover, Body } from './styles';
import { errorType } from '../../components/SnackBar/variants';
import StudentsListTable from '../../components/StudentsListTable';
import Select from '../../components/Select';
import { superAdminType } from '../../helpers/session/constants';

const View = (
    {
        studentsToView,
        schoolSelected,
        schoolsFromWebConfig,
        handleChangeSchoolToView,
        userType,
        error,
    }
: {
        studentsToView: Array,
        schoolsFromWebConfig: Array,
        userType: String,
        schoolSelected: String,
        handleChangeSchoolToView: Function,
        error: Object,
}): FunctionnalComponent => (
    <Cover>
        <Helmet title="Liste des étudiantsr" />
        {error && <Snackbar isOpen variant={errorType} message={error.message} />}

        <Typography gutterBottom variant="h4">Liste des étudiants</Typography>
        <Body>
            {userType === superAdminType && (
                <Select source={schoolsFromWebConfig} destination={schoolSelected} handleChange={handleChangeSchoolToView} />
            )}
            <StudentsListTable data={studentsToView} />
        </Body>
    </Cover>
);

export default View;
