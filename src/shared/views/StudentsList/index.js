import { branch, compose, lifecycle, renderComponent, withHandlers, withPropsOnChange, withState } from 'recompose';
import { connect } from 'react-redux';
import { filter, path, pathOr } from 'ramda';

import Content from './content';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchStudents } from '../../redux/student/actions';
import LargeLoader from '../../components/LargeLoader';
import { convertHashmapToArray } from '../../helpers/utils';
import { superAdminType } from '../../helpers/session/constants';

const handlers = {
    handleChangeSchoolToView: ({ setSchoolSelected }) => (event) => {
        setSchoolSelected(path(['target', 'value'], event));
    },
};

const lifecyclePlayload = {
    componentDidMount() {
        this.props.fetchStudents();
    },
};

const mapStateToProps = ({ entities, students }: { entities: EntitiesStructure, students: StateElementStructure }) => ({
    students: convertHashmapToArray(path(['students'], entities)),
    userType: path(['session', 'userType'], entities),
    error: path(['error'], students),
    isLoading: path(['isLoading'], students),
    schoolsFromWebConfig: pathOr([], ['webConfig', 'schools'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    {
        fetchStudents,
    },
);

const changeStudents = ({ userType, schoolSelected = '', setStudentsToView, students = [] }) => {
    if (userType !== superAdminType) {
        setStudentsToView(students);
        return;
    }

    setStudentsToView(filter((student = {}) => student.organizationName.toLowerCase() === schoolSelected.toLowerCase(), students));
};

const container = compose(
    connectToStore,
    lifecycle(lifecyclePlayload),
    withState('schoolSelected', 'setSchoolSelected', ''),
    withState('studentsToView', 'setStudentsToView', ({ students }) => students),
    withHandlers(handlers),
    withPropsOnChange(['schoolSelected'], changeStudents),
    withMenu,
    withBackgroundCard,
    branch(({ isLoading }) => isLoading, renderComponent(LargeLoader)),
)(Content);

export default container;
