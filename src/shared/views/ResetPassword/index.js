import withStyles from '@material-ui/core/styles/withStyles';
import { compose, withState, withHandlers } from 'recompose';
import { path } from 'ramda';
import { connect } from 'react-redux';

import { styles } from './styles';
import View from './content';
import { getQuery } from '../../helpers/navigation';
import { querySendResetPasswordMail, querySetCompanyPassword } from '../../redux/company/actions';
import { success } from '../../redux/resultStates';

const hasPostedSuccessfully = (result, hasSentEmail, hasUpdatedPass) => result === success && (hasSentEmail || hasUpdatedPass);

const mapStateToProps = ({ session, entities }, { location }) => ({
    error: path(['error'], session),
    isLoading: path(['isLoading'], session),
    hasPostedSuccessfully: hasPostedSuccessfully(path(['result'], session),
        path(['resetPass', 'hasSentEmail'], entities),
        path(['resetPass', 'hasUpdatedPass'], entities)),
    code: path(['code'], getQuery(location)),
});

const container = compose(
    connect(
        mapStateToProps,
        {
            sendMail: querySendResetPasswordMail,
            setUserPassword: querySetCompanyPassword,
        },
    ),
    withState('email', 'setEmail', ''),
    withState('password', 'setPassword', ''),
    withHandlers({
        handlePasswordChange: ({ setPassword }) => (event) => {
            setPassword(path(['target', 'value'], event));
        },
        handleEmailChange: ({ setEmail }) => (event) => {
            setEmail(path(['target', 'value'], event));
        },
    }),
    withStyles(styles),
);

export default container(View);
