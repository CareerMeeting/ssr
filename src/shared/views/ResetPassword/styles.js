import styled from 'react-emotion';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Send from '@material-ui/icons/Send';
import Particles from 'react-particles-js';

export const Content = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'center',
});

export const Cover = styled('div')({
    alignItems: 'center',
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
});

export const Field = styled(TextField)({
    minWidth: 300,
    margin: '6% 0',
    display: 'grid',
});

export const ButtonLogin = styled(Button)({
    width: '100%',
});

export const PasswordCard = styled(Card)({
    padding: '1%',
    maxWidth: '60%',
    overflow: 'visible !important',
    position: 'absolute',
});

export const CardHeader = styled(Paper)({
    marginTop: '-50px',
    position: 'relative',
    padding: '8%',
});

export const SendIcon = styled(Send)({
    marginLeft: 5,
});

export const ParticlesStyled = styled(Particles)({
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    height: '100%',
    display: 'flex',
    flex: 1,
});

export const styles = theme => ({
    cardHeader: {
        backgroundColor: theme.palette.secondary.main,
        color: 'white',
        marginBottom: '3%',
    },
    buttonWrapper: {
        margin: theme.spacing.unit,
        position: 'relative',
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -15,
        marginLeft: -18,
    },
});
