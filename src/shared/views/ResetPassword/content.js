import React from 'react';
import Helmet from 'react-helmet';
import classNames from 'classnames';
import InputAdornment from '@material-ui/core/InputAdornment';
import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';
import CircularProgress from '@material-ui/core/CircularProgress';

import { Cover, PasswordCard, CardHeader, Field, ButtonLogin, SendIcon, ParticlesStyled, Content } from './styles';
import Snackbar from '../../components/SnackBar';
import particlesParams from '../../assets/particlesConfig/particles-home-background.json';
import { errorType, successType } from '../../components/SnackBar/variants';
import type { FunctionnalComponent } from '../../app/types';

const PasswordForm = (
    {
        code,
        classes,
        password,
        className,
        isLoading,
        setUserPassword,
        handlePasswordChange,
        hasPostedSuccessfully,
    } : {
        classes: Object,
        className: Function,
        code: string,
        setUserPassword: Function,
        handlePasswordChange: Function,
        hasPostedSuccessfully: Function,
        password: string,
        isLoading: boolean,
}) : FunctionnalComponent => (
    <Content>
        <Field
            id="password"
            variant="outlined"
            type="password"
            onChange={handlePasswordChange}
            label="Nouveau mot de passe"
            InputProps={{
                endAdornment: (
                    <InputAdornment position="end">
                        <LockIcon />
                    </InputAdornment>
                ),
            }}
        />
        <div className={classNames(classes.buttonWrapper, className)}>
            <ButtonLogin
                variant="contained"
                color="secondary"
                onClick={() => setUserPassword(code, password)}
                disabled={isLoading || hasPostedSuccessfully}
            >
                Mettre à jour
                <SendIcon />
            </ButtonLogin>
            {isLoading &&
            (<CircularProgress
                size={30}
                className={classNames(classes.buttonProgress, className)}
                color="secondary"
            />)}
        </div>
    </Content>
);

const EmailForm = (
    {
        email,
        classes,
        sendMail,
        className,
        isLoading,
        handleEmailChange,
        hasPostedSuccessfully,
    } : {
        classes: Object,
        className: Function,
        sendMail: Function,
        hasPostedSuccessfully: Function,
        handleEmailChange: Function,
        email: string,
        isLoading: boolean,
}) : FunctionnalComponent => (
    <Content>
        <Field
            id="email"
            variant="outlined"
            type="email"
            label="Email"
            onChange={handleEmailChange}
            InputProps={{
                endAdornment: (
                    <InputAdornment position="end">
                        <EmailIcon />
                    </InputAdornment>
                ),
            }}
        />
        <div className={classNames(classes.buttonWrapper, className)}>
            <ButtonLogin
                variant="contained"
                color="secondary"
                onClick={() => sendMail(email)}
                disabled={isLoading || hasPostedSuccessfully}
            >
                Envoyer
                <SendIcon />
            </ButtonLogin>
            {isLoading &&
            (<CircularProgress
                size={30}
                className={classNames(classes.buttonProgress, className)}
                color="secondary"
            />)}
        </div>
    </Content>
);

const View = (
    {
        classes,
        className,
        error,
        code,
        sendMail,
        setUserPassword,
        handlePasswordChange,
        hasPostedSuccessfully,
        handleEmailChange,
        email,
        password,
        isLoading,
    } : {
        classes: Object,
        className: Function,
        error: Object,
        code: string,
        sendMail: Function,
        setUserPassword: Function,
        handlePasswordChange: Function,
        hasPostedSuccessfully: Function,
        handleEmailChange: Function,
        email: string,
        password: string,
        isLoading: boolean,
}) => (
    <Cover>
        <Helmet title="Registration" />
        {error && <Snackbar isOpen variant={errorType} message={error.message} />}
        {hasPostedSuccessfully && !code && <Snackbar isOpen variant={successType} message={'Vérifiez votre boite mail'} />}
        {hasPostedSuccessfully && code && <Snackbar isOpen variant={successType} message={'Votre mot de passe a été mise à jour'} />}

        <PasswordCard raised>
            <CardHeader className={classNames(classes.cardHeader, className)} elevation={9} >
                Réinitialisation mot de passe
            </CardHeader>
            {code && (
                <PasswordForm
                    code={code}
                    classes={classes}
                    password={password}
                    className={className}
                    isLoading={isLoading}
                    setUserPassword={setUserPassword}
                    handlePasswordChange={handlePasswordChange}
                    hasPostedSuccessfully={hasPostedSuccessfully}
                />
            )}
            {!code && (
                <EmailForm
                    email={email}
                    classes={classes}
                    sendMail={sendMail}
                    className={className}
                    isLoading={isLoading}
                    handleEmailChange={handleEmailChange}
                    hasPostedSuccessfully={hasPostedSuccessfully}
                />
            )}
        </PasswordCard>

        <ParticlesStyled params={particlesParams} />
    </Cover>
);

export default View;
