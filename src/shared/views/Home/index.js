import withStyles from '@material-ui/core/styles/withStyles';
import { branch, compose, withState, withHandlers, lifecycle, setPropTypes, renderComponent } from 'recompose';
import { connect } from 'react-redux';
import { string } from 'prop-types';
import { path } from 'ramda';

import Content, { goToSubscribe } from './content';
import { fetchMOAuth } from '../../redux/microsoftOnline/actions';
import { authenticateCompany } from '../../redux/company/actions';
import { studentType } from '../../redux/microsoftOnline/user-types';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import { styles } from './styles';
import { navigateTo } from '../../helpers/navigation';
import { isStudentUser, isAdminUser, isCompanyUser } from '../../helpers/session';

const lifecyclePlayload = {
    componentDidMount() {
        this.props.fetchMOAuth(studentType);
    },
};

const mapStateToProps = ({ entities, session, codeChecker }
                             : { entities: EntitiesStructure, session: StateElementStructure }) => ({
    error: path(['error'], session) || path(['error'], codeChecker),
    microsoftOnlineAuthUrl: path(['session', 'microsoftOnlineAuthUrl'], entities),
    session,
    registrationCode: path(['codeChecker', 'code'], entities),
    userType: path(['session', 'userType'], entities),
    token: path(['session', 'token'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    {
        fetchMOAuth,
        authenticateCompany,
    },
);

const componentProps = compose(
    withState('open', 'setOpen', true),
    withHandlers({
        toggleOpen: ({ setOpen, open }) => () => {
            setOpen(!open);
        },
        closeAuth: ({ setOpen }) => () => {
            setOpen(true);
        },
    }),
);

const container = compose(
    connectToStore,
    lifecycle(lifecyclePlayload),
    setPropTypes({
        microsoftOnlineAuthUrl: string,
    }),
    branch(
        ({ token, userType }: { token: string, userType: boolean }): boolean => !!token && isStudentUser(userType),
        renderComponent(navigateTo('/student/profile')),
    ),
    branch(
        ({ token, userType }: { token: string, userType: boolean }): boolean => !!token && isCompanyUser(userType),
        renderComponent(navigateTo('/company/profile')),
    ),
    branch(
        ({ token, userType }: { token: string, userType: boolean }): boolean => !!token && isAdminUser(userType),
        renderComponent(navigateTo('/admin/profile')),
    ),
    branch(
        ({ registrationCode }: { registrationCode: string }): boolean => !!registrationCode,
        renderComponent(goToSubscribe),
    ),
    componentProps,
    withStyles(styles),
);


export default container(Content);
