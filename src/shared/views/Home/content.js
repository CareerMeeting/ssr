/* eslint-disable react/prop-types */
// @flow
import React from 'react';
import Helmet from 'react-helmet';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper/Paper';
import { Redirect } from 'react-router-dom';

import LoginForm from '../../components/LoginForm';
import particlesParams from '../../assets/particlesConfig/particles-home-background.json';
import Snackbar from '../../components/SnackBar';
import type { FunctionnalComponent } from '../../app/types';
import { ButtonLogIn, Cover, WorkIconLogin, AssigmentIconLogin, SpaceCard, LoginCard, ParticlesStyled, GrowStyled, styles } from './styles';
import { errorType } from '../../components/SnackBar/variants';

export const goToSubscribe = ({ registrationCode }) => <Redirect to={`/company/subscribe?code=${registrationCode}`} />;

const SpaceCardHeader = ({ classes, className }) => (
    <Paper elevation={9} className={classNames(classes.root, className)} >
        Career Meeting IONIS
    </Paper>
);

const SpaceCardHeaderStyled = withStyles(styles)(SpaceCardHeader);

const SpaceView = (toggleOpen, microsoftOnlineAuthUrl): FunctionnalComponent => (
    <SpaceCard raised>
        <SpaceCardHeaderStyled />
        <ButtonLogIn variant="contained" color="primary" onClick={toggleOpen}>
            Espace Entreprise
            <WorkIconLogin />
        </ButtonLogIn>
        <ButtonLogIn variant="contained" color="primary" href={microsoftOnlineAuthUrl}>
            Espace Etudiant
            <AssigmentIconLogin />
        </ButtonLogIn>
    </SpaceCard>
);

const View = ({ toggleOpen, open, microsoftOnlineAuthUrl, error, authenticateCompany, classes, className, session }
                  : {toggleOpen: Function, open: boolean,
                        microsoftOnlineAuthUrl: string, error: Object})
    : FunctionnalComponent => {
    const spaceView = open ? SpaceView(toggleOpen, microsoftOnlineAuthUrl) : <noscript />;
    const loginForm = !open ?
        (<LoginCard raised>
            <LoginForm login={authenticateCompany} isLoading={session.isLoading} toggleOpen={toggleOpen} />
        </LoginCard>)
        : <noscript />;

    return (
        <Cover>
            {error && <Snackbar isOpen variant={errorType} message={error.message} />}
            <Helmet title="Career Meeting" />
            <GrowStyled in={open} className={classNames(classes.spaceCard, className)}>
                {spaceView}
            </GrowStyled>
            <GrowStyled in={!open}>
                {loginForm}
            </GrowStyled>
            <ParticlesStyled params={particlesParams} canvasClassName={classes.canvasParticles} />
        </Cover>
    );
};

export default View;
