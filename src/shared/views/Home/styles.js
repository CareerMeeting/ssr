import AssignmentIcon from '@material-ui/icons/AssignmentInd';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import InputIcon from '@material-ui/icons/Input';
import Particles from 'react-particles-js';
import styled from 'react-emotion';
import WorkIcon from '@material-ui/icons/Work';
import Grow from '@material-ui/core/Grow/Grow';

export const Cover = styled('div')({
    alignItems: 'center',
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
});

export const GrowStyled = styled(Grow)({
    position: 'absolute',
    display: 'grid',
});

export const LoginCard = styled(Card)({
    padding: '2%',
    textAlign: 'center',
    overflow: 'visible !important',
});

export const SpaceCard = styled(Card)({
    padding: '1%',
    display: 'inherit',
});

export const ButtonLogIn = styled(Button)({
    margin: '18px !important',
});

export const IconLogIn = styled(InputIcon)({
    marginLeft: 5,
});

export const WorkIconLogin = styled(WorkIcon)({
    marginLeft: 5,
});

export const AssigmentIconLogin = styled(AssignmentIcon)({
    marginLeft: 5,
});

export const ParticlesStyled = styled(Particles)({
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    height: '100%',
    display: 'flex',
    flex: 1,
});

export const styles = theme => ({
    canvasParticles: {
        display: 'block',
    },
    root: {
        backgroundColor: theme.palette.secondary.main,
        alignItems: 'center',
        display: 'flex',
        marginTop: -55,
        justifyContent: 'center',
        color: 'white',
        lineHeight: '5',
        userSelect: 'none',
    },
    spaceCard: {
        overflow: 'visible',
    },
});
