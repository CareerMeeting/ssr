// @flow
import { branch, compose, lifecycle, setPropTypes, renderComponent } from 'recompose';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { string } from 'prop-types';

import Content from './content';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import { adminType } from '../../redux/microsoftOnline/user-types';
import { fetchCMWithMOAuth } from '../../redux/microsoftOnline/actions';
import { getQuery, navigateTo } from '../../helpers/navigation';

const lifecyclePayload = {
    componentDidMount() {
        const { code } = getQuery(path(['location'], this.props));
        this.props.fetchCMWithMOAuth(code, adminType);
    },
};

const mapStateToProps = ({ entities, session }: { entities: EntitiesStructure, session: StateElementStructure }) => ({
    error: path(['error'], session),
    token: path(['session', 'token'], entities),
});

const connectToStore = connect(
    mapStateToProps,
    { fetchCMWithMOAuth },
);

const container = compose(
    connectToStore,
    lifecycle(lifecyclePayload),
    setPropTypes({
        token: string,
    }),
    // Is Successful
    branch(
        ({ token }: { token: string }): boolean => !!token,
        renderComponent(navigateTo('/admin/profile')),
    ),
    // Has error
    branch(
        ({ error }): boolean => !!error,
        renderComponent(navigateTo('/admin/')),
    ),
)(Content);

export default container;
