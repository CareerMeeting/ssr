import Button from '@material-ui/core/Button';
import CheckIcon from '@material-ui/icons/Check';
import WarningIconMU from '@material-ui/icons/Warning';
import styled from 'react-emotion';
import green from '@material-ui/core/colors/green';

import { orange } from '../../app/colors';

export const Cover = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'start-flex',
});

export const Body = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'center',
});

export const SettingItem = styled('div')({
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 20,
    paddingTop: 20,
});

export const Separator = styled('hr')({
    width: 160,
});

export const ButtonValidate = styled(Button)({
    margin: 5,
    color: green,
    borderColor: green,
});

export const IconCheck = styled(CheckIcon)({
    marginLeft: 5,
});

export const WarningIcon = styled(WarningIconMU)({
    marginRight: 5,
});

export const WarningMessage = styled('span')({
    color: orange,
    fontFamily: 'bold',
});
