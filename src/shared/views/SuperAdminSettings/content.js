// @flow
import Helmet from 'react-helmet';
import React from 'react';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import Dialog from '../../components/Dialog';
import Snackbar from '../../components/SnackBar';
import type { FunctionnalComponent } from '../../app/types';
import { Cover, Body, SettingItem, Separator, ButtonValidate, IconCheck, WarningIcon, WarningMessage } from './styles';
import { errorType, successType } from '../../components/SnackBar/variants';

const getStateSiteAction = activateSite => (activateSite ? 'activer' : 'désactiver');

const getDialogSiteStateContent = (activateSite) => {
    if (!activateSite) {
        return (
            <div>
                <WarningMessage>
                    <strong><WarningIcon />Attention ! </strong>
                En désactivant le site, vous désactiverez toutes les entreprises et étudiants de la plateforme.
                </WarningMessage>
                <p>Les seuls qui pourront encore se connecter à la plateforme seront les administrateurs.</p>
                <p>Les données des entreprises et étudiants ne seront pas perdues.
                Cependant il faudra une reconnexion de leurs parts pour une réactivation de leur profil.</p>
            </div>
        );
    }

    return (
        <div>
            <p>En activant le site, vous autorisez l'accès à la plateforme des entreprises et étudiants.</p>
            <p>Les données des étudiants/entreprises des années précédentes ne sont pas perdues.
                Il faut une connexion de leurs parts pour réactiver leur profil.</p>
        </div>
    );
};

const View = (
    {
        activateSite,
        deactivatedText,
        error,
        handleCancelSiteStateDialog,
        handleChangeDeactivateText,
        handleChangeSiteState,
        handleOkSiteStateDialog,
        handleValidateDeactivateText,
        handleSetEventDate,
        handleValidateEventDate,
        openSiteStateDialog,
        hasPostedSuccessfully,
        canEdit,
        eventDate,
    }
: {
        activateSite: Boolean,
        deactivatedText: String,
        error: Object,
        handleCancelSiteStateDialog: Function,
        handleChangeDeactivateText: Function,
        handleChangeSiteState: Function,
        handleOkSiteStateDialog: Function,
        handleValidateDeactivateText: Function,
        handleSetEventDate: Function,
        handleValidateEventDate: Function,
        openSiteStateDialog: Boolean,
        hasPostedSuccessfully: Boolean,
        canEdit: Boolean,
        eventDate: String,
}): FunctionnalComponent => (
    <Cover>
        <Helmet title="Paramètres du site" />
        {error && <Snackbar isOpen variant={errorType} message={error.message} />}
        {hasPostedSuccessfully && <Snackbar isOpen variant={successType} message={'Votre action a été validée.'} />}

        <Typography gutterBottom variant="h4">Paramètres du site</Typography>
        <Body>
            <SettingItem>
                <span>Activation du site</span>
                <Switch disabled={!canEdit} onChange={handleChangeSiteState} checked={activateSite} value={activateSite} />
                <Dialog
                    open={openSiteStateDialog}
                    title={`Êtes vous sur de vouloir ${getStateSiteAction(activateSite)} le site ?`}
                    content={getDialogSiteStateContent(activateSite)}
                    handleOk={handleOkSiteStateDialog}
                    handleCancel={handleCancelSiteStateDialog}
                />
            </SettingItem>
            <Separator />
            <SettingItem>
                <span>Texte d'affichage quand le site est désactivé</span>
                <TextField
                    disabled={!canEdit}
                    id="desactivated-text"
                    label="Texte du site désactivé"
                    multiline
                    placeholder={'Le site est fermé. À l\'année prochaine 👋'}
                    rowsMax="4"
                    value={deactivatedText}
                    onChange={handleChangeDeactivateText}
                    margin="normal"
                    helperText="Ce texte apparaît sur la page d'accueil lorsque il est désactivé."
                    variant="outlined"
                />
                <ButtonValidate disabled={!canEdit} variant={'outlined'} color="secondary" onClick={handleValidateDeactivateText}>
                    Valider mon texte d'affichage
                    <IconCheck />
                </ButtonValidate>
            </SettingItem>
            <Separator />
            <SettingItem>
                <span> Date de l'évenement </span> <br />
                <TextField
                    id="date"
                    type="date"
                    defaultValue={eventDate}
                    onChange={handleSetEventDate}
                    helperText=" "
                    variant="outlined"
                    disabled={!canEdit}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <ButtonValidate disabled={!canEdit} variant={'outlined'} color="secondary" onClick={handleValidateEventDate}>
                    Valider la date de l'évenement
                    <IconCheck />
                </ButtonValidate>
            </SettingItem>
        </Body>
    </Cover>
);

export default View;
