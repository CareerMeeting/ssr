import { compose, lifecycle, setPropTypes, withHandlers, withState, withProps, withPropsOnChange } from 'recompose';
import { connect } from 'react-redux';
import { func, string } from 'prop-types';
import { path, pathOr, isNil } from 'ramda';
import moment from 'moment';

import Content from './content';
import type { EntitiesStructure, StateElementStructure } from '../../redux/types';
import withMenu from '../../hoc/withMenu';
import withBackgroundCard from '../../hoc/withBackgroundCard';
import { fetchWebConfig, changeDeactivationText, changeSiteState, setEventDate } from '../../redux/webConfig/actions';
import { success } from '../../redux/resultStates';

const handlers = {
    handleChangeSiteState: ({ setSiteStateDialogOpen, setActivateSite }) => (event) => {
        setActivateSite(pathOr(true, ['target', 'checked'], event));
        setSiteStateDialogOpen(true);
    },
    handleOkSiteStateDialog: ({ changeSiteStateAPI, setSiteStateDialogOpen, siteIsActivatedAPI, activateSite }) => () => {
        if (siteIsActivatedAPI === activateSite) return;

        setSiteStateDialogOpen(false);
        changeSiteStateAPI(activateSite);
    },
    handleCancelSiteStateDialog: ({ setSiteStateDialogOpen, setActivateSite }) => () => {
        setActivateSite(prevState => !prevState);
        setSiteStateDialogOpen(false);
    },
    handleChangeDeactivateText: ({ setDeactivatedText }) => (event) => {
        setDeactivatedText(pathOr(null, ['target', 'value'], event));
    },
    handleValidateDeactivateText: ({ deactivatedTextAPI, changeDeactivationTextAPI, deactivatedText }) => () => {
        if (deactivatedText === deactivatedTextAPI) return;

        changeDeactivationTextAPI(deactivatedText);
    },
    handleSetEventDate: ({ setNewEventDate }) => (event) => {
        setNewEventDate(pathOr(null, ['target', 'value'], event));
    },
    handleValidateEventDate: ({ eventDate, newEventDate, setEventDateAPI }) => () => {
        if (eventDate === newEventDate) return;

        setEventDateAPI(newEventDate);
    },
};

const lifecyclePlayload = {
    componentDidMount() {
        this.props.fetchWebConfig();
    },
};

const hasPostedSuccessfully = (result, hasRetrievedOnly) => result === success && !hasRetrievedOnly;

const mapStateToProps = ({ entities, webConfig }: { entities: EntitiesStructure, webConfig: StateElementStructure }) => ({
    deactivatedTextAPI: path(['webConfig', 'deactivatedText'], entities),
    eventDate: moment(pathOr(new Date(), ['webConfig', 'eventDate'], entities)).format('YYYY-MM-DD'),
    error: path(['error'], webConfig),
    hasPostedSuccessfully: hasPostedSuccessfully(path(['result'], webConfig), path(['hasRetrievedOnly'], webConfig)),
    isLoading: path(['isLoading'], webConfig),
    siteIsActivatedAPI: path(['webConfig', 'isActivated'], entities),
});

const withPropsPayload = ({ isLoading, siteIsActivatedAPI, deactivatedTextAPI }) => ({
    canEdit: !isNil(siteIsActivatedAPI) && !isNil(deactivatedTextAPI) && !isLoading,
});

const connectToStore = connect(
    mapStateToProps,
    {
        fetchWebConfig,
        changeSiteStateAPI: changeSiteState,
        changeDeactivationTextAPI: changeDeactivationText,
        setEventDateAPI: setEventDate,
    },
);

const initWebConfig = ({ siteIsActivatedAPI, deactivatedTextAPI, setDeactivatedText, setActivateSite }) => {
    setDeactivatedText(deactivatedTextAPI);
    setActivateSite(!!siteIsActivatedAPI);
};

const container = compose(
    connectToStore,
    withProps(withPropsPayload),
    withState('openSiteStateDialog', 'setSiteStateDialogOpen', false),
    withState('activateSite', 'setActivateSite', ({ siteIsActivatedAPI }) => !!siteIsActivatedAPI),
    withState('deactivatedText', 'setDeactivatedText', ({ deactivatedTextAPI }) => deactivatedTextAPI || ''),
    withState('newEventDate', 'setNewEventDate', ({ eventDate }) => eventDate || ''),
    withPropsOnChange(['siteIsActivatedAPI', 'deactivatedTextAPI'], initWebConfig),
    withHandlers(handlers),
    lifecycle(lifecyclePlayload),
    setPropTypes({
        microsoftOnlineAuthUrl: string,
        removeError: func,
    }),
    withMenu,
    withBackgroundCard,
)(Content);

export default container;
