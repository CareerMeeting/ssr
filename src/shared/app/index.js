// @flow
import Helmet from 'react-helmet';
import React from 'react';
import { renderRoutes } from 'react-router-config';
import { Switch, withRouter } from 'react-router-dom';
import { compose, lifecycle, mapProps } from 'recompose';
import { omit, path } from 'ramda';
import { connect } from 'react-redux';

import AppContent from './styles';
import { adminPrivateRoutes, adminPublicRoutes, privateRoutes, DeactivateAppPageView, publicRoutes } from './routes/';
import renderPrivateRoutes from './routes/guards';
import { initSession } from '../redux/session/actions';
import { fetchWebConfig } from '../redux/webConfig/actions';

const App = ({ isActivated }: { isActivated: Boolean }) => (
    <AppContent>
        <Helmet
            htmlAttributes={{ lang: 'fr', amp: undefined }} // amp takes no value
            titleTemplate="%s | Career Meeting Ionis"
            titleAttributes={{ itemprop: 'name', lang: 'en' }}
            meta={[
                { name: 'description', content: 'Career meeting IONIS platform' },
                { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            ]}
        />
        {isActivated ? (
            <Switch>
                {renderPrivateRoutes(privateRoutes)}
                {renderRoutes(publicRoutes)}
            </Switch>
        ) : (
            <Switch>
                {renderPrivateRoutes(adminPrivateRoutes)}
                {renderRoutes([...adminPublicRoutes, { component: DeactivateAppPageView }])}
            </Switch>
        )}
    </AppContent>
);

const lifecyclePlayload = {
    componentDidMount() {
        this.props.initSessionAction();
        this.props.fetchWebConfig();
    },
};

const mapStateToProps = ({ entities, webConfig }) => ({
    isActivated: path(['webConfig', 'isActivated'], entities),
    isLoading: path(['isLoading'], webConfig),
});

const container = compose(
    withRouter,
    connect(mapStateToProps, { initSessionAction: initSession, fetchWebConfig }),
    lifecycle(lifecyclePlayload),
    mapProps(props => omit(['initSessionAction'], props)),
)(App);

export default container;
