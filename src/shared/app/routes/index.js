// @flow
import loadable from 'loadable-components';
import { superAdminType, adminType, studentType, companyType } from '../../helpers/session/constants';

const AdminAuthenticationView = loadable(() => import('../../views/AdminAuthentication/'));
const AdminListView = loadable(() => import('../../views/AdminList/'));
const AdminLoginView = loadable(() => import('../../views/AdminLogin/'));
const AdminProfileView = loadable(() => import('../../views/AdminProfile/'));
const CompaniesListView = loadable(() => import('../../views/CompaniesList/'));
const CompanyProfileView = loadable(() => import('../../views/CompanyProfile'));
const CompanyRegistrationView = loadable(() => import('../../views/CompanyRegistration'));
const CompanySchedulerView = loadable(() => import('../../views/CompanyScheduler/'));
const HomeView = loadable(() => import('../../views/Home/'));
const NotFound = loadable(() => import('../../views/NotFound/'));
const StudentLoginView = loadable(() => import('../../views/StudentLogin/'));
const StudentPlanningView = loadable(() => import('../../views/StudentPlanning'));
const StudentProfileView = loadable(() => import('../../views/StudentProfile/'));
const StudentsListView = loadable(() => import('../../views/StudentsList/'));
const SuperAdminSettingsView = loadable(() => import('../../views/SuperAdminSettings/'));
export const DeactivateAppPageView = loadable(() => import('../../views/AppDeactivated/'));
export const ResetPasswordView = loadable(() => import('../../views/ResetPassword/'));

/** ADMINS ROUTES */

export const adminPublicRoutes = [
    {
        path: '/admin/login',
        exact: true,
        component: AdminAuthenticationView,
    },
    {
        path: '/admin/',
        exact: true,
        component: AdminLoginView,
    },
];

export const adminPrivateRoutes = [
    {
        path: '/admin/profile',
        authorisations: [superAdminType, adminType],
        component: AdminProfileView,
    },
    {
        path: '/admin/settings',
        exact: true,
        authorisations: [superAdminType],
        component: SuperAdminSettingsView,
    },
    {
        path: '/admin/admins',
        exact: true,
        authorisations: [superAdminType],
        component: AdminListView,
    },
    {
        path: '/admin/companies',
        exact: true,
        authorisations: [superAdminType, adminType],
        component: CompaniesListView,
    },
    {
        path: '/admin/students',
        exact: true,
        authorisations: [superAdminType, adminType],
        component: StudentsListView,
    },
];

/** Others routes */

export const publicRoutes = [
    ...adminPublicRoutes,
    {
        path: '/',
        exact: true,
        component: HomeView,
    },
    {
        path: '/reset-password',
        exact: true,
        component: ResetPasswordView,
    },
    {
        path: '/company/subscribe',
        exact: true,
        component: CompanyRegistrationView,
    },
    {
        path: '/student/login',
        exact: true,
        component: StudentLoginView,
    },
    {
        component: NotFound,
    },
];

export const privateRoutes = [
    ...adminPrivateRoutes,
    {
        path: '/student/profile',
        authorisations: [superAdminType, adminType, studentType, companyType],
        component: StudentProfileView,
    },
    {
        path: '/company/planning',
        exact: true,
        authorisations: [superAdminType, adminType, studentType, companyType],
        component: CompanySchedulerView,
    },
    {
        path: '/student/planning',
        exact: true,
        authorisations: [superAdminType, adminType, studentType],
        component: StudentPlanningView,
    },
    {
        path: '/student/companies',
        exact: true,
        authorisations: [superAdminType, adminType, studentType, companyType],
        component: CompaniesListView,
    },
    {
        path: '/company/profile',
        exact: true,
        authorisations: [superAdminType, adminType, studentType, companyType],
        component: CompanyProfileView,
    },
];
