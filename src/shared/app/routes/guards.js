import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { find } from 'ramda';

import { isNilOrEmpty } from '../../helpers/utils';
import {
    getIsAuthenticatedAsAdmin,
    getIsAuthenticatedAsCompany,
    getIsAuthenticatedAsStudent,
    getIsAuthenticatedAsSuperAdmin,
} from '../../helpers/session';
import { adminType, companyType, studentType, superAdminType } from '../../helpers/session/constants';

const authenticationByUserTypes = {
    [companyType]: getIsAuthenticatedAsCompany,
    [studentType]: getIsAuthenticatedAsStudent,
    [adminType]: getIsAuthenticatedAsAdmin,
    [superAdminType]: getIsAuthenticatedAsSuperAdmin,
};

const getAuthenticationByUserType = (userType) => {
    const authentication = authenticationByUserTypes[userType];
    return authentication ? authentication() : false;
};

const getRender = (authorisations, Component) => (props) => {
    const isAuthenticated = find(getAuthenticationByUserType, authorisations);

    return (
        isAuthenticated
            ? <Component {...props} /> :
            <Redirect to="/" />
    );
};

const PrivateRoute = ({ authorisations, privateComponent: Component, ...rest }) => (
    <Route
        {...rest}
        render={getRender(authorisations, Component)}
    />
);

const renderPrivateRoutes = (routes, extraProps = {}) => {
    if (isNilOrEmpty(routes)) return null;

    return routes.map(({ key, path, exact, strict, authorisations, component }, i) => (
        <PrivateRoute
            key={key || i}
            path={path}
            exact={exact}
            strict={strict}
            privateComponent={component}
            authorisations={authorisations}
            {...extraProps}
        />
    ));
};

export default renderPrivateRoutes;
