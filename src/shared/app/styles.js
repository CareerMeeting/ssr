import styled from 'react-emotion';

const AppContent = styled('div')({
    height: '100%',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 400,
    lineHeight: 1.5,
    letterSpacing: '0.01071em',
});

export default AppContent;
