// @flow

export type FunctionnalComponent = React$Element<React$StatelessFunctionalComponent<any>>;
