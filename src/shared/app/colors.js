export const greenLight = '#eaeaea';
export const white = '#ffffff';
export const green = '#008000';
export const orange = '#ffa500';
export const red = '#ff0c16';
export const info = '#1976d2';
