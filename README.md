## React SSR BASE

This project has to purpose to begin with a base of React.JS app in Server Side Rendering (SSR).

You can find in this project:

    🔥 Redux
    🔥 Redux-Saga
    🔥 Flow
    🔥 Express.Js (SSR)
    🔥 Ramda
    🔥 Emotion
    🔥 Babel
    🔥 Webpack
    🔥 Loadable-Components
    🔥 Recompose
    🌎 Jest
    🌎 Eslint
    🌎 React Helmet


### Installation

* Clone the project
* install the dependencies with `yarn` or `npm install`

### Start servers

`yarn start`.
`yarn stop`.
`yarn restart`.
`yarn test`.
`yarn logs`.
